<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

const REGEX_ANY_NUMBER = '[0-9]+';
const REGEX_POSITIVE_NUMBER = '[1-9][0-9]*';
const REGEX_STRING = '[a-zA-Z0-9]+';
const REGEX_COLOR = '[a-zA-Z-0-9]{6}';

/** @var RouteBuilder $routes */
$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $routes) {

    // basic
    $routes->connect('/',
        ['controller' => 'Public', 'action' => 'index'],
        ['_name' => 'index'],
    )->setMethods(['GET']);

    // users
    $routes->connect('/login',
        ['controller' => 'Users', 'action' => 'login'],
        ['_name' => 'login']
    )->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/logout',
        ['controller' => 'Users', 'action' => 'logout'],
        ['_name' => 'logout']
    )->setMethods(['GET']);
    $routes->connect('/register',
        ['controller' => 'Users', 'action' => 'register'],
        ['_name' => 'register']
    )->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/password_recovery',
        ['controller' => 'Users', 'action' => 'passwordRecovery'],
        ['_name' => 'password_recovery']
    )->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/verify_email/{user_id}/{code}',
        ['controller' => 'Users', 'action' => 'verifyEmail'],
        ['_name' => 'verify_email', 'user_id' => REGEX_POSITIVE_NUMBER, 'code' => REGEX_STRING]
    )->setPass(['user_id', 'code'])->setMethods(['GET']);

    // for project proposals
    $routes->connect('/participate/index',
        ['controller' => 'UserProjects', 'action' => 'index'],
        ['_name' => 'user_projects_index']
    )->setMethods(['GET']);
    $routes->connect('/participate/new/{appeal_id}',
        ['controller' => 'UserProjects', 'action' => 'addModify'],
        ['_name' => 'user_projects_add', 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/participate/modify/{project_id}/{appeal_id}',
        ['controller' => 'UserProjects', 'action' => 'addModify'],
        ['_name' => 'user_projects_modify', 'project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/participate/modify/{project_id}/{appeal_id}/detail',
        ['controller' => 'UserProjects', 'action' => 'detail'],
        ['_name' => 'user_projects_detail', 'project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    // proposal budget
    $routes->connect('/participate/modify/{project_id}/{appeal_id}/budget/{item_id}',
        ['controller' => 'UserProjects', 'action' => 'addModifyBudgetItem'],
        ['project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'item_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id', 'item_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/participate/modify/{project_id}/{appeal_id}/budget/add',
        ['controller' => 'UserProjects', 'action' => 'addModifyBudgetItem'],
        ['project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/participate/modify/{project_id}/{appeal_id}/budget/{item_id}/delete',
        ['controller' => 'UserProjects', 'action' => 'deleteBudgetItem'],
        ['project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'item_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id', 'item_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    // proposal attachments
    $routes->connect('/participate/modify/{project_id}/{appeal_id}/attachment/add',
        ['controller' => 'UserProjects', 'action' => 'addModifyAttachment'],
        ['project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/participate/modify/{project_id}/{appeal_id}/attachment/{attachment_id}',
        ['controller' => 'UserProjects', 'action' => 'addModifyAttachment'],
        ['project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/participate/modify/{project_id}/{appeal_id}/attachment/{attachment_id}/delete',
        ['controller' => 'UserProjects', 'action' => 'deleteAttachment'],
        ['project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/participate/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/download',
        ['controller' => 'UserProjects', 'action' => 'downloadAttachment', 'force' => false],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id', 'force'])->setMethods(['GET']);
    $routes->connect('/participate/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/download/force',
        ['controller' => 'UserProjects', 'action' => 'downloadAttachment', 'force' => true],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id', 'force'])->setMethods(['GET']);
    // proposal submission
    $routes->connect('/participate/submit/{project_id}/{appeal_id}',
        ['controller' => 'UserProjects', 'action' => 'submit'],
        ['_name' => 'user_projects_submit', 'project_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['appeal_id', 'project_id'])->setMethods(['GET']);

    // public organization presentation, js widget use-cases
    $routes->connect('/public/{organization_id}',
        ['controller' => 'Widgets', 'action' => 'organization'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'public_organization_detail']
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/public',
        ['controller' => 'Widgets', 'action' => 'discover'],
        ['_name' => 'public_gallery']
    )->setMethods(['GET']);
    $routes->connect('/public/widgets',
        ['controller' => 'Widgets', 'action' => 'index'],
        ['_name' => 'public_widgets']
    )->setMethods(['GET']);

    // public portal, SaaS
    $routes->connect('/portal/index',
        ['controller' => 'Portal', 'action' => 'index'],
        ['_name' => 'portal_index']
    )->setMethods(['GET']);

    // organizations
    $routes->connect('/organizations',
        ['controller' => 'Organizations', 'action' => 'index'],
        ['_name' => 'my_organizations']
    )->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/edit',
        ['controller' => 'Organizations', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/managers',
        ['controller' => 'Organizations', 'action' => 'managers'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/settings',
        ['controller' => 'Organizations', 'action' => 'settings'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/settings/nia',
        ['controller' => 'Organizations', 'action' => 'niaSettings'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/add',
        ['controller' => 'Organizations', 'action' => 'addModify'],
        ['_name' => 'organization_add']
    )->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}',
        ['controller' => 'Organizations', 'action' => 'detail'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'organization_detail']
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/delete',
        ['controller' => 'Organizations', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'organization_delete']
    )->setPass(['organization_id'])->setMethods(['POST']);

    // domains
    $routes->connect('/organizations/{organization_id}/domains/{domain_id}/edit',
        ['controller' => 'Domains', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'domain_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'domain_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/domains/add',
        ['controller' => 'Domains', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/domains/{domain_id}/delete',
        ['controller' => 'Domains', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'domain_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'domain_id'])->setMethods(['POST']);

    // districts
    $routes->connect('/organizations/{organization_id}/districts/{district_id}/edit',
        ['controller' => 'Districts', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'district_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'district_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/districts/add',
        ['controller' => 'Districts', 'action' => 'addModify'],
        ['district_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/districts/{district_id}/delete',
        ['controller' => 'Districts', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'district_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'district_id'])->setMethods(['POST']);

    // project categories
    $routes->connect('/organizations/{organization_id}/categories',
        ['controller' => 'ProjectCategories', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/categories/{category_id}/edit',
        ['controller' => 'ProjectCategories', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'category_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'category_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/categories/{category_id}/delete',
        ['controller' => 'ProjectCategories', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'category_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'category_id'])->setMethods(['POST']);
    $routes->connect('/organizations/{organization_id}/categories/add',
        ['controller' => 'ProjectCategories', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // organization parts
    $routes->connect('/organizations/{organization_id}/parts',
        ['controller' => 'OrganizationParts', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/parts/{part_id}/edit',
        ['controller' => 'OrganizationParts', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'part_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'part_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/parts/{part_id}/delete',
        ['controller' => 'OrganizationParts', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'part_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'part_id'])->setMethods(['POST']);
    $routes->connect('/organizations/{organization_id}/parts/add',
        ['controller' => 'OrganizationParts', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project states
    $routes->connect('/organizations/{organization_id}/states',
        ['controller' => 'ProjectStatus', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/states/{state_id}/edit',
        ['controller' => 'ProjectStatus', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'state_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'state_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/states/{state_id}/delete',
        ['controller' => 'ProjectStatus', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'state_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'state_id'])->setMethods(['POST']);
    $routes->connect('/organizations/{organization_id}/states/add',
        ['controller' => 'ProjectStatus', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'project_status_add']
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // appeals
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/edit',
        ['controller' => 'Appeals', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/add',
        ['controller' => 'Appeals', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_add']
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}',
        ['controller' => 'Appeals', 'action' => 'detail'],
        ['appeal_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_detail']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/copy',
        ['controller' => 'Appeals', 'action' => 'makeCopy'],
        ['appeal_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_copy']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/delete',
        ['controller' => 'Appeals', 'action' => 'delete'],
        ['appeal_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_delete']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['POST']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/order_projects',
        ['controller' => 'Appeals', 'action' => 'projectsOrderHelper'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'appeal_order_projects']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // phases
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/phases/{phase_id}/edit',
        ['controller' => 'Phases', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'phase_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'phase_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/phases/{phase_id}/delete',
        ['controller' => 'Phases', 'action' => 'delete'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'phase_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'phase_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/phases/add',
        ['controller' => 'Phases', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/phases/{phase_id}',
        ['controller' => 'Phases', 'action' => 'detail'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'phase_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'phase_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // projects
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects',
        ['controller' => 'Projects', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/add',
        ['controller' => 'Projects', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}',
        ['controller' => 'Projects', 'action' => 'detail'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // Projects formal control

    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/formal_control',
        ['controller' => 'Projects', 'action' => 'formalControl'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project logs
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/logs/{log_type}/{log_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModifyLog'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'log_id' => REGEX_POSITIVE_NUMBER, 'log_type' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'log_type', 'log_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/logs/{log_type}/add',
        ['controller' => 'Projects', 'action' => 'addModifyLog'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'log_type' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'log_type'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/logs/{log_type}/{log_id}/delete',
        ['controller' => 'Projects', 'action' => 'deleteLog'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'log_id' => REGEX_POSITIVE_NUMBER, 'log_type' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'log_type', 'log_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project budget items
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/budget-items/{item_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModifyBudgetItem'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'item_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'item_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/budget-items/add',
        ['controller' => 'Projects', 'action' => 'addModifyBudgetItem'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/budget-items/{item_id}/delete',
        ['controller' => 'Projects', 'action' => 'deleteBudgetItem'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'item_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'item_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);

    // project attachments
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/edit',
        ['controller' => 'Projects', 'action' => 'addModifyAttachment'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/add',
        ['controller' => 'Projects', 'action' => 'addModifyAttachment'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/delete',
        ['controller' => 'Projects', 'action' => 'deleteAttachment'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/download',
        ['controller' => 'Projects', 'action' => 'downloadAttachment', 'force' => false],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id', 'force'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/appeals/{appeal_id}/projects/{project_id}/attachments/{attachment_id}/download/force',
        ['controller' => 'Projects', 'action' => 'downloadAttachment', 'force' => true],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id', 'force'])->setMethods(['GET']);

    // custom_users
    $routes->connect('/organizations/{organization_id}/custom_users',
        ['controller' => 'CustomUsers', 'action' => 'index'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET']);
    $routes->connect('/organizations/{organization_id}/custom_users/{user_id}/edit',
        ['controller' => 'CustomUsers', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'user_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'user_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/custom_users/add',
        ['controller' => 'CustomUsers', 'action' => 'addModify'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/custom_users/{user_id}/delete',
        ['controller' => 'CustomUsers', 'action' => 'delete'],
        ['user_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'user_id'])->setMethods(['GET']);

    $routes->connect('/organizations/{organization_id}/custom_users/category/{category_id}/edit',
        ['controller' => 'CustomUsers', 'action' => 'addModifyCategory'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'category_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'category_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/custom_users/category/add',
        ['controller' => 'CustomUsers', 'action' => 'addModifyCategory'],
        ['organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/organizations/{organization_id}/custom_users/category/{category_id}/delete',
        ['controller' => 'CustomUsers', 'action' => 'deleteCategory'],
        ['category_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'category_id'])->setMethods(['GET']);

    // voting
    $routes->connect('/vote/result',
        ['controller' => 'Voting', 'action' => 'result'],
        ['_name' => 'voting_final_status']
    )->setMethods(['GET']);
    $routes->connect('/vote/choose-provider',
        ['controller' => 'Voting', 'action' => 'chooseProvider'],
        ['_name' => 'voting_choose_provider']
    )->setMethods(['GET']);
    $routes->connect('/vote/{provider_id}/{otp}',
        ['controller' => 'Voting', 'action' => 'interactive'],
        ['_name' => 'voting_interactive', 'provider_id' => REGEX_POSITIVE_NUMBER, 'otp' => '[0-9a-zA-Z]{16}']
    )->setPass(['provider_id', 'otp'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/vote/{provider_id}',
        ['controller' => 'Voting', 'action' => 'interactive'],
        ['_name' => 'voting_non_interactive', 'provider_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['provider_id'])->setMethods(['GET', 'POST', 'PATCH', 'PUT']);
    $routes->connect('/vote/metadata/{organization_id}/{provider_id}',
        ['controller' => 'Voting', 'action' => 'metadata'],
        ['_name' => 'voting_metadata', 'provider_id' => REGEX_POSITIVE_NUMBER, 'organization_id' => REGEX_POSITIVE_NUMBER]
    )->setPass(['organization_id', 'provider_id'])->setMethods(['GET']);

    // api
    $routes->redirect('/api', '/api/v1/info.json');
    $routes->redirect('/api/v1', '/api/v1/info.json');
    $routes->connect('/api/v1/info.json',
        ['controller' => 'Api', 'action' => 'info'],
        ['_name' => 'api_info']
    )->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organizations.json',
        ['controller' => 'Api', 'action' => 'organizations']
    )->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/info.json',
        ['controller' => 'Api', 'action' => 'organization'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_organization_info']
    )->setPass(['organization_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/projects_in_progress.json',
        ['controller' => 'Api', 'action' => 'projectsInProgress'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_organization_projects_in_progress']
    )->setPass(['organization_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/project/{project_id}/info.json',
        ['controller' => 'Api', 'action' => 'projectInfo'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_project_redirect']
    )->setPass(['organization_id', 'project_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/info.json',
        ['controller' => 'Api', 'action' => 'appeal'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_appeal_info']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/projects.json',
        ['controller' => 'Api', 'action' => 'appealProjects'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_appeal_projects']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    // For programatic proposal, send project data in structure get success/error response
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/add_project',
        ['controller' => 'Api', 'action' => 'appealProposeProject'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_appeal_propose_project']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/add_project_interactive',
        ['controller' => 'Api', 'action' => 'appealProposeProjectInteractive'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_appeal_propose_project_interactive']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['GET', 'POST', 'PUT', 'PATCH', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/project/{project_id}/add_vote',
        ['controller' => 'Api', 'action' => 'projectAddVote'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_project_add_vote']
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/project/{project_id}/substract_vote',
        ['controller' => 'Api', 'action' => 'projectAddVote'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_project_substract_vote']
    )->setPass(['organization_id', 'appeal_id', 'project_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/project/{project_id}/attachment/{attachment_id}',
        ['controller' => 'Api', 'action' => 'projectAttachmentDownload'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, 'project_id' => REGEX_POSITIVE_NUMBER, 'attachment_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_project_download_attachment']
    )->setPass(['organization_id', 'appeal_id', 'project_id', 'attachment_id'])->setMethods(['GET', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/organization/{organization_id}/appeal/{appeal_id}/submit_project',
        ['controller' => 'Api', 'action' => 'submitProject'],
        ['organization_id' => REGEX_POSITIVE_NUMBER, 'appeal_id' => REGEX_POSITIVE_NUMBER, '_name' => 'api_submit_project']
    )->setPass(['organization_id', 'appeal_id'])->setMethods(['POST', 'HEAD', 'OPTIONS']);
    $routes->connect('/api/v1/map_icon/{color}',
        ['controller' => 'Api', 'action' => 'mapIcon'],
        ['color' => REGEX_COLOR]
    )->setPass(['color'])->setMethods(['GET']);

});
