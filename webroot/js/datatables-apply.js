;$(function () {
    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    var parsed = false;
                    $("textarea,input", node).not("input[type=radio]").each(function () {
                        data = $(this).val();
                        parsed = true;
                    })
                    $("input[type=radio]:checked", node).each(function () {
                        data = $(this).parent().text();
                        parsed = true;
                    })
                    return (parsed ? data : $(node).text()).trim();
                }
            }
        }
    };
    var buttonCsvExcelCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    var parsed = false;
                    $("textarea,input", node).not("input[type=radio]").each(function () {
                        data = $(this).val();
                        parsed = true;
                    })
                    $("input[type=radio]:checked", node).each(function () {
                        data = $(this).parent().text();
                        parsed = true;
                    })
                    if ($(node).data('type') === 'currency') {
                        data = data.replace(/[^\d,.]/g, '').replace(/,/g, '.')
                        parsed = true;
                    }
                    return (parsed ? data : $(node).text()).trim();
                }
            }
        }
    };
    var buttonPrintCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    $("textarea,input", node).not("input[type=radio]").each(function () {
                        data = $(this).val();
                    })
                    $("input[type=radio]:checked", node).each(function () {
                        data = $(this).parent().text();
                    })
                    return data;
                }
            }
        }
    };
    $(".datatables").DataTable({
        dom: "<'border-top pb-2'><'row'<'col'l><'col'B><'col'f>>rtip",
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'copy',
                text: 'Kopírovat',
            }),
            $.extend(true, {}, buttonCsvExcelCommon, {
                extend: 'csv',
            }),
            $.extend(true, {}, buttonCsvExcelCommon, {
                extend: 'excel',
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdf',
            }),
            $.extend(true, {}, buttonPrintCommon, {
                extend: 'print',
                text: 'Vytisknout',
            }),
        ],
        pageLength: 200,
        lengthMenu: [20, 50, 100, 150, 200, 250, 300, 500, 1000],
        stateSave: true,
        language: {
            "lengthMenu": "Zobrazit _MENU_ řádků na stránku",
            "zeroRecords": "Hledání neodpovídá žádný záznam",
            "info": "Zobrazit stránku _PAGE_ z _PAGES_",
            "infoEmpty": "Žádné dostupné záznamy",
            "infoFiltered": "(vyhledáváním v _MAX_ záznamech)",
            "search": "Hledat",
            "paginate": {
                "previous": "Předchozí",
                "next": "Následující",
                "first": "První",
                "last": "Poslední",
            },
        }
    });
});
