;(function ($, window, document, undefined) {
    let $target = $("#mapy-select-point");

    let stred_lat = $target.data('center-lat') || 50.08;
    let stred_lon = $target.data('center-lon') || 14.41;
    let stred = SMap.Coords.fromWGS84(parseFloat(stred_lon), parseFloat(stred_lat));
    // limit default zoom-out to level 13
    let zoom = Math.max($target.data('default-zoom') || 13, 13);

    let mapa = new SMap($target.get(0), stred, zoom);
    mapa.setZoomRange(12, 19);
    mapa.addDefaultLayer(SMap.DEF_OPHOTO);
    mapa.addDefaultLayer(SMap.DEF_TURIST).enable();
    mapa.addDefaultLayer(SMap.DEF_BASE);
    mapa.addDefaultControls();

    let wmsLayer = new SMap.Layer.WMS('brno', 'https://gis.brno.cz/services/mapserver/local/nemovity_majetek/gservice',
        {
            'layers': 'psmb',
            'transparent': true,
            'format': 'image/png'
        },
    );
    wmsLayer.getContainer()[SMap.LAYER_TILE].style.opacity = 0.7;
    mapa.addLayer(wmsLayer).enable();

    var layerSwitch = new SMap.Control.Layer({
        width: 65,
        items: 4,
        page: 4
    });
    layerSwitch.addDefaultLayer(SMap.DEF_BASE);
    layerSwitch.addDefaultLayer(SMap.DEF_OPHOTO);
    layerSwitch.addDefaultLayer(SMap.DEF_TURIST);
    mapa.addControl(layerSwitch, {left: "8px", top: "9px"});

    let mouse = new SMap.Control.Mouse(SMap.MOUSE_PAN | SMap.MOUSE_WHEEL | SMap.MOUSE_ZOOM);
    mapa.addControl(mouse);

    let markerLayer = new SMap.Layer.Marker();
    mapa.addLayer(markerLayer).enable();

    let marker = new SMap.Marker(stred);
    marker.decorate(SMap.Marker.Feature.Draggable);
    markerLayer.addMarker(marker);

    let sync = new SMap.Control.Sync({});
    mapa.addControl(sync);

    $.fn.paro2 = $.fn.paro2 || {};
    $.fn.paro2.mapy_select_point = {
        mapa: mapa,
        marker: marker,
        default_stred: stred,
        default_zoom: zoom,
        last_geocoded_coords: undefined,
        setNewCoords: function (event = undefined, coords = undefined, set_map_center = false) {
            coords = typeof coords == 'object' ? coords : SMap.Coords.fromEvent(event.data.event, $.fn.paro2.mapy_select_point.mapa);
            window.coords = coords;
            $.fn.paro2.mapy_select_point.marker.setCoords(coords);
            if (set_map_center === true) {
                $.fn.paro2.mapy_select_point.mapa.setCenter(coords);
            }
            $.fn.paro2.mapy_select_point.doCallback(coords);
        },
        doCallback: function (coords) {
            // coords here are object SMap.Coords
            if ("positionSelected" in window && typeof window.positionSelected === "function") {
                window.positionSelected(coords);
            } else {
                console.error("callback not available (window.positionSelected), just logging: " + coords);
            }
        },
        reverseGeocode: function (coords, text_target = undefined) {
            $.fn.paro2.mapy_select_point.last_geocoded_coords = coords;
            new SMap.Geocoder.Reverse(coords, function (response) {
                if (coords.equals($.fn.paro2.mapy_select_point.last_geocoded_coords)) {
                    if (text_target) {
                        text_target.val(response.getResults().label);
                    } else {
                        console.error('reverseGeocode finished, no data target provided :: ' + response.getResults().label)
                    }
                }
            });
        }
    };

    let mapSignals = mapa.getSignals();
    mapSignals.addListener(window, "map-click", $.fn.paro2.mapy_select_point.setNewCoords);
    mapSignals.addListener(window, "marker-drag-stop", $.fn.paro2.mapy_select_point.setNewCoords);
    /*mapSignals.addListener(window, "zoom-stop", function (event) {
        if ($.fn.paro2.mapy_select_point.mapa.getZoom() < 12) {
            $.fn.paro2.mapy_select_point.mapa.setZoom(12);
        }
    });*/

    // bind suggestors
    $("input.mapy-suggestor").each(function () {
        let suggestor = new SMap.Suggest($(this).get(0), {
            provider: new SMap.SuggestProvider({
                updateParams: params => {
                    let c = $.fn.paro2.mapy_select_point.mapa.getCenter().toWGS84();
                    params.lon = c[0].toFixed(5);
                    params.lat = c[1].toFixed(5);
                    params.zoom = $.fn.paro2.mapy_select_point.mapa.getZoom();
                    params.bounds = "48.5370786,12.0921668|51.0746358,18.8927040";
                    params.lang = "cs";
                }
            })
        });
        let callback = $(this).data('suggestor-callback') || undefined;
        if (typeof callback != "undefined" && callback in window) {
            callback = window[callback];
        }
        suggestor.addListener("suggest", suggestData => {
            if (typeof callback == 'function') {
                callback(suggestData);
            } else {
                console.log("no callback for suggestion " + suggestData);
            }
        });
    });

    $("a#mapy-select-point-kn-link").click(function (event) {
        let markerCoords = $.fn.paro2.mapy_select_point.marker.getCoords().toWGS84();
        let point = $.fn.paro2.cuzk.convertWGS84toEPSG5514(markerCoords[0], markerCoords[1]);
        window.open('http://nahlizenidokn.cuzk.cz/MapaIdentifikace.aspx?l=KN&x=' + parseInt(point.x) + '&y=' + parseInt(point.y), '_blank');
        event.preventDefault();
    });

    $("input#has-no-location").on('change click', function () {
        let $hasNoLocation = $(this).is(':checked');
        $target.toggle(!$hasNoLocation);
        $("input#gps-latitude").parent().toggle(!$hasNoLocation);
        $("input#gps-longitude").parent().toggle(!$hasNoLocation);
        $("a#mapy-select-point-kn-link").toggle(!$hasNoLocation);
        $("input#address").attr('required', false).parent().toggle(!$hasNoLocation);
        $("label[for=district-id]").parent().toggle(!$hasNoLocation);
    }).trigger('change');

})(jQuery, window, document);
