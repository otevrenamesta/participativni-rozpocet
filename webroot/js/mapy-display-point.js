;(function ($, window, document, undefined) {

    let $targets = $(".mapy-display-point");

    $targets.each(function () {
        let $target = $(this);

        console.log('target', $target, $target.data());

        let stred_lat = $target.data('center-lat') || 50.08;
        let stred_lon = $target.data('center-lon') || 14.41;
        let stred = SMap.Coords.fromWGS84(parseFloat(stred_lon), parseFloat(stred_lat));
        // limit default zoom-out to level 13
        let zoom = Math.max($target.data('default-zoom') || 13, 13);

        let mapa = new SMap($target.get(0), stred, zoom);
        mapa.setZoomRange(12, 19);
        mapa.addDefaultLayer(SMap.DEF_OPHOTO);
        mapa.addDefaultLayer(SMap.DEF_TURIST).enable();
        mapa.addDefaultLayer(SMap.DEF_BASE);
        mapa.addDefaultControls();

        let wmsLayer = new SMap.Layer.WMS('brno', 'https://gis.brno.cz/services/mapserver/local/nemovity_majetek/gservice',
            {
                'layers': 'psmb',
                'transparent': true,
                'format': 'image/png'
            },
        );
        wmsLayer.getContainer()[SMap.LAYER_TILE].style.opacity = 0.7;
        mapa.addLayer(wmsLayer).enable();

        var layerSwitch = new SMap.Control.Layer({
            width: 65,
            items: 4,
            page: 4
        });
        layerSwitch.addDefaultLayer(SMap.DEF_BASE);
        layerSwitch.addDefaultLayer(SMap.DEF_OPHOTO);
        layerSwitch.addDefaultLayer(SMap.DEF_TURIST);
        mapa.addControl(layerSwitch, {left: "8px", top: "9px"});

        let mouse = new SMap.Control.Mouse(SMap.MOUSE_PAN | SMap.MOUSE_WHEEL | SMap.MOUSE_ZOOM);
        mapa.addControl(mouse);

        let markerLayer = new SMap.Layer.Marker();
        mapa.addLayer(markerLayer).enable();

        let marker = new SMap.Marker(stred);
        marker.decorate(SMap.Marker.Feature.Draggable);
        markerLayer.addMarker(marker);

        let sync = new SMap.Control.Sync({});
        mapa.addControl(sync);

        $("a.mapy-select-point-kn-link", $target.parent()).click(function (event) {
            let markerCoords = marker.getCoords().toWGS84();
            let point = $.fn.paro2.cuzk.convertWGS84toEPSG5514(markerCoords[0], markerCoords[1]);
            window.open('http://nahlizenidokn.cuzk.cz/MapaIdentifikace.aspx?l=KN&x=' + parseInt(point.x) + '&y=' + parseInt(point.y), '_blank');
            event.preventDefault();
        });
    });

})(jQuery, window, document);
