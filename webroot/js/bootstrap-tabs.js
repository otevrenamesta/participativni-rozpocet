$(function () {
    let $targets = $(".nav-tabs li.nav-item a.nav-link");
    $targets.tab();

    let pageId = 'tab-' + window.location.pathname;

    $targets.on('click', function () {
        Cookies.set(pageId, $(this).attr('id'));
    });

    let stored = Cookies.get(pageId);
    if (stored) {
        $(".nav-tabs li.nav-item a#" + stored).tab("show");
    } else {
        $targets = $(".nav-tabs li.nav-item a.nav-link.active");
        $targets.tab('show');
    }
});
