;(function ($, window, document, undefined) {

    let paro2cuzk = {
            ensureKrovak: function () {
                try {
                    proj4('EPSG:5514')
                } catch (e) {
                    proj4.defs("EPSG:5514", "+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813972222222 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +pm=greenwich +units=m +no_defs +towgs84=570.8,85.7,462.8,4.998,1.587,5.261,3.56");
                }
            },
            convertWGS84toEPSG5514(latitude, longitude) {
                paro2cuzk.ensureKrovak();
                return paro2cuzk.convert(latitude, longitude, proj4.Proj('WGS84'), proj4.Proj('EPSG:5514'));
            },
            convertEPSG5514toEGS84(latitude, longitude) {
                paro2cuzk.ensureKrovak();
                return paro2cuzk.convert(latitude, longitude, proj4.Proj('EPSG:5514'), proj4.Proj('EGS84'));
            },
            convert: function (latitude, longitude, from = undefined, to = undefined) {
                // ensure EPSG:5514 is available
                if (typeof from === 'undefined') {
                    from = proj4.Proj('WGS84')
                }
                if (typeof to === 'undefined') {
                    paro2cuzk.ensureKrovak();
                    to = proj4.Proj('EPSG:5514');
                }
                // do the conversion
                let point = proj4.transform(
                    from, to,
                    proj4.toPoint([latitude, longitude])
                );
                console.log('paro2cuzk::convert from ' + (latitude + " " + longitude) + " to " + (point.x + " " + point.y + " " + point.z));
                return point;
            },
            getMOMCBrno: function (latitude, longitude, callback) {
                let krovak = paro2cuzk.convertWGS84toEPSG5514(latitude, longitude);
                let url = 'https://gis.brno.cz/services/mapserver/local/spravni-cleneni/gservice?typeName=hr-mc&service=WFS&request=GetFeature&outputFormat=json&version=1.1.0&srsName=EPSG:5514&cql_filter=DWITHIN(geom%2CPOINT(' + krovak.x + '%20' + krovak.y + ')%2C1%2Cmeters)';
                $.get(url).done(function (data) {
                    callback(data.features.length > 0 ? data.features[0].properties.kod : null);
                })
            },
            getMOMCCode: function (latitude, longitude, callback) {
                let url = 'https://services.cuzk.cz/wfs/inspire-ad-wfs.asp?service=WFS&version=2.0.0&request=GetFeature&StoredQuery_id=GetFeatureByPoint&srsName=urn:ogc:def:crs:EPSG::4326&FEATURE_TYPE=Address&POINT=' + parseFloat(longitude) + "," + parseFloat(latitude);
                $.get(
                    url
                ).done(function (dom) {
                    let success = false;
                    for (let el of dom.getElementsByTagName('ad:component')) {
                        let href = el.getAttribute('xlink:href');
                        if (href.includes('MOMC')) {
                            let split = href.split('.');
                            split = split[split.length - 1];

                            if (typeof callback === 'function') {
                                callback(parseInt(split));
                            } else {
                                console.error('cuzk::getMOMCCode callback is not a function', split);
                            }

                            success = true;
                        }
                    }
                    if (!success && typeof callback === 'function') {
                        paro2cuzk.getMOMCBrno(latitude, longitude, callback);
                    }
                })
            }
        }
    ;

    $.fn.extend({
        paro2: {
            cuzk: paro2cuzk
        }
    });

    //$.fn.paro2.cuzk.convert(16.1103337, 50.4684273);
    //$.fn.paro2.cuzk.getMOMCCode(16.600320, 49.19185000)

})(jQuery, window, document);
