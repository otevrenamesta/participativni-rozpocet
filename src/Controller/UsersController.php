<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\Http\Response;

/**
 * @property UsersTable $Users
 */
class UsersController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->Authentication->allowUnauthenticated(['login', 'register', 'passwordRecovery', 'passwordRecoveryGo', 'verifyEmail']);
    }

    public function register(): void
    {
        $user = $this->Users->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $entityData = $this->getRequest()->getData();
            $entityData[User::FIELD_VERIFICATION_CODE] = $user->getMailVerificationCode();
            $user = $this->Users->newEntity($entityData);
            $this->set(compact('user'));

            if ($user->hasErrors()) {
                return;
            }

            // set hashed version
            $user->setPassword($user->password);

            if ($this->Users->save($user)) {
                $this->sendMailSafe('User', 'verifyEmail', [$user]);
                $this->Flash->success('Registrace proběhla úspěšně, nyní musíte ověřit svůj účet odkazem, který vám byl zaslán do e-mailu');
                $this->redirect(['_name' => 'index']);
            } else {
                $this->Flash->error('Registrace neproběhla úspěšně, prosím zkuste to znovu');
            }
        }

        $this->set(compact('user'));
    }

    public function passwordRecovery(): void
    {
        if ($this->getCurrentUser() instanceof User) {
            $this->redirect('/');
            return;
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $req_mail = $this->getRequest()->getData('email');
            $user = $this->Users->findByEmail($req_mail)->first();
            if ($user) {
                $this->Flash->success(__('Byl vám zaslán e-mail s odkazem na obnovení hesla'));
            } elseif (!empty($req_mail)) {
                $this->Flash->error(__('Uživatel s danou e-mailovou adresou nebyl nalezen'));
            }
        }

        $this->set('user', $this->Users->newEmptyEntity());
    }

    public function login(): ?Response
    {
        $this->set('user', $this->Users->newEmptyEntity());
        $result = $this->Authentication->getResult();

        if (!$this->getRequest()->is(['post', 'put', 'patch']) && $this->getCurrentUser() instanceof User) {
            return $this->redirect('/');
        }

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $user = $result->getData();

            if (!$result->isValid() || !($user instanceof User)) {
                $this->Flash->error('Neplatné uživatelské jméno nebo heslo');

                return null;
            }

            if ($user->is_enabled !== true) {
                $this->Flash->error(__('Váš účet je aktuálně zakázán'));
                $this->sendMailSafe('User', 'verifyEmail', [$user]);
                $this->Authentication->logout();

                return null;
            }

            $this->Flash->success(__('Vítejte :)'));

            $redirect = '/';
            if (!empty($user->organizations)) {
                if (count($user->organizations) > 1) {
                    $redirect = ['_name' => 'my_organizations'];
                } else {
                    $firstOrganization = reset($user->organizations);
                    $redirect = ['_name' => 'organization_detail', 'organization_id' => $firstOrganization->id];
                }
            }

            return $this->redirect($this->Authentication->getLoginRedirect() ?? $redirect);
        }

        return null;
    }

    public function verifyEmail(int $user_id, string $code)
    {
        /** @var User|null $user */
        $user = $this->Users->find('all', [
            'conditions' => [
                'id' => $user_id,
                'verification_code' => $code,
            ],
        ])->first();
        if ($user) {
            $user->is_enabled = true;
            $this->Users->save($user);
            $this->Flash->success(__('Váš účet je nyní aktivovaný, můžete se přihlásit'));
            $this->redirect(['_name' => 'login']);
        }  else {
            $this->Flash->error(__('Aktivační kód již není platný'));
            $this->redirect('/');
        }
    }

    public function logout(): ?Response
    {
        $this->Authentication->logout();
        $this->Flash->success(__('Nashledanou'));
        return $this->redirect('/');
    }

}
