<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\DistrictsTable;
use App\Model\Table\OrganizationsTable;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read DistrictsTable $Districts
 */
class DistrictsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('Districts');
    }

    public function addModify(int $organization_id, ?int $district_id = null)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $district = $district_id > 0 ? $this->Districts->getWithOrganization($district_id, $organization->id)
            : $this->Districts->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $district = $this->Districts->patchEntity($district, $this->getRequest()->getData() + [
                    'organization_id' => $organization->id,
                ]
            );
            if ($this->Districts->save($district)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
                $this->redirect(['controller' => 'Organizations', 'action' => 'detail', 'organization_id' => $organization_id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('district', 'organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function delete(int $organization_id, int $district_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $district = $this->Districts->getWithOrganization($district_id, $organization_id);

        if ($this->Districts->delete($district)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
            ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }

        return $this->redirect(['action' => 'detail', 'controller' => 'Organizations', 'organization_id' => $organization->id]);
    }

}
