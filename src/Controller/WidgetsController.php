<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\OrganizationsTable;

/**
 * @property-read OrganizationsTable $Organizations
 */
class WidgetsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated([$this->getRequest()->getParam('action')]);
        $this->loadModel('Organizations');
    }

    public function index()
    {

    }

    public function discover()
    {

    }

    public function organization(int $organization_id)
    {
        $organization = $this->Organizations->get($organization_id);
        if (!$organization->is_enabled) {
            $this->Flash->error(__('Tato organizace není aktuálně povolená'));
            $this->redirect('/');
            return;
        }
        $this->set(compact('organization'));
    }

}
