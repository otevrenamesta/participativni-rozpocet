<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Model\Entity\User;
use ArrayAccess;
use Authentication\Controller\Component\AuthenticationComponent;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Event\EventManager;
use Cake\Mailer\MailerAwareTrait;
use Exception;
use Throwable;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 * @property AuthenticationComponent $Authentication
 */
class AppController extends Controller implements EventListenerInterface
{
    use MailerAwareTrait;

    const FLASH_SUCCESS_SAVE = 'Uloženo úspěšně';
    const FLASH_FAILURE_SAVE = 'Formulář obsahuje chyby';
    const FLASH_SUCCESS_DELETE = 'Smazáno úspěšně';
    const FLASH_FAILURE_DELETE = 'Nebylo možné smazat';

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     * @throws Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('FormProtection');
        $this->loadComponent('Authentication.Authentication');

        EventManager::instance()->on($this);
    }

    public function getCurrentUserId(): int
    {
        if (!$this->Authentication || !$this->Authentication->getIdentity()) {
            return 0;
        }
        return intval($this->Authentication->getIdentity()->getIdentifier());
    }

    public function userHasRole(int $user_role_id): bool
    {
        if ($this->getCurrentUserId() === 0) {
            return false;
        }
        return intval($this->getCurrentUser()->user_role_id) === $user_role_id;
    }

    public function userHasManagedOrganizations(): bool
    {
        if ($this->getCurrentUserId() === 0) {
            return false;
        }
        return !empty($this->getCurrentUser()->organizations)
            || !empty($this->getCurrentUser()->organizations_to_users);
    }

    /**
     * @return null|User|ArrayAccess|array
     */
    public function getCurrentUser(): ?ArrayAccess
    {
        if (!$this->Authentication || !$this->Authentication->getIdentity()) {
            return null;
        }
        return $this->Authentication->getIdentity()->getOriginalData();
    }

    protected function persistReferer()
    {
        $this->getRequest()->getSession()->write('refer.' . md5($this->getRequest()->getRequestTarget()), $this->referer());
    }

    protected function retrieveReferer($default = null)
    {
        return $this->getRequest()->getSession()->read('refer.' . md5($this->getRequest()->getRequestTarget())) ?? $default ?? $this->referer();
    }

    public function sendMailSafe(string $mailer, string $mailAction, array $args = []): bool
    {
        try {
            $this->getMailer($mailer)->send($mailAction, $args);

            return true;
        } catch (Throwable $t) {
            $this->Flash->error(__('Odesílání e-mailu selhalo, chyba: ') . $t->getMessage());
        }

        return false;
    }

    public function implementedEvents(): array
    {
        return array_merge(parent::implementedEvents(), ['Model.Version.beforeSave' => 'versionIncludeUserId']);
    }

    public function versionIncludeUserId(Event $event): array
    {
        $currentUserId = $this->getCurrentUserId();
        return [
            'user_id' => $currentUserId > 0 ? $currentUserId : null,
        ];
    }
}
