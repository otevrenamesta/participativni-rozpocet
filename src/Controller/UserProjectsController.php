<?php
declare(strict_types=1);

namespace App\Controller;


use App\Model\Entity\Appeal;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectSubmissionStatus;
use App\Model\Table\AttachmentsTable;
use App\Model\Table\ContactsTable;
use App\Model\Table\OrganizationsTable;
use App\Model\Table\ProjectBudgetItemsTable;
use App\Model\Table\ProjectsTable;
use Cake\Collection\Collection;
use Cake\Http\Exception\NotFoundException;
use Laminas\Diactoros\UploadedFile;

/**
 * @property-read ProjectsTable $Projects
 * @property-read OrganizationsTable $Organizations
 * @property-read ContactsTable $Contacts
 * @property-read ProjectBudgetItemsTable $ProjectBudgetItems
 * @property-read AttachmentsTable $Attachments
 */
class UserProjectsController extends AppController
{

    private iterable $contacts;
    private iterable $contacts_ids;
    private iterable $projects;
    private iterable $current_submission_phases;
    private iterable $current_open_appeals;

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('Projects');
        $this->loadModel('Contacts');
        $this->loadModel('ProjectBudgetItems');
        $this->loadModel('Attachments');

        $this->contacts = $this->Contacts->find('all', [
            'conditions' => [
                'email' => $this->getCurrentUser()->email,
            ]
        ]);
        $this->contacts_ids = (new Collection($this->contacts))->extract('id')->toList();

        $this->projects = empty($this->contacts_ids) ? (new Collection([])) : $this->Projects->find('all', [
            'conditions' => [
                'proposed_by_contact_id IN' => $this->contacts_ids
            ],
            'contain' => ProjectsTable::ALL_CONTAIN,
        ])->order(['Projects.modified' => 'DESC']);

        $this->current_submission_phases = $this->Projects->Appeals->Phases->find('all', [
            'conditions' => [
                'Phases.date_start <=' => date('Y-m-d'),
                'Phases.date_end >=' => date('Y-m-d'),
                'Phases.allows_proposals_submission' => true,
            ]
        ])->extract('appeal_id')->toList();

        $this->current_open_appeals = empty($this->current_submission_phases) ? (new Collection([])) : $this->Projects->Appeals->find('all', [
            'conditions' => [
                'Appeals.id IN' => $this->current_submission_phases
            ],
            'contain' => [
                'Organizations',
                'Organizations.ProjectStatus',
                'Organizations.ProjectCategories',
                'Organizations.Districts',
                'Organizations.OrganizationParts',
            ]
        ]);
    }

    private function getAppeal(int $appeal_id, bool $throwIfNotFound = true): ?Appeal
    {
        foreach ($this->current_open_appeals as $appeal) {
            if ($appeal->id === $appeal_id) {
                return $appeal;
            }
        }
        if ($throwIfNotFound) {
            throw new NotFoundException();
        }
        return null;
    }

    private function getProject(int $appeal_id, int $project_id, bool $throwIfNotFound = true): ?Project
    {
        foreach ($this->projects as $project) {
            if ($project->id === $project_id) {
                if ($project->appeal_id === $appeal_id) {
                    return $project;
                }
            }
        }
        if ($throwIfNotFound) {
            throw new NotFoundException();
        }
        return null;
    }

    public function index()
    {
        $this->set('projects', $this->projects);
        $this->set('contacts', $this->contacts);
        $this->set('contacts_ids', $this->contacts_ids);
        $this->set('open_appeals', $this->current_open_appeals);
        $this->set('open_phases', $this->current_submission_phases);
    }

    public function detail(int $appeal_id, int $project_id)
    {
        $project = $this->getProject($appeal_id, $project_id);
        $appeal = $project->appeal;
        $organization = $appeal->organization;

        $emptyBudgetItem = $this->Projects->ProjectBudgetItems->newEmptyEntity();
        $emptyAttachment = $this->Projects->Attachments->newEmptyEntity();
        $attachmentTypes = $this->Projects->Attachments->AttachmentTypes->find('list');
        $parts = (new Collection($organization->organization_parts ?? []))->combine('id', 'name');

        $this->set(compact('appeal', 'project', 'organization', 'emptyAttachment', 'emptyBudgetItem', 'attachmentTypes', 'parts'));
    }

    public function addModifyAttachment(int $appeal_id, int $project_id, ?int $attachment_id = null)
    {
        $project = $this->getProject($appeal_id, $project_id);
        $appeal = $project->appeal;
        $attachment = $attachment_id > 0 ? $this->Projects->Attachments->getWithProject($attachment_id, $project->id) : $this->Projects->Attachments->newEmptyEntity();
        $attachmentTypes = $this->Projects->Attachments->AttachmentTypes->find('list');

        if ($project->canProposerEdit() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $file = $this->getRequest()->getUploadedFile('filedata');
            $attachment = $this->Projects->Attachments->patchEntity($attachment, $this->getRequest()->getData() + [
                    'project_id' => $project->id
                ]
            );
            if ($file instanceof UploadedFile) {
                if ($file->getError() !== UPLOAD_ERR_OK) {
                    $attachment->setError('filedata', __('Soubor se nepodařilo nahrát'));
                } else {
                    $attachment->filepath = $attachment->fileUploadMove($file, $appeal->organization_id, $project->id);
                    $attachment->filesize = $file->getSize();
                    $attachment->original_filename = $file->getClientFilename();
                }
            }
            if ($this->Attachments->save($attachment)) {
                if ($attachment->attachment_type_id === AttachmentType::TYPE_PHOTO && $this->getRequest()->getData('is_title_image')) {
                    $project->title_image_id = $attachment->id;
                    $this->Projects->save($project);
                }
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'detail', 'appeal_id' => $appeal->id, 'project_id' => $project->id]);
            } else {
                if ($attachment->isNew()) {
                    $attachment->deletePhysically();
                }
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('appeal', 'project', 'attachment', 'attachmentTypes'));
    }

    public function deleteAttachment(int $appeal_id, int $project_id, int $attachment_id)
    {
        $project = $this->getProject($appeal_id, $project_id);
        $attachment = $this->Attachments->get($attachment_id, ['conditions' => ['project_id' => $project->id]]);
        if ($project->canProposerEdit() && $this->Attachments->delete($attachment)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        $this->redirect($this->referer());
    }

    public function addModifyBudgetItem(int $appeal_id, int $project_id, ?int $item_id = null)
    {
        $project = $this->getProject($appeal_id, $project_id);
        $appeal = $project->appeal;
        $item = $item_id > 0 ? $this->ProjectBudgetItems->get($item_id, ['conditions' => ['ProjectBudgetItems.project_id' => $project->id]])
            : $this->ProjectBudgetItems->newEmptyEntity();

        if ($project->canProposerEdit() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $item = $this->ProjectBudgetItems->patchEntity($item, $this->getRequest()->getData() + [
                    'project_id' => $project->id,
                    'is_final' => false,
                ]);
            $item->total_price = $item->item_price ?? 0 * $item->item_count ?? 0;
            if ($this->ProjectBudgetItems->save($item)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'detail', 'appeal_id' => $appeal->id, 'project_id' => $project->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('appeal', 'project', 'item'));
    }

    public function deleteBudgetItem(int $appeal_id, int $project_id, int $item_id)
    {
        $project = $this->getProject($appeal_id, $project_id);
        $item = $this->ProjectBudgetItems->get($item_id, ['conditions' => ['project_id' => $project->id]]);
        if ($project->canProposerEdit() && $this->ProjectBudgetItems->delete($item)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        $this->redirect($this->referer());
    }

    public function addModify(int $appeal_id, ?int $project_id = null)
    {
        $appeal = $this->getAppeal($appeal_id);
        $project = $project_id > 0 ? $this->getProject($appeal_id, $project_id) :
            $this->Projects->newEmptyEntity();

        if (!$project->isNew() && !$project->canProposerEdit()) {
            $this->Flash->error(__('Tento projekt aktuálně nemůžete upravovat'));
            $this->redirect(['action' => 'detail', 'project_id' => $project->id, 'appeal_id' => $appeal->id]);
            return;
        }

        if ($project->canProposerEdit() && $this->getRequest()->is(['post', 'put', 'patch'])) {
            $newStatus = $appeal->organization->findAppropriateSubmissionStatus($project->isNew() ? ProjectSubmissionStatus::NOT_SUBMITTED : $project->project_submission_status_id);
            $project = $this->Projects->patchEntity($project, array_merge_recursive($this->getRequest()->getData(), [
                'appeal_id' => $appeal->id,
                'project_status_id' => $newStatus ? $newStatus->id : 0,
                'project_submission_status_id' => $project->isNew() ? ProjectSubmissionStatus::NOT_SUBMITTED : $project->project_submission_status_id,
                'contact' => [
                    'email' => $this->getCurrentUser()->email
                ]
            ]));
            if ($this->Projects->save($project)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'detail', 'project_id' => $project->id, 'appeal_id' => $appeal->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('appeal', 'project'));
        $this->set('organization', $appeal->organization);
    }

    public function submit(int $appeal_id, int $project_id)
    {
        $appeal = $this->getAppeal($appeal_id);
        $project = $this->getProject($appeal_id, $project_id);
        $submitValidationError = $project->getSubmitValidationError();

        if ($submitValidationError !== null || !$project->canProposerSubmit()) {
            $this->Flash->error(__('Tento projet nyní nemůžete odeslat'));
            if ($submitValidationError) {
                $this->Flash->error($submitValidationError);
            }
        } else {
            $project->prepareProposerSubmit($appeal->organization);
            if ($this->Projects->save($project)) {
                $this->Flash->success(__('Projekt byl úspěšně odeslán'));
            } else {
                $this->Flash->error(__('Odeslání projektu se nezdařilo'));
            }
        }
        $this->redirect(['action' => 'index']);
    }

    public function downloadAttachment(int $organization_id, int $appeal_id, int $project_id, int $attachment_id, bool $forceDownload = false)
    {
        $appeal = $this->getAppeal($appeal_id);
        $project = $this->getProject($appeal_id, $project_id);
        $attachment = $project->getAttachmentById($attachment_id);
        $path = $attachment->getRealPath();
        if (is_file($path)) {
            return $this->getResponse()
                ->withType($attachment->getMimeType())
                ->withDownload($attachment->original_filename)
                ->withFile($attachment->getRealPath(), [
                    'download' => $forceDownload,
                    'name' => $attachment->original_filename,
                ]);
        }
        return $this->getResponse()->withStatus(404);
    }
}
