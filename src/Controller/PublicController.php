<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Organization;
use App\Model\Table\OrganizationsTable;

/**
 * @property-read OrganizationsTable $Organizations
 */
class PublicController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->Authentication->allowUnauthenticated(['index']);
        $this->loadModel('Organizations');
    }

    public function index()
    {
        $organizations = $this->Organizations->find('all', [
            'conditions' => [
                Organization::FIELD_IS_ENABLED => true,
            ]
        ]);
        $this->set(compact('organizations'));
    }

}
