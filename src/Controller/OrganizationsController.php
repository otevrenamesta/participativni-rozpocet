<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\UserRole;
use App\Model\Table\OrganizationsTable;
use Cake\Utility\Hash;

/**
 * @property-read OrganizationsTable $Organizations
 */
class OrganizationsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        if (!$this->userHasRole(UserRole::ROLE_MANAGER) && !$this->userHasManagedOrganizations()) {
            $this->Flash->error(__('Přístup odepřen'));
            $this->redirect('/');
        }
    }

    public function index()
    {
        $this->set('organizations', $this->Organizations->findAllWithUser($this->getCurrentUserId()));
    }

    public function addModify(?int $organization_id = null)
    {
        $organization = $organization_id > 0 ? $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId()) : $this->Organizations->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $organization = $this->Organizations->patchEntity($organization, $this->getRequest()->getData() + [
                    Organization::FIELD_ADMIN_USER_ID => $organization->isNew() ? $this->getCurrentUserId() : $organization->admin_user_id
                ]
            );

            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__(self::FLASH_SUCCESS_SAVE));
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATIONS);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization_id]);
                $this->redirect(['action' => 'detail', 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(__(self::FLASH_FAILURE_SAVE));
            }
        }

        $this->set(compact('organization'));
        $crumbs = [__('Moje organizace') => 'my_organizations'];
        if (!$organization->isNew()) {
            $crumbs += [$organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]];
        }
        $this->set('crumbs', $crumbs);
    }

    public function settings(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            foreach (Hash::flatten($this->getRequest()->getData('settings')) as $key => $value) {
                foreach ($organization->organization_settings as $setting) {
                    if ($setting->name === $key) {
                        if ($setting->value !== $value) {
                            $setting->set('value', $value);
                            $organization->setDirty('organization_settings');
                        }
                        continue 2;
                    }
                }
                if (OrganizationSetting::getDefaultValue($key) === $value || empty($value)) {
                    continue;
                }
                $new_setting = $this->Organizations->OrganizationSettings->newEntity([
                    'name' => $key,
                    'value' => $value,
                    'organization_id' => $organization->id,
                ]);
                $this->Organizations->OrganizationSettings->save($new_setting);
                $organization->organization_settings[] = $new_setting;
            }

            if ($this->Organizations->save($organization)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', h($organization->name) => ['action' => 'detail', 'organization_id' => $organization->id]]);
    }

    public function niaSettings(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser(
            $organization_id,
            $this->getCurrentUserId()
        );

        $this->set(compact('organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', h($organization->name) => ['action' => 'detail', 'organization_id' => $organization->id]]);
    }

    public function detail(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser(
            $organization_id,
            $this->getCurrentUserId(),
            ['ProjectCategories', 'ProjectStatus', 'Domains', 'Appeals', 'Districts', 'OrganizationParts', 'ProjectStatus.ProjectFinalStates']
        );

        $this->set(compact('organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations']);
    }

    public function delete(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        if ($this->Organizations->delete($organization)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }
        $this->redirect(['action' => 'index']);
    }

    public function managers(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId(), [
            'Managers',
        ]);
        $users = $this->Organizations->Users->find('list');

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $organization = $this->Organizations->patchEntity($organization, $this->getRequest()->getData());
            if ($this->Organizations->save($organization)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'detail', 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'users'));
    }

}
