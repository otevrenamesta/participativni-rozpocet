<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\CustomUserCategoriesTable;
use App\Model\Table\CustomUsersTable;
use App\Model\Table\OrganizationsTable;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read CustomUsersTable $CustomUsers
 * @property-read CustomUserCategoriesTable $CustomUserCategories
 */
class CustomUsersController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('CustomUsers');
        $this->loadModel('CustomUserCategories');
    }

    public function index(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $users = $this->CustomUsers->find('all', [
            'conditions' => [
                'organization_id' => $organization_id
            ],
        ]);
        $categories = $this->CustomUserCategories->find('all', [
            'conditions' => [
                'organization_id' => $organization_id
            ],
            'contain' => [
                'CustomUsers'
            ]
        ]);
        $this->set(compact('organization', 'users', 'categories'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', h($organization->name) => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function addModify(int $organization_id, ?int $custom_user_id = null)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $custom_user = $custom_user_id > 0 ? $this->CustomUsers->getWithOrganization($custom_user_id, $organization->id, ['Contacts'])
            : $this->CustomUsers->newEmptyEntity();
        $categories = $this->CustomUserCategories->find('list', [
            'conditions' => [
                'organization_id' => $organization->id,
            ]
        ]);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $patch_data = $this->getRequest()->getData() + [
                    'organization_id' => $organization->id,
                ];
            if (!is_array($patch_data['custom_user_categories']['_ids'])) {
                $patch_data['custom_user_categories']['_ids'] = [$patch_data['custom_user_categories']['_ids']];
            }
            $custom_user = $this->CustomUsers->patchEntity($custom_user, $patch_data);
            if ($this->CustomUsers->save($custom_user)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'index', 'organization_id' => $organization_id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('custom_user', 'organization', 'categories'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function delete(int $organization_id, int $custom_user_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $custom_user = $this->CustomUsers->getWithOrganization($custom_user_id, $organization_id);

        if ($this->CustomUsers->delete($custom_user)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }

        return $this->redirect(['action' => 'index', 'organization_id' => $organization->id]);
    }

    public function addModifyCategory(int $organization_id, ?int $custom_category_id = null)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $custom_category = $custom_category_id > 0 ? $this->CustomUserCategories->getWithOrganization($custom_category_id, $organization->id)
            : $this->CustomUserCategories->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $custom_category = $this->CustomUserCategories->patchEntity($custom_category, $this->getRequest()->getData() + [
                    'organization_id' => $organization->id,
                ]
            );
            if ($this->CustomUserCategories->save($custom_category)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                $this->redirect(['action' => 'index', 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('custom_category', 'organization'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function deleteCategory(int $organization_id, int $custom_category_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $custom_category = $this->CustomUserCategories->getWithOrganization($custom_category_id, $organization_id, ['CustomUsers']);

        if ($this->CustomUsers->deleteMany($custom_category->custom_users) && $this->CustomUserCategories->delete($custom_category)) {
            $this->Flash->success(self::FLASH_SUCCESS_DELETE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_DELETE);
        }

        return $this->redirect(['action' => 'index', 'organization_id' => $organization->id]);
    }

}
