<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Table\OrganizationPartsTable;
use App\Model\Table\OrganizationsTable;

/**
 * @property-read OrganizationsTable $Organizations
 * @property-read OrganizationPartsTable $OrganizationParts
 */
class OrganizationPartsController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Organizations');
        $this->loadModel('OrganizationParts');
    }

    public function index(int $organization_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $parts = $this->OrganizationParts->find('all', [
            'conditions' => [
                'organization_id' => $organization->id
            ],
        ])->toArray();
        $this->set(compact('organization', 'parts'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id]]);
    }

    public function addModify(int $organization_id, ?int $part_id = null)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $part = $part_id > 0 ? $this->OrganizationParts->getWithOrganization($part_id, $organization->id) :
            $this->OrganizationParts->newEmptyEntity();

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {
            $part = $this->OrganizationParts->patchEntity($part, $this->getRequest()->getData() + [
                    'organization_id' => $organization->id
                ]
            );

            if ($this->OrganizationParts->save($part)) {
                $this->Flash->success(self::FLASH_SUCCESS_SAVE);
                ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
                $this->redirect(['action' => 'index', 'organization_id' => $organization->id]);
            } else {
                $this->Flash->error(self::FLASH_FAILURE_SAVE);
            }
        }

        $this->set(compact('organization', 'part'));
        $this->set('crumbs', [__('Moje organizace') => 'my_organizations', $organization->name => ['_name' => 'organization_detail', 'organization_id' => $organization->id], __('Útvary (části úřadu)') => ['action' => 'index', 'organization_id' => $organization->id]]);
    }

    public function delete(int $organization_id, int $part_id)
    {
        $organization = $this->Organizations->getOrganizationWithUser($organization_id, $this->getCurrentUserId());
        $part = $this->OrganizationParts->getWithOrganization($part_id, $organization->id);
        if ($this->OrganizationParts->delete($part)) {
            ApiController::resetCache(ApiController::CACHE_KEY_PREFIX_ORGANIZATION_DETAIL, [$organization->id]);
            $this->Flash->success(self::FLASH_SUCCESS_SAVE);
        } else {
            $this->Flash->error(self::FLASH_FAILURE_SAVE);
        }
        return $this->redirect($this->referer());
    }

}
