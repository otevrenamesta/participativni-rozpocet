<?php
declare(strict_types=1);

namespace App\IdentityProvider;

use App\Controller\Component\VotingComponent;
use App\Model\Entity\CustomUser;
use App\Model\Entity\IdentityProvider;
use Cake\Http\ServerRequest;

class CustomUsersProvider extends RegisteredUsersProvider
{

    public function getProviderId(): int
    {
        return IdentityProvider::PROVIDER_CUSTOM_USERS;
    }

    protected function getVerifiedUser(ServerRequest $request)
    {
        $customUsersTable = $this->getTableLocator()->get('CustomUsers');

        return $customUsersTable->find('all', [
            'conditions' => [
                'username' => $request->getData('username'),
                'password' => $request->getData('password'),
                'organization_id' => $this->_component->getOrganization()->id,
            ],
            'contain' => [
                'Contacts'
            ]
        ])->first();
    }

    public function processRequest(VotingResult $initialResult, ServerRequest $request): VotingResult
    {
        if ($request->is(['post', 'put', 'patch'])) {

            $user = $this->getVerifiedUser($request);

            if ($user instanceof CustomUser) {
                $this->_component->persist([
                    self::PERSIST_USER_EMAIL => $user->username,
                    self::PERSIST_CURRENT_PHASE => self::PHASE_AFTER_LOGIN,
                ]);
                $initialResult->setUniqueIdentityKey($user->contact->identity_key)
                    ->setResultCode(VotingComponent::VOTE_SUCCESS);
            } else {
                $this->_component->persist([
                    VotingComponent::PERSIST_CUSTOM_ERROR_CODE => VotingComponent::VOTE_FAILED_AUTH,
                    VotingComponent::PERSIST_CUSTOM_ERROR_MESSAGE => __('Uživatelské jméno nebo heslo není správné')
                ]);
                $initialResult->setResultCode(VotingComponent::VOTE_FAILED_AUTH);
            }
        }

        return $initialResult;
    }

    public function getInteractiveTemplatePath(): ?string
    {
        return '/IdentityProviders/custom_users';
    }

}
