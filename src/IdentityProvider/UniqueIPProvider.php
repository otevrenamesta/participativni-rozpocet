<?php
declare(strict_types=1);

namespace App\IdentityProvider;

use App\Controller\Component\VotingComponent;
use App\Model\Entity\IdentityProvider;

class UniqueIPProvider extends AbstractIdentityProvider
{

    public function vote(VotingResult $initialResult): VotingResult
    {
        return $initialResult
            ->setUniqueIdentityKey($this->_component->getController()->getRequest()->clientIp())
            ->setResultCode(VotingComponent::VOTE_SUCCESS);
    }

    public function getProviderId(): int
    {
        return IdentityProvider::PROVIDER_UNIQUE_IP;
    }
}
