<?php
declare(strict_types=1);


namespace App\IdentityProvider;


use App\Controller\Component\VotingComponent;
use App\Model\Entity\IdentityProvider;
use Cake\Http\ServerRequest;
use Cake\Utility\Text;

class WithouAuthenticationProvider extends RegisteredUsersProvider
{

    public const PERSIST_CAPTCHA = 'voting.captcha.code';
    public const PERSIST_CAPTCHA_VERIFY = 'voting.captcha.user';
    public const PERSIST_IP = 'voting.captcha.ip';

    public function getProviderId(): int
    {
        return IdentityProvider::PROVIDER_WITHOUT_AUTHENTICATION;
    }

    public function vote(VotingResult $initialResult): VotingResult
    {
        $currentPhase = $this->_component->retrieve(self::PERSIST_CURRENT_PHASE, true, self::PHASE_BEFORE_LOGIN);
        if ($currentPhase !== self::PHASE_AFTER_LOGIN) {
            return $initialResult->setNeedsRedirect(true)
                ->setRedirectUrl($this->getAuthUrl());
        }
        // clear persistence
        $this->_component->retrieve(self::PERSIST_CAPTCHA, true);
        $this->_component->retrieve(self::PERSIST_CAPTCHA_VERIFY, true);
        // pass the vote result
        return $initialResult->setFullIdentityData(json_encode($this->_component->retrieve(self::PERSIST_IP, true)));
    }

    public function processRequest(VotingResult $initialResult, ServerRequest $request): VotingResult
    {
        $captcha = $this->_component->retrieve(self::PERSIST_CAPTCHA);
        if (empty($captcha)) {
            $this->_component->persist([
                self::PERSIST_CAPTCHA => mb_strtoupper(random_str('alphanum', 5))
            ]);
        }

        if ($request->is(['post', 'put', 'patch'])) {

            $captcha = $this->_component->retrieve(self::PERSIST_CAPTCHA);
            $provided = $request->getData('captcha');

            if ($captcha === $provided) {
                $this->_component->persist([
                    self::PERSIST_CURRENT_PHASE => self::PHASE_AFTER_LOGIN,
                    self::PERSIST_CAPTCHA_VERIFY => $provided,
                    self::PERSIST_IP => $request->clientIp(),
                ]);
                $initialResult->setUniqueIdentityKey(Text::uuid())
                    ->setResultCode(VotingComponent::VOTE_SUCCESS);
            } else {
                $initialResult->setResultCode(VotingComponent::VOTE_FAILED_AUTH);
            }
        }

        return $initialResult;
    }

    protected function getAuthUrl(): array
    {
        return [
            '_name' => 'voting_interactive',
            'provider_id' => $this->getProviderId(),
            'otp' => $this->_component->getOTP(),
        ];
    }

    public function getInteractiveTemplatePath(): ?string
    {
        return '/IdentityProviders/without_authentication';
    }
}
