<?php
declare(strict_types=1);

namespace App\IdentityProvider;

use App\Controller\Component\VotingComponent;
use App\Model\Entity\IdentityProvider;
use Cake\Http\ServerRequest;

class StateIssuedDocuments extends RegisteredUsersProvider
{

    public function getProviderId(): int
    {
        return IdentityProvider::PROVIDER_STATE_ISSUED_DOCUMENT;
    }

    public function vote(VotingResult $initialResult): VotingResult
    {
        $currentPhase = $this->_component->retrieve(self::PERSIST_CURRENT_PHASE, true, self::PHASE_BEFORE_LOGIN);
        if ($currentPhase !== self::PHASE_AFTER_LOGIN) {
            return $initialResult->setNeedsRedirect(true)
                ->setRedirectUrl($this->getAuthUrl());
        }

        return $initialResult->setNeedsRedirect(false);
    }

    public function processRequest(VotingResult $initialResult, ServerRequest $request): VotingResult
    {
        if ($request->is(['post', 'put', 'patch'])) {

            $known_documents = [
                ['ID', 111111984, false],
                ['ID', 111111985, true],
                ['P', 11112057, false],
                ['P', 11112062, true],
            ];

            $req_type = $request->getData('document_type');
            $req_id = intval($request->getData('document_id'));

            $error_message = null;
            $status = false;
            $found_match = false;
            foreach ($known_documents as $known_document) {
                if ($known_document[0] === $req_type) {
                    if ($known_document[1] === $req_id) {
                        $found_match = true;
                        $status = $known_document[2];
                    }
                }
            }

            if (!$found_match) {
                $status = false;
                $error_message = __('Doklad se zadanými informacemi nebylo možné ověřit');
            } elseif (!$status) {
                $error_message = __('Bohužel nejste občanem s trvalým pobytem v obci, kde se snažíte hlasovat');
            }

            if ($status) {
                $this->_component->persist([
                    self::PERSIST_CURRENT_PHASE => self::PHASE_AFTER_LOGIN,
                ]);
                $initialResult->setUniqueIdentityKey($req_type . '|' . strval($req_id))
                    ->setResultCode(VotingComponent::VOTE_SUCCESS);
            } else {
                if ($error_message) {
                    $this->_component->persist([
                        VotingComponent::PERSIST_CUSTOM_ERROR_CODE => VotingComponent::VOTE_FAILED_AUTH,
                        VotingComponent::PERSIST_CUSTOM_ERROR_MESSAGE => $error_message
                    ]);
                }
                $initialResult->setResultCode(VotingComponent::VOTE_FAILED_AUTH);
            }
        }

        return $initialResult;
    }

    public function getInteractiveTemplatePath(): ?string
    {
        return '/IdentityProviders/iszr_proxy';
    }

}
