<?php
declare(strict_types=1);

namespace App\IdentityProvider;


use App\Controller\Component\VotingComponent;
use App\Model\Entity\IdentityProvider;
use App\Model\Entity\User;
use Cake\Http\ServerRequest;

class ManagerUsersProvider extends RegisteredUsersProvider
{

    protected function getVerifiedUser(ServerRequest $request)
    {
        $user = parent::getVerifiedUser($request);

        if ($user instanceof User) {
            $usersTable = $this->getTableLocator()->get('Users');
            $usersTable->loadInto($user, ['Organizations', 'SharedOrganizations']);
            if (!$user->isManagerToOrganization($this->_component->getOrganization()->id)) {
                $user = null;
                $this->_component->persist([
                    VotingComponent::PERSIST_CUSTOM_ERROR_CODE => VotingComponent::VOTE_FAILED_AUTH,
                    VotingComponent::PERSIST_CUSTOM_ERROR_MESSAGE => __('Přihlášení bylo úspěšné, ale nejste manažerem organizace ') . h($this->_component->getOrganization()->name)
                ]);
            }
        }

        return $user;
    }

    public function getProviderId(): int
    {
        return IdentityProvider::PROVIDER_PORTAL_MANAGERS;
    }
}
