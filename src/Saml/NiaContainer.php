<?php
declare(strict_types=1);

namespace App\Saml;

use Cake\Log\Log;
use Cake\Utility\Text;
use Psr\Log\LoggerInterface;
use SAML2\Compat\AbstractContainer;

class NiaContainer extends AbstractContainer
{

    public function getLogger(): LoggerInterface
    {
        return Log::engine('default');
    }

    public function generateId(): string
    {
        return '_' . Text::uuid();
    }

    public function debugMessage($message, string $type): void
    {
    }

    public function redirect(string $url, array $data = []): void
    {

    }

    public function postRedirect(string $url, array $data = []): void
    {
    }

    public function getTempDir(): string
    {
    }

    public function writeFile(string $filename, string $data, int $mode = null): void
    {
    }
}
