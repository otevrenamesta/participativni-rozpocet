<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * ProjectSubmissionStatus Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Project[] $projects
 */
class ProjectSubmissionStatus extends Entity
{
    public const NOT_SUBMITTED = 1,
        WAITING_FOR_FORMAL_CHECK = 2,
        WAITING_FOR_RE_SUBMISSION = 3,
        FORMAL_CONTROL_ACCEPTED = 4;

    public static function getColor(int $project_submission_status_id): string
    {
        switch ($project_submission_status_id) {
            default:
            case self::NOT_SUBMITTED:
                return '#DC3545';
            case self::WAITING_FOR_FORMAL_CHECK:
                return '#FFC107';
            case self::WAITING_FOR_RE_SUBMISSION:
                return '#17A2B8';
            case self::FORMAL_CONTROL_ACCEPTED:
                return '#28A745';
        }
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'projects' => true,
    ];

    public static function getLabel($submission_status_id): ?string
    {
        switch ($submission_status_id) {
            case self::NOT_SUBMITTED:
                return __('Neodeslaný koncept');
            case self::WAITING_FOR_FORMAL_CHECK:
                return __('Odeslaný, čeká na formální kontrolu');
            case self::WAITING_FOR_RE_SUBMISSION:
                return __('Čeká na úpravy od navrhovatele');
            case self::FORMAL_CONTROL_ACCEPTED:
                return __('Akceptovaný formální kontrolou');
        }
        return null;
    }
}
