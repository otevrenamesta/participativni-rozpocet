<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\Utility\Hash;
use Josegonzalez\Version\Model\Behavior\Version\VersionTrait;

/**
 * Appeal Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string|null $info_link
 * @property string $year
 * @property int $budget_czk
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 * @property Phase[] $phases
 * @property Project[] $projects
 */
class Appeal extends Entity
{
    use VersionTrait;
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'info_link' => true,
        'year' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
        'phases' => true,
        'projects' => true,
        'budget_czk' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_ORGANIZATION_ID = 'organization_id';
    public const FIELD_INFO_LINK = 'info_link';
    public const FIELD_YEAR = 'year';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_ORGANIZATION = 'organization';
    public const FIELD_PHASES = 'phases';
    public const FIELD_PROJECTS = 'projects';
    public const FIELD_BUDGET_CZK = 'budget_czk';

    /**
     * @return Phase[]
     */
    public function getCurrentPhases(): iterable
    {
        $phases = [];
        foreach ($this->phases ?? [] as $phase) {
            if ($phase->isCurrentlyActive()) {
                $phases[$phase->id] = $phase;
            }
        }
        return $phases;
    }

    public function getCountByCategory(): array
    {
        $category_name = [];
        $category_count = [];

        foreach ($this->projects ?? [] as $project) {
            foreach ($project->project_categories as $project_category) {
                $category_name[$project_category->id] = $project_category->name;
                $category_count[$project_category->id] = 1 + ($category_count[$project_category->id] ?? 0);
            }
        }

        foreach ($category_count as $key => $val) {
            if (!is_numeric($key)) {
                continue;
            }
            $category_count[$category_name[$key]] = $val;
            unset($category_count[$key]);
        }

        return $category_count;
    }

    public function getCountByStatus(): array
    {
        $status_name = [];
        $status_count = [];

        foreach ($this->projects ?? [] as $project) {
            $status_name[$project->project_status->id] = $project->project_status->name;
            $status_count[$project->project_status->id] = 1 + ($category_count[$project->project_status->id] ?? 0);
        }

        foreach ($status_count as $key => $val) {
            if (!is_numeric($key)) {
                continue;
            }
            $status_count[$status_name[$key]] = $val;
            unset($status_count[$key]);
        }

        return $status_count;
    }

    public function getCountByFinalState(): array
    {
        $status_name = [];
        $status_count = [];

        foreach ($this->projects ?? [] as $project) {
            $status_name[$project->project_status->project_final_status_id] = $project->project_status->project_final_state->name;
            $status_count[$project->project_status->project_final_status_id] = 1 + ($category_count[$project->project_status->project_final_status_id] ?? 0);
        }

        foreach ($status_count as $key => $val) {
            if (!is_numeric($key)) {
                continue;
            }
            $status_count[$status_name[$key]] = $val;
            unset($status_count[$key]);
        }

        return $status_count;
    }

    public function getCountByDistrictId(): array
    {
        $district_count = [
            0 => 0
        ];
        $district_name = [
            0 => __('Bez městské části')
        ];

        foreach ($this->projects ?? [] as $project) {
            $district_count[$project->district_id ?? 0] = 1 + ($district_count[$project->district_id] ?? 0);
            if ($project->district) {
                $district_name[$project->district_id] = $project->district->name;
            }
        }

        foreach ($district_count as $key => $val) {
            if (!is_numeric($key)) {
                continue;
            }
            unset($district_count[$key]);
            $district_count[$district_name[$key]] = $val;
        }

        return $district_count;
    }

    public function loadPhases(): self
    {
        if (!is_array($this->phases)) {
            $organizationsTable = $this->getTableLocator()->get('Appeals');
            $organizationsTable->loadInto($this, ['Phases']);
        }
        return $this;
    }

    public function loadProjects(bool $force = false): self
    {
        if (!is_array($this->phases) || $force === true) {
            $organizationsTable = $this->getTableLocator()->get('Appeals');
            $organizationsTable->loadInto($this, ['Projects.ProjectStatus.ProjectFinalStates', 'Projects.Votes']);
        }
        return $this;
    }

    public function getPhaseById(int $phase_id, string $field_to_extract = null)
    {
        $this->loadPhases();
        foreach ($this->phases ?? [] as $phase) {
            if ($phase->id === $phase_id) {
                if (!empty($field_to_extract)) {
                    return Hash::get($phase, $field_to_extract);
                }
                return $phase;
            }
        }
        return null;
    }

    public function getProjectsInOrder(): array
    {
        $this->loadProjects();
        $projects = [];

        foreach ($this->projects as $project) {
            list($votesByPhase, $overall) = $project->getVotesBreakdown();

            // proposed order with winner/reason indications
            $data = [
                'order' => 0,
                'reason' => null,
                'is_winner' => $project->project_status->project_final_status_id === ProjectFinalState::STATE_FINAL_SUCCESS,
                'positive' => $overall['positive'],
                'negative' => $overall['negative'],
                'total' => $project->getBudgetTotal(false),
                'overall' => $overall['positive'] - $overall['negative'],
                'project' => $project,
            ];

            if ($overall['negative'] > $overall['positive']) {
                $data['reason'] = __('Více negativních než pozitivních hlasů');
                $data['is_winner'] = false;
            }

            $projects[] = $data;
        }

        usort($projects, function ($a, $b) {
            $overall = $b['positive'] - $a['positive'];
            if ($overall === 0) {
                $overall = $a['negative'] - $b['negative'];
            }
            if ($overall === 0) {
                $overall = $b['overall'] - $a['overall'];
            }
            return $overall;
        });

        $winner_counter = 1;
        $budget_consumed = 0;
        foreach ($projects as $key => $project_data) {
            if (!$project_data['is_winner']) {
                continue;
            }
            $budget_consumed += $project_data['total'];
            if ($project_data['is_winner']) {
                $project_data['is_winner'] = $budget_consumed <= $this->budget_czk;
                if (!$project_data['is_winner']) {
                    $project_data['reason'] = __('Rozpočet byl již vyčerpán úspěšnějšími projekty');
                } else {
                    $project_data['order'] = $winner_counter++;
                    $project_data['reason'] = __('V pořadí se vejde do rozpočtu ročníku');
                }
            } else {
                $project_data['reason'] = $project_data['project']->project_status->project_final_state->name;
            }
            $projects[$key] = $project_data;
        }

        usort($projects, function ($a, $b) {
            return $a['order'] - $b['order'];
        });

        return $projects;
    }
}
