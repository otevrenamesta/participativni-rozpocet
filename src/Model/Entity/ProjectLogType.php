<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * ProjectLogType Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property ProjectLog[] $project_logs
 */
class ProjectLogType extends Entity
{
    public const TYPE_PROJECT_HISTORY = 1,
        TYPE_FEASIBILITY_STATEMENT = 2;
    const ALL_TYPES = [
        self::TYPE_FEASIBILITY_STATEMENT,
        self::TYPE_PROJECT_HISTORY,
    ];

    public static function getLabel(int $log_type_id): string
    {
        switch ($log_type_id) {
            default:
            case self::TYPE_PROJECT_HISTORY:
                return __('Deník / Historie projektu');
            case self::TYPE_FEASIBILITY_STATEMENT:
                return __('Posouzení proveditelnosti');
        }
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'project_logs' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_PROJECT_LOGS = 'project_logs';
}
