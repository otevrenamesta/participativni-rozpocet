<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Josegonzalez\Version\Model\Behavior\Version\VersionTrait;

/**
 * ProjectStatus Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property bool $is_public
 * @property bool $is_voting_enabled
 * @property bool $is_default_for_proposals
 * @property bool $is_editable_by_proposer
 * @property bool $are_votes_public
 * @property int $project_final_status_id
 * @property string $color hex string like FF00FF
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 * @property Project[] $projects
 * @property ProjectFinalState $project_final_state
 */
class ProjectStatus extends Entity
{
    use VersionTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'name' => true,
        'is_public' => true,
        'is_voting_enabled' => true,
        'is_default_for_proposals' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
        'projects' => true,
        'is_editable_by_proposer' => true,
        'color' => true,
        'are_votes_public' => true,
        'project_final_status_id' => true,
        'project_final_status' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_ORGANIZATION_ID = 'organization_id';
    public const FIELD_NAME = 'name';
    public const FIELD_IS_PUBLIC = 'is_public';
    public const FIELD_IS_VOTING_ENABLED = 'is_voting_enabled';
    public const FIELD_IS_DEFAULT_FOR_PROPOSALS = 'is_default_for_proposals';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_ORGANIZATION = 'organization';
    public const FIELD_PROJECTS = 'projects';
    public const FIELD_IS_EDITABLE_BY_PROPOSER = 'is_editable_by_proposer';
    public const FIELD_ARE_VOTES_PUBLIC = 'are_votes_public';
}
