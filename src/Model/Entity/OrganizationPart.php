<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * OrganizationPart Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property int|null $number_code
 * @property string|null $text_code
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property ProjectLog[] $project_logs
 */
class OrganizationPart extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'name' => true,
        'number_code' => true,
        'text_code' => true,
        'modified' => true,
        'created' => true,
        'project_logs' => true,
    ];
}
