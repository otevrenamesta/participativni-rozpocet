<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Josegonzalez\Version\Model\Behavior\Version\VersionTrait;

/**
 * OrganizationSetting Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property string|null $value
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Organization $organization
 */
class OrganizationSetting extends Entity
{
    use VersionTrait;

    public const MAP_CENTER_LAT = 'map.center.lat',
        MAP_CENTER_LON = 'map.center.lon',
        MAP_DEFAULT_ZOOM = 'map.default.zoom';

    public const NIA_PUBLIC_KEY_PEM = 'nia_public_key',
        NIA_PRIVATE_KEY_PEM = 'nia_private_key',
        NIA_CERTIFICATE_PEM = 'nia_certificate',
        NIA_USE_PRODUCTION = 'nia_production_enabled';

    public const ORGANIZATION_CONTACT_EMAIL = 'org.contact_email',
        HAS_CITY_DISCTRICTS = 'org.enable_districts',
        ORGANIZATION_PRIMARY_PORTAL_URL = 'org.primary_portal_url';

    public const CUZK_ALLOWED_KATASTR_UZEMI = 'cuzk.allowed_katastr_uzemi_list',
        CUZK_ALLOWED_KATASTR_MUNICIPALITY = 'cuzk.allowed_katastr_municipality_list';

    public const DEFAULT_VALUES = [
        self::MAP_CENTER_LAT => 49.7437989,
        self::MAP_CENTER_LON => 15.3386383,
        self::MAP_DEFAULT_ZOOM => 13,
        self::HAS_CITY_DISCTRICTS => false,
    ];

    public const ALL_BASIC = [
        self::ORGANIZATION_CONTACT_EMAIL,
        self::ORGANIZATION_PRIMARY_PORTAL_URL,
        self::HAS_CITY_DISCTRICTS,
        self::MAP_CENTER_LON,
        self::MAP_CENTER_LAT,
        self::MAP_DEFAULT_ZOOM,
        self::CUZK_ALLOWED_KATASTR_UZEMI,
        self::CUZK_ALLOWED_KATASTR_MUNICIPALITY,
    ];

    public const FLOATS = [
        self::MAP_CENTER_LON,
        self::MAP_CENTER_LAT
    ];

    public const INTEGERS = [
        self::MAP_DEFAULT_ZOOM
    ];

    public const TEXTAREAS = [
        self::NIA_PRIVATE_KEY_PEM,
        self::NIA_PUBLIC_KEY_PEM,
        self::NIA_CERTIFICATE_PEM,
    ];

    public const BOOLEANS = [
        self::NIA_USE_PRODUCTION,
        self::HAS_CITY_DISCTRICTS,
    ];
    public const DOUBLES = [

    ];
    public const EMAILS = [
        self::ORGANIZATION_CONTACT_EMAIL,
    ];

    /**
     * @param string $field setting id, see constants
     * @return string
     */
    public static function getSettingDataType(string $field): string
    {
        if (in_array($field, self::BOOLEANS)) {
            return 'checkbox';
        }

        if (in_array($field, self::INTEGERS)) {
            return 'integer';
        }

        if (in_array($field, self::DOUBLES)) {
            return 'decimal';
        }

        if (in_array($field, self::EMAILS)) {
            return 'email';
        }

        if (in_array($field, self::TEXTAREAS)) {
            return 'textarea';
        }

        return 'string';
    }

    public static function getLabels(): array
    {
        return [
            self::MAP_CENTER_LAT => __('Umístění obce (střed mapy) - šířka (např. 50.12345)'),
            self::MAP_CENTER_LON => __('Umístění obce (střed mapy) - délka (např. 14.98764)'),
            self::MAP_DEFAULT_ZOOM => __('Výchozí přiblížení mapy na střed (min. 12, max. 19)'),
            self::ORGANIZATION_CONTACT_EMAIL => __('Kontaktní e-mail organizace'),
            self::HAS_CITY_DISCTRICTS => __('Povolit správu a párování městských obvodů (městských částí) pro tuto organizaci'),
            self::CUZK_ALLOWED_KATASTR_UZEMI => __('Výčet ID katastrálních území (ČUZK), na kterých lze navrhovat/realizovat projekty'),
            self::CUZK_ALLOWED_KATASTR_MUNICIPALITY => __('Výčet ID obcí (ČUZK), na kterých lze navrhovat/realizovat projekty'),
            self::ORGANIZATION_PRIMARY_PORTAL_URL => __('Hlavní url adresa pro účastníky participativního rozpočtu'),
        ];
    }

    public static function getSettingLabel($field): ?string
    {
        return self::getLabels()[$field] ?? null;
    }

    /**
     * @param string $field setting id, see constants
     * @return string
     */
    public static function getSettingFormOptionName(string $field): string
    {
        if (in_array($field, self::BOOLEANS)) {
            return 'checked';
        }

        return 'value';
    }

    /**
     * @param string $field setting id
     * @param string $value serialized, string, value
     * @return bool|float|int|string|null NULL if the value does not conform
     */
    public static function deserializeValue(string $field, string $value)
    {
        if (in_array($field, self::BOOLEANS)) {
            return boolval($value);
        }

        if (in_array($field, self::INTEGERS)) {
            return intval($value);
        }

        if (in_array($field, self::FLOATS)) {
            return floatval($value);
        }

        if (in_array($field, self::DOUBLES)) {
            return doubleval($value);
        }

        if (in_array($field, self::EMAILS)) {
            $value = filter_var($value, FILTER_SANITIZE_EMAIL);
            return filter_var($value, FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE);
        }

        if (in_array($field, self::TEXTAREAS)) {
            return mb_substr($value, 0, 65535);
        }

        return $value;
    }

    /**
     * instance wrapper for static class method
     *
     * @return bool|float|int|string|null
     */
    public function _deserializeValue()
    {
        return self::deserializeValue($this->name, $this->value);
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'name' => true,
        'value' => true,
        'modified' => true,
        'created' => true,
        'organization' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_ORGANIZATION_ID = 'organization_id';
    public const FIELD_NAME = 'name';
    public const FIELD_VALUE = 'value';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_ORGANIZATION = 'organization';

    /**
     * @param string $settingName
     * @return mixed|null NULL in case it's default or the default value was not found
     */
    public static function getDefaultValue(string $settingName)
    {
        return self::DEFAULT_VALUES[$settingName] ?? null;
    }
}
