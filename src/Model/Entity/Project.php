<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Josegonzalez\Version\Model\Behavior\Version\VersionTrait;

/**
 * Project Entity
 *
 * @property int $id
 * @property int $appeal_id
 * @property int|null $title_image_id
 * @property int $proposed_by_contact_id
 * @property int $project_status_id
 * @property int $project_submission_status_id
 * @property string $name
 * @property string $annotation
 * @property string|null $description
 * @property string|null $formal_comment
 * @property string|null $public_interest
 * @property bool $has_no_location
 * @property string|null $gps_longitude
 * @property string|null $gps_latitude
 * @property string $address
 * @property int|null $district_id
 * @property string|null $promo_video_url
 * @property int $cache_votes_overall
 * @property int $cache_votes_positive
 * @property int $cache_votes_negative
 * @property int $final_appeal_order
 * @property int $progress
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property-read FrozenTime|null $version_created
 * @property-read int|null $version_user_id
 *
 * @property Appeal $appeal
 * @property Attachment[] $attachments
 * @property Contact $contact
 * @property ProjectStatus $project_status
 * @property ProjectBudgetItem[] $project_budget_items
 * @property ProjectLog[] $project_logs
 * @property ProjectsToCategory[] $projects_to_categories
 * @property Vote[] $votes
 * @property ProjectCategory[] $project_categories
 * @property District $district
 * @property ProjectSubmissionStatus $project_submission_status
 */
class Project extends Entity
{
    use VersionTrait;
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'appeal_id' => true,
        'title_image_id' => true,
        'proposed_by_contact_id' => true,
        'project_submission_status_id' => true,
        'project_status_id' => true,
        'name' => true,
        'description' => true,
        'public_interest' => true,
        'gps_longitude' => true,
        'gps_latitude' => true,
        'address' => true,
        'progress' => true,
        'modified' => true,
        'created' => true,
        'appeal' => true,
        'attachments' => true,
        'contact' => true,
        'project_status' => true,
        'project_budget_items' => true,
        'project_logs' => true,
        'projects_to_categories' => true,
        'votes' => true,
        'project_categories' => true,
        'district_id' => true,
        'district' => true,
        'formal_comment' => true,
        'project_submission_status' => true,
        'promo_video_url' => true,
        'has_no_location' => true,
        'cache_votes_negative' => true,
        'cache_votes_positive' => true,
        'cache_votes_overall' => true,
        'annotation' => true,
        'final_appeal_order' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_APPEAL_ID = 'appeal_id';
    public const FIELD_TITLE_IMAGE_ID = 'title_image_id';
    public const FIELD_PROPOSED_BY_CONTACT_ID = 'proposed_by_contact_id';
    public const FIELD_PROJECT_STATUS_ID = 'project_status_id';
    public const FIELD_NAME = 'name';
    public const FIELD_DESCRIPTION = 'description';
    public const FIELD_PUBLIC_INTEREST = 'public_interest';
    public const FIELD_GPS_LONGITUDE = 'gps_longitude';
    public const FIELD_GPS_LATITUDE = 'gps_latitude';
    public const FIELD_ADDRESS = 'address';
    public const FIELD_PROGRESS = 'progress';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_APPEAL = 'appeal';
    public const FIELD_ATTACHMENTS = 'attachments';
    public const FIELD_CONTACT = 'contact';
    public const FIELD_PROJECT_STATUS = 'project_status';
    public const FIELD_PROJECT_BUDGET_ITEMS = 'project_budget_items';
    public const FIELD_PROJECT_LOGS = 'project_logs';
    public const FIELD_PROJECTS_TO_CATEGORIES = 'projects_to_categories';
    public const FIELD_VOTES = 'votes';
    public const FIELD_PROJECT_CATEGORIES = 'project_categories';
    public const FIELD_PROJECT_SUBMISSION_STATUS_ID = 'project_submission_status_id';

    /**
     * @param int $logType
     * @return ProjectLog[]
     */
    public function getLogsByType(int $logType): iterable
    {
        $logs = [];
        foreach ($this->project_logs as $log) {
            if ($log->project_log_type_id === $logType) {
                $logs[$log->id] = $log;
            }
        }
        return $logs;
    }

    public function getAttachmentById(int $attachment_id): ?Attachment
    {
        foreach ($this->attachments ?? [] as $attachment) {
            if ($attachment->id === $attachment_id) {
                return $attachment;
            }
        }
        return null;
    }

    public function isFormallyAccepted(): bool
    {
        return $this->project_submission_status_id === ProjectSubmissionStatus::FORMAL_CONTROL_ACCEPTED;
    }

    public function isPublic(): bool
    {
        return $this->isFormallyAccepted() && $this->project_status->is_public;
    }

    public function isWaitingForFormalControl(): bool
    {
        return $this->project_submission_status_id === ProjectSubmissionStatus::WAITING_FOR_FORMAL_CHECK;
    }

    public function isWaitingForReSubmission(): bool
    {
        return $this->project_submission_status_id === ProjectSubmissionStatus::WAITING_FOR_RE_SUBMISSION;
    }

    public function canProposerEdit(): bool
    {
        return $this->isNew() || in_array($this->project_submission_status_id, [
                ProjectSubmissionStatus::NOT_SUBMITTED,
                ProjectSubmissionStatus::WAITING_FOR_RE_SUBMISSION,
            ], true) ||
            $this->project_status->is_editable_by_proposer;
    }

    public function canProposerSubmit(): bool
    {
        return in_array($this->project_submission_status_id, [ProjectSubmissionStatus::NOT_SUBMITTED, ProjectSubmissionStatus::WAITING_FOR_RE_SUBMISSION], true);
    }

    public function canFormalControlCheck(): bool
    {
        return $this->project_submission_status_id === ProjectSubmissionStatus::WAITING_FOR_FORMAL_CHECK;
    }

    public function prepareProposerSubmit(Organization $organization): self
    {
        $this->set(self::FIELD_PROJECT_SUBMISSION_STATUS_ID, ProjectSubmissionStatus::WAITING_FOR_FORMAL_CHECK);
        $appropriateStatus = $organization->findAppropriateSubmissionStatus(ProjectSubmissionStatus::WAITING_FOR_FORMAL_CHECK);
        if ($appropriateStatus) {
            $this->set(self::FIELD_PROJECT_STATUS_ID, $appropriateStatus->id);
        }
        return $this;
    }

    /**
     * @param bool $join
     * @param string $glue
     * @return array|string joined string or array of strings
     */
    public function getCategories(bool $join = true, string $glue = ', ')
    {
        $categories = (new Collection($this->project_categories))->extract('name')->toList();
        return $join ? join($glue, $categories) : $categories;
    }

    public function isProposer(User $user): bool
    {
        return $this->contact->email === $user->email;
    }

    public function getVotesBreakdown(): array
    {
        $votes = [];
        $overall = ['positive' => 0, 'negative' => 0];
        foreach ($this->votes as $vote) {
            $key = $vote->phase_id;
            if (!isset($votes[$key])) {
                $votes[$key] = ['positive' => 0, 'negative' => 0];
            }
            $votes[$key][$vote->is_upvote ? 'positive' : 'negative']++;
            $overall[$vote->is_upvote ? 'positive' : 'negative']++;
        }
        return [$votes, $overall];
    }

    public function getSubmitValidationError(): ?string
    {
        if (empty($this->project_budget_items)) {
            return __('Projekt musí obsahovat alespoň 1 položku v rozpočtu');
        }
        return null;
    }

    public function loadBudgetItems(): self
    {
        if (!is_array($this->project_budget_items)) {
            $organizationsTable = $this->getTableLocator()->get('Projects');
            $organizationsTable->loadInto($this, ['ProjectBudgetItems']);
        }
        return $this;
    }

    public function getBudgetTotal(bool $count_approved = false): int
    {
        $sum = 0;
        $this->loadBudgetItems();
        foreach($this->project_budget_items ?? [] as $budget_item) {
            if ($budget_item->is_final && $count_approved) {
                $sum += $budget_item->total_price;
            } else if (!$budget_item->is_final && !$count_approved) {
                $sum += $budget_item->total_price;
            }
        }
        return $sum;
    }
}
