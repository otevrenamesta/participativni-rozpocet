<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;

/**
 * AttachmentType Entity
 *
 * @property int $id
 * @property string $name
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property Attachment[] $attachments
 */
class AttachmentType extends Entity
{
    public const TYPE_PHOTO = 1,
        TYPE_OTHERS = 2;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'modified' => true,
        'created' => true,
        'attachments' => true,
    ];

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_ATTACHMENTS = 'attachments';

    public static function getLabel(int $attachment_type_id): string
    {
        switch ($attachment_type_id) {
            default:
            case self::TYPE_PHOTO:
                return __('Fotografie');
            case self::TYPE_OTHERS:
                return __('Ostatní');
        }
    }
}
