<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Josegonzalez\Version\Model\Behavior\Version\VersionTrait;

/**
 * District Entity
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property int|null $ui_momc
 * @property FrozenTime|null $modified
 *
 * @property Organization $organization
 */
class District extends Entity
{
    use VersionTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'organization_id' => true,
        'name' => true,
        'modified' => true,
        'organization' => true,
        'ui_momc' => true,
    ];
}
