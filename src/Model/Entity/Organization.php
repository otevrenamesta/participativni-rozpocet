<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Josegonzalez\Version\Model\Behavior\Version\VersionTrait;

/**
 * Organization Entity
 *
 * @property int $id
 * @property int $admin_user_id
 * @property string $name
 * @property bool $is_enabled
 * @property FrozenTime|null $modified
 * @property FrozenTime|null $created
 *
 * @property User $user
 * @property Appeal[] $appeals
 * @property CustomUserCategory[] $custom_user_categories
 * @property CustomUser[] $custom_users
 * @property Domain[] $domains
 * @property OrganizationSetting[] $organization_settings
 * @property OrganizationsToUser[] $organizations_to_users
 * @property ProjectCategory[] $project_categories
 * @property ProjectStatus[] $project_status
 * @property District[] $districts
 * @property User[] $managers
 * @property OrganizationPart[] $organization_parts
 */
class Organization extends Entity
{
    use LocatorAwareTrait;
    use VersionTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'admin_user_id' => true,
        'name' => true,
        'is_enabled' => true,
        'modified' => true,
        'created' => true,
        'user' => true,
        'appeals' => true,
        'custom_user_categories' => true,
        'custom_users' => true,
        'domains' => true,
        'organization_settings' => true,
        'organizations_to_users' => true,
        'project_categories' => true,
        'project_status' => true,
        'districts' => true,
        'managers' => true,
    ];

    public function loadAppeals(): self
    {
        if (!is_array($this->appeals)) {
            $organizationsTable = $this->getTableLocator()->get('Organizations');
            $organizationsTable->loadInto($this, ['Appeals']);
        }
        return $this;
    }

    public function loadDomains(): self
    {
        if (!is_array($this->domains)) {
            $organizationsTable = $this->getTableLocator()->get('Organizations');
            $organizationsTable->loadInto($this, ['Domains']);
        }
        return $this;
    }

    public const FIELD_ID = 'id';
    public const FIELD_ADMIN_USER_ID = 'admin_user_id';
    public const FIELD_NAME = 'name';
    public const FIELD_IS_ENABLED = 'is_enabled';
    public const FIELD_MODIFIED = 'modified';
    public const FIELD_CREATED = 'created';
    public const FIELD_USER = 'user';
    public const FIELD_APPEALS = 'appeals';
    public const FIELD_CUSTOM_USER_CATEGORIES = 'custom_user_categories';
    public const FIELD_CUSTOM_USERS = 'custom_users';
    public const FIELD_DOMAINS = 'domains';
    public const FIELD_ORGANIZATION_SETTINGS = 'organization_settings';
    public const FIELD_ORGANIZATIONS_TO_USERS = 'organizations_to_users';
    public const FIELD_PROJECT_CATEGORIES = 'project_categories';
    public const FIELD_PROJECT_STATUS = 'project_status';
    public const FIELD_MANAGERS = 'managers';

    public function isDomainAuthorized(string $domain): bool
    {
        if (empty($this->domains) || empty($domain)) {
            return false;
        }
        foreach ($this->domains as $configured_domain) {
            if (strcmp(mb_strtolower($domain), mb_strtolower($configured_domain)) === 0) {
                return true;
            }
        }
        return false;
    }

    public function loadSettings(bool $force = false): self
    {
        if ($this->organization_settings === null || $force === true) {
            $organizationsTable = $this->getTableLocator()->get('Organizations');
            $organizationsTable->loadInto($this, ['OrganizationSettings']);
        }
        return $this;
    }

    public function getSetting(string $settingName): ?OrganizationSetting
    {
        $this->loadSettings();
        foreach ($this->organization_settings as $setting) {
            if ($setting->name === $settingName) {
                return $setting;
            }
        }
        return null;
    }

    /**
     * @param string $settingName
     * @param bool $returnDefaultIfNotFound
     * @return mixed|string|null NULL in case the setting is not found and/or the default is not found
     */
    public function getSettingValue(string $settingName, bool $returnDefaultIfNotFound = false)
    {
        $setting = $this->getSetting($settingName);
        return $setting ? $setting->_deserializeValue() : ($returnDefaultIfNotFound ? OrganizationSetting::getDefaultValue($settingName) : null);
    }

    public function findAppropriateSubmissionStatus(int $submission_status): ?ProjectStatus
    {
        foreach ($this->project_status as $status) {
            switch ($submission_status) {
                case ProjectSubmissionStatus::NOT_SUBMITTED:
                case ProjectSubmissionStatus::WAITING_FOR_FORMAL_CHECK:
                    if ($status->is_default_for_proposals) {
                        return $status;
                    }
                    break;

                case ProjectSubmissionStatus::WAITING_FOR_RE_SUBMISSION:
                    if ($status->is_editable_by_proposer) {
                        return $status;
                    }
                    break;
            }
        }
        return null;
    }

    public function sortAppeals(): self
    {
        usort($this->appeals, function (Appeal $a, Appeal $b) {
            return intval($b->year) - intval($a->year);
        });
        return $this;
    }
}
