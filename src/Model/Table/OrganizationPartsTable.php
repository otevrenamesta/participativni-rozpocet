<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\OrganizationPart;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrganizationParts Model
 *
 * @property ProjectLogsTable&HasMany $ProjectLogs
 *
 * @method OrganizationPart newEmptyEntity()
 * @method OrganizationPart newEntity(array $data, array $options = [])
 * @method OrganizationPart[] newEntities(array $data, array $options = [])
 * @method OrganizationPart get($primaryKey, $options = [])
 * @method OrganizationPart findOrCreate($search, ?callable $callback = null, $options = [])
 * @method OrganizationPart patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method OrganizationPart[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method OrganizationPart|false save(EntityInterface $entity, $options = [])
 * @method OrganizationPart saveOrFail(EntityInterface $entity, $options = [])
 * @method OrganizationPart[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method OrganizationPart[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method OrganizationPart[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method OrganizationPart[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class OrganizationPartsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('organization_parts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ProjectLogs', [
            'foreignKey' => 'organization_part_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('number_code')
            ->allowEmptyString('number_code');

        $validator
            ->scalar('text_code')
            ->maxLength('text_code', 50)
            ->allowEmptyString('text_code');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }

    public function getWithOrganization(int $part_id, int $organization_id, array $contain = [])
    {
        return $this->get($part_id, [
            'conditions' => [
                'OrganizationParts.organization_id' => $organization_id
            ],
            'contain' => $contain
        ]);
    }
}
