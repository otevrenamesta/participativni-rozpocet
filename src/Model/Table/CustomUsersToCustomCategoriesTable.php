<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\CustomUsersToCustomCategory;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CustomUsersToCustomCategories Model
 *
 * @property CustomUsersTable&BelongsTo $CustomUsers
 * @property CustomUserCategoriesTable&BelongsTo $CustomUserCategories
 *
 * @method CustomUsersToCustomCategory newEmptyEntity()
 * @method CustomUsersToCustomCategory newEntity(array $data, array $options = [])
 * @method CustomUsersToCustomCategory[] newEntities(array $data, array $options = [])
 * @method CustomUsersToCustomCategory get($primaryKey, $options = [])
 * @method CustomUsersToCustomCategory findOrCreate($search, ?callable $callback = null, $options = [])
 * @method CustomUsersToCustomCategory patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method CustomUsersToCustomCategory[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method CustomUsersToCustomCategory|false save(EntityInterface $entity, $options = [])
 * @method CustomUsersToCustomCategory saveOrFail(EntityInterface $entity, $options = [])
 * @method CustomUsersToCustomCategory[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method CustomUsersToCustomCategory[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method CustomUsersToCustomCategory[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method CustomUsersToCustomCategory[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class CustomUsersToCustomCategoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('custom_users_to_custom_categories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CustomUsers', [
            'foreignKey' => 'custom_user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('CustomUserCategories', [
            'foreignKey' => 'custom_user_category_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['custom_user_id'], 'CustomUsers'), ['errorField' => 'custom_user_id']);
        $rules->add($rules->existsIn(['custom_user_category_id'], 'CustomUserCategories'), ['errorField' => 'custom_user_category_id']);

        return $rules;
    }
}
