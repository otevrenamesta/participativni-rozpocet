<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\District;
use App\Model\Entity\Domain;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Districts Model
 *
 * @property OrganizationsTable&BelongsTo $Organizations
 *
 * @method District newEmptyEntity()
 * @method District newEntity(array $data, array $options = [])
 * @method District[] newEntities(array $data, array $options = [])
 * @method District get($primaryKey, $options = [])
 * @method District findOrCreate($search, ?callable $callback = null, $options = [])
 * @method District patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method District[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method District|false save(EntityInterface $entity, $options = [])
 * @method District saveOrFail(EntityInterface $entity, $options = [])
 * @method District[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method District[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method District[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method District[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class DistrictsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('districts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Josegonzalez/Version.Version');

        $this->belongsTo('Organizations', [
            'foreignKey' => 'organization_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['organization_id'], 'Organizations'), ['errorField' => 'organization_id']);

        return $rules;
    }

    public function getWithOrganization(int $district_id, int $organization_id, array $contain = [])
    {
        return $this->get($district_id, [
            'conditions' => [
                'Districts.organization_id' => $organization_id
            ],
            'contain' => $contain
        ]);
    }
}
