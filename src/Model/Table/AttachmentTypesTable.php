<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\AttachmentType;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AttachmentTypes Model
 *
 * @property AttachmentsTable&HasMany $Attachments
 *
 * @method AttachmentType newEmptyEntity()
 * @method AttachmentType newEntity(array $data, array $options = [])
 * @method AttachmentType[] newEntities(array $data, array $options = [])
 * @method AttachmentType get($primaryKey, $options = [])
 * @method AttachmentType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method AttachmentType patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method AttachmentType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method AttachmentType|false save(EntityInterface $entity, $options = [])
 * @method AttachmentType saveOrFail(EntityInterface $entity, $options = [])
 * @method AttachmentType[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method AttachmentType[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method AttachmentType[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method AttachmentType[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AttachmentTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('attachment_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Attachments', [
            'foreignKey' => 'attachment_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
