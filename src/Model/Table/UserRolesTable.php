<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\UserRole;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\HasMany;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserRoles Model
 *
 * @property OrganizationsToUsersTable&HasMany $OrganizationsToUsers
 *
 * @method UserRole newEmptyEntity()
 * @method UserRole newEntity(array $data, array $options = [])
 * @method UserRole[] newEntities(array $data, array $options = [])
 * @method UserRole get($primaryKey, $options = [])
 * @method UserRole findOrCreate($search, ?callable $callback = null, $options = [])
 * @method UserRole patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method UserRole[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method UserRole|false save(EntityInterface $entity, $options = [])
 * @method UserRole saveOrFail(EntityInterface $entity, $options = [])
 * @method UserRole[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method UserRole[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method UserRole[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method UserRole[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class UserRolesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('user_roles');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('OrganizationsToUsers', [
            'foreignKey' => 'user_role_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
