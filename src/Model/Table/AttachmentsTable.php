<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Attachment;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Behavior\TimestampBehavior;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Attachments Model
 *
 * @property ProjectsTable&BelongsTo $Projects
 * @property AttachmentTypesTable&BelongsTo $AttachmentTypes
 *
 * @method Attachment newEmptyEntity()
 * @method Attachment newEntity(array $data, array $options = [])
 * @method Attachment[] newEntities(array $data, array $options = [])
 * @method Attachment get($primaryKey, $options = [])
 * @method Attachment findOrCreate($search, ?callable $callback = null, $options = [])
 * @method Attachment patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method Attachment[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method Attachment|false save(EntityInterface $entity, $options = [])
 * @method Attachment saveOrFail(EntityInterface $entity, $options = [])
 * @method Attachment[]|ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method Attachment[]|ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method Attachment[]|ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method Attachment[]|ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin TimestampBehavior
 */
class AttachmentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('attachments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('AttachmentTypes', [
            'foreignKey' => 'attachment_type_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['project_id'], 'Projects'), ['errorField' => 'project_id']);
        $rules->add($rules->existsIn(['attachment_type_id'], 'AttachmentTypes'), ['errorField' => 'attachment_type_id']);

        return $rules;
    }

    public function getWithProject(int $attachment_id, int $project_id, array $contain = [])
    {
        return $this->get($attachment_id, [
            'conditions' => [
                'Attachments.' . Attachment::FIELD_PROJECT_ID => $project_id
            ],
            'contain' => $contain
        ]);
    }
}
