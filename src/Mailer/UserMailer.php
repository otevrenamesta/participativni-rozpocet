<?php


namespace App\Mailer;

use App\Model\Entity\User;
use Cake\Mailer\Mailer;

class UserMailer extends Mailer
{

    /**
     * @param User $user
     * @return Mailer
     */
    public function verifyEmail(User $user)
    {
        $this->viewBuilder()->setTemplate('verify_email');

        return $this->setTo($user->email)
            ->setSubject(__('Participativní Rozpočet: Ověření e-mailu'))
            ->setEmailFormat('both')
            ->setViewVars(
                [
                    'code' => $user->getMailVerificationCode(),
                    'user_id' => $user->id
                ]
            );
    }
}
