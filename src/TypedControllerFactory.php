<?php

namespace App;

use Cake\Controller\ControllerFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\InvalidArgumentException;
use ReflectionFunction;
use ReflectionNamedType;

class TypedControllerFactory extends ControllerFactory
{

    /**
     * Invoke a controller's action and wrapping methods.
     *
     * @param mixed $controller The controller to invoke.
     * @return \Psr\Http\Message\ResponseInterface The response
     * @throws \Cake\Controller\Exception\MissingActionException If controller action is not found.
     * @throws \UnexpectedValueException If return value of action method is not null or ResponseInterface instance.
     * @psalm-param \Cake\Controller\Controller $controller
     */
    public function invoke($controller): ResponseInterface
    {
        $result = $controller->startupProcess();
        if ($result instanceof ResponseInterface) {
            return $result;
        }
        $action = $controller->getAction();

        $args = [];
        $reflection = new ReflectionFunction($action);
        $passed = array_values((array)$controller->getRequest()->getParam('pass'));
        foreach ($reflection->getParameters() as $i => $parameter) {
            $position = $parameter->getPosition();
            $hasDefault = $parameter->isDefaultValueAvailable();

            // If there is no type we can't look in the container
            // assume the parameter is a passed param
            $type = $parameter->getType();
            if (!$type) {
                if (count($passed)) {
                    $args[$position] = array_shift($passed);
                } elseif ($hasDefault) {
                    $args[$position] = $parameter->getDefaultValue();
                }
                continue;
            }
            $typeName = $type instanceof ReflectionNamedType ? ltrim($type->getName(), '?') : null;

            // Primitive types are passed args as they can't be looked up in the container.
            // We only handle strings currently.
            if ($typeName === 'string') {
                if (count($passed)) {
                    $args[$position] = array_shift($passed);
                } elseif ($hasDefault) {
                    $args[$position] = $parameter->getDefaultValue();
                }
                continue;
            } elseif ($typeName === 'int') {
                if (count($passed)) {
                    $args[$position] = intval(array_shift($passed));
                } elseif ($hasDefault) {
                    $args[$position] = $parameter->getDefaultValue();
                }
                continue;
            }

            // Check the container and parameter default value.
            if ($typeName && $this->container->has($typeName)) {
                $args[$position] = $this->container->get($typeName);
            } elseif ($hasDefault) {
                $args[$position] = $parameter->getDefaultValue();
            }
            if (!array_key_exists($position, $args)) {
                throw new InvalidArgumentException(
                    "Could not resolve action argument `{$parameter->getName()}`. " .
                    'It has no definition in the container, no passed parameter, and no default value.'
                );
            }
        }
        if (count($passed)) {
            $args = array_merge($args, $passed);
        }
        $controller->invokeAction($action, $args);

        $result = $controller->shutdownProcess();
        if ($result instanceof ResponseInterface) {
            return $result;
        }

        return $controller->getResponse();
    }

}
