<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View;

use App\Model\Entity\User;
use Authentication\View\Helper\IdentityHelper;
use Cake\View\View;

/**
 * Application View
 *
 * Your application's default view class
 *
 * @link https://book.cakephp.org/4/en/views.html#the-app-view
 * @property-read IdentityHelper $Identity
 */
class AppView extends View
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize(): void
    {
        $this->loadHelper('Form', [
            'templates' => 'bootstrap_form',
            'className' => 'App\View\Helper\FormHelper'
        ]);
        $this->loadHelper('Authentication.Identity');
    }

    public function isUser(): bool
    {
        return $this->Identity->isLoggedIn();
    }

    public function getUser(?string $path = null)
    {
        return $this->Identity->get($path);
    }

    public function userHasRole(int $user_role_id): bool
    {
        if (!$this->isUser()) {
            return false;
        }
        return intval($this->getUser(User::FIELD_USER_ROLE_ID)) === $user_role_id;
    }

    public function userHasManagedOrganizations(): bool
    {
        if (!$this->isUser()) {
            return false;
        }
        /** @var User|null $user */
        $user = $this->Identity->get();
        return $user instanceof User && (
                !empty($user->organizations_to_users) ||
                !empty($user->organizations)
            );
    }
}
