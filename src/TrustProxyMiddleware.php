<?php
declare(strict_types=1);

namespace App;

use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class TrustProxyMiddleware implements MiddlewareInterface
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request instanceof ServerRequest) {
            $request->trustProxy = true;
        } else {
            Log::debug('not ServerRequest but ' . get_class($request));
        }

        return $handler->handle($request);
    }
}
