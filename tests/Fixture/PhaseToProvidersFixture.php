<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PhaseToProvidersFixture
 */
class PhaseToProvidersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'phase_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'identity_provider_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'phase_id' => ['type' => 'index', 'columns' => ['phase_id'], 'length' => []],
            'identity_provider_id' => ['type' => 'index', 'columns' => ['identity_provider_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'phase_to_providers_ibfk_2' => ['type' => 'foreign', 'columns' => ['phase_id'], 'references' => ['phases', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'phase_to_providers_ibfk_1' => ['type' => 'foreign', 'columns' => ['identity_provider_id'], 'references' => ['identity_providers', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'phase_id' => 1,
                'identity_provider_id' => 1,
                'modified' => '2020-09-26 10:24:22',
            ],
        ];
        parent::init();
    }
}
