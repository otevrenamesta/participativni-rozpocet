<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectBudgetItemsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectBudgetItemsTable Test Case
 */
class ProjectBudgetItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ProjectBudgetItemsTable
     */
    protected $ProjectBudgetItems;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectBudgetItems',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectBudgetItems') ? [] : ['className' => ProjectBudgetItemsTable::class];
        $this->ProjectBudgetItems = $this->getTableLocator()->get('ProjectBudgetItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectBudgetItems);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
