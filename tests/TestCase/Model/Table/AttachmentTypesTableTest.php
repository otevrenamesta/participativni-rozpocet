<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AttachmentTypesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AttachmentTypesTable Test Case
 */
class AttachmentTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var AttachmentTypesTable
     */
    protected $AttachmentTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.AttachmentTypes',
        'app.Attachments',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('AttachmentTypes') ? [] : ['className' => AttachmentTypesTable::class];
        $this->AttachmentTypes = $this->getTableLocator()->get('AttachmentTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->AttachmentTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
