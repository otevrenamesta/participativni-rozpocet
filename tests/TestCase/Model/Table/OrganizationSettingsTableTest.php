<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationSettingsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationSettingsTable Test Case
 */
class OrganizationSettingsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var OrganizationSettingsTable
     */
    protected $OrganizationSettings;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.OrganizationSettings',
        'app.Organizations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('OrganizationSettings') ? [] : ['className' => OrganizationSettingsTable::class];
        $this->OrganizationSettings = $this->getTableLocator()->get('OrganizationSettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->OrganizationSettings);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
