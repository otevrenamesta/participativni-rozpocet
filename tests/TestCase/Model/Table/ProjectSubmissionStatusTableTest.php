<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectSubmissionStatusTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectSubmissionStatusTable Test Case
 */
class ProjectSubmissionStatusTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectSubmissionStatusTable
     */
    protected $ProjectSubmissionStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectSubmissionStatus',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectSubmissionStatus') ? [] : ['className' => ProjectSubmissionStatusTable::class];
        $this->ProjectSubmissionStatus = $this->getTableLocator()->get('ProjectSubmissionStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectSubmissionStatus);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
