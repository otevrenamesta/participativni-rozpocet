<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectLogsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectLogsTable Test Case
 */
class ProjectLogsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ProjectLogsTable
     */
    protected $ProjectLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectLogs',
        'app.Projects',
        'app.ProjectLogTypes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectLogs') ? [] : ['className' => ProjectLogsTable::class];
        $this->ProjectLogs = $this->getTableLocator()->get('ProjectLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectLogs);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
