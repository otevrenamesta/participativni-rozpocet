<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectsToCategoriesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectsToCategoriesTable Test Case
 */
class ProjectsToCategoriesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var ProjectsToCategoriesTable
     */
    protected $ProjectsToCategories;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectsToCategories',
        'app.Projects',
        'app.ProjectCategories',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectsToCategories') ? [] : ['className' => ProjectsToCategoriesTable::class];
        $this->ProjectsToCategories = $this->getTableLocator()->get('ProjectsToCategories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectsToCategories);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
