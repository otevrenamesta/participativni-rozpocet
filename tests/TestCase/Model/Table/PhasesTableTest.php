<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PhasesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PhasesTable Test Case
 */
class PhasesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var PhasesTable
     */
    protected $Phases;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Phases',
        'app.Appeals',
        'app.PhaseToProviders',
        'app.Votes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Phases') ? [] : ['className' => PhasesTable::class];
        $this->Phases = $this->getTableLocator()->get('Phases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Phases);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
