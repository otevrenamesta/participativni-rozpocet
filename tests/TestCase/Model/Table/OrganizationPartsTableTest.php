<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrganizationPartsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrganizationPartsTable Test Case
 */
class OrganizationPartsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrganizationPartsTable
     */
    protected $OrganizationParts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.OrganizationParts',
        'app.ProjectLogs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('OrganizationParts') ? [] : ['className' => OrganizationPartsTable::class];
        $this->OrganizationParts = $this->getTableLocator()->get('OrganizationParts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->OrganizationParts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
