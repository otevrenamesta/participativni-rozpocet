<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectFinalStatesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectFinalStatesTable Test Case
 */
class ProjectFinalStatesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectFinalStatesTable
     */
    protected $ProjectFinalStates;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ProjectFinalStates',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ProjectFinalStates') ? [] : ['className' => ProjectFinalStatesTable::class];
        $this->ProjectFinalStates = $this->getTableLocator()->get('ProjectFinalStates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ProjectFinalStates);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
