<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IdentityProvidersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IdentityProvidersTable Test Case
 */
class IdentityProvidersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var IdentityProvidersTable
     */
    protected $IdentityProviders;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.IdentityProviders',
        'app.Contacts',
        'app.PhaseToProviders',
        'app.Votes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('IdentityProviders') ? [] : ['className' => IdentityProvidersTable::class];
        $this->IdentityProviders = $this->getTableLocator()->get('IdentityProviders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->IdentityProviders);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
