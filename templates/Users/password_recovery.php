<?php

use App\Model\Entity\User;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $user User
 */
$this->assign('title', 'Zapomenuté heslo');
?>
    <h1><?= $this->fetch('title') ?></h1>

<?php
echo $this->Form->create($user);
echo $this->Form->control('email', ['label' => __('E-mail registrovaného uživatele')]);
echo $this->Form->submit(__('Požádat o obnovení hesla'), ['class' => 'btn btn-warning']);
echo $this->Form->end();
