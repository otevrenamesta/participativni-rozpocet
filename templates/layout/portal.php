<?php
/**
 * @var $this AppView
 */

use App\View\AppView;

?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $this->get('title') ?> - <?= __('Participativní Rozpočet') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('/widgets/common/minireset-v0.0.6.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<?= $this->fetch('content') ?>

</body>
</html>
