<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeal Appeal
 * @var $organization Organization
 * @var $projects array
 * @var $projectStates array
 */

$projectsById = [];
foreach ($projects as $project) {
    // Form->control for associated entities order is not the same as order of projects by evaluation
    $projectsById[$project['project']->id] = $project;
}
$projects = $projectsById;

$this->assign('title', __('Automatické navržení pořadí projektů'));
echo $this->element('datatables');
echo $this->Form->create($appeal);
?>

<div class="card m-2">
    <div class="card-header">
        <h2>
            <?= __('Navržené aktuání pořadí dle dosažených výsledků') ?>
        </h2>
        <?= $this->Form->submit(__('Uložit')) ?>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped datatables">
            <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Název projektu') ?></th>
                <th><?= __('Pořadí') ?></th>
                <th><?= __('Stav projektu?') ?></th>
                <th><?= __('Rozpočet (navržený)') ?></th>
                <th><?= __('Počet pozitivních hlasů') ?></th>
                <th><?= __('Počet negativních hlasů') ?></th>
                <th><?= __('Celkový výsledek hlasování') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php $counter = 0; ?>
            <?php foreach ($appeal->projects as $project): ?>
                <?php $pdata = $projects[$project->id]; ?>
                <tr>
                    <td><?= $project->id ?></td>
                    <td>
                        <?= $project->name ?>
                        <hr/>
                        <?= __('Stav: ') . $project->project_status->name ?>
                        <br/>
                        <?= __('Finální Stav: ') . $project->project_status->project_final_state->name ?>
                        <?php if (!empty($pdata['reason'])): ?>
                            <br/>
                            <?= __('Odůvodnění navrženého: ') . $pdata['reason'] ?>
                        <?php endif; ?>
                    </td>
                    <td data-order="<?= $pdata['order'] > 0 ? (1000 - $pdata['order']) : 0; ?>">
                        <?= $this->Form->hidden(sprintf('projects.%d.id', $counter), ['value' => $project->id]) ?>
                        <?= $this->Form->control(sprintf('projects.%d.final_appeal_order', $counter), ['value' => $pdata['order'], 'label' => false]) ?>

                        <?php if ($project->final_appeal_order !== intval($pdata['order'])): ?>
                            <hr/>
                            <?= __('Původní pořadí: ') . $project->final_appeal_order ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?= $this->Form->control(sprintf('projects.%d.project_status_id', $counter), ['options' => $projectStates, 'label' => false]) ?>
                    </td>
                    <td><?= Number::currency($pdata['total'], 'czk') ?></td>
                    <td><?= $pdata['positive'] ?></td>
                    <td><?= $pdata['negative'] ?></td>
                    <td><?= $pdata['overall'] ?></td>
                </tr>
                <?php $counter++; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php
$this->Form->unlockField('DataTables_Table_0_length');
echo $this->Form->end();
?>

