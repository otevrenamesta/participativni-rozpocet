<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\View\AppView;
use Cake\Collection\Collection;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $appeal Appeal
 * @var $organization Organization
 */

$this->assign('title', sprintf("%s %d", __('Ročník'), $appeal->year));
echo $this->element('datatables');
?>

<ul class="nav nav-tabs" id="index-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="dashboard-tab" data-toggle="tab" href="#dashboard-tab-content" role="tab"
           aria-controls="dashboard-tab-content" aria-selected="true">
            <?= __('Přehled ročníku') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="formalcontrol-tab" data-toggle="tab" href="#formalcontrol-tab-content" role="tab"
           aria-controls="formalcontrol-tab-content" aria-selected="false">
            <?= __('Formální kontrola') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="aproved-tab" data-toggle="tab" href="#aproved-tab-content" role="tab"
           aria-controls="aproved-tab-content" aria-selected="false">
            <?= __('Schválené projekty') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="phases-tab" data-toggle="tab" href="#phases-tab-content" role="tab"
           aria-controls="phases-tab-content" aria-selected="false">
            <?= __('Fáze / Harmonogram') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="appeal-tab" data-toggle="tab" href="#appeal-tab-content" role="tab"
           aria-controls="appeal-tab-content" aria-selected="false">
            <?= __('Nastavení ročníku') ?>
        </a>
    </li>
</ul>

<div class="tab-content" id="index-tabs-content">
    <div class="tab-pane fade show active" id="dashboard-tab-content" role="tabpanel" aria-labelledby="dashboard-tab">
        <div class="card m-2">
            <h2 class="card-header"><?= __('Přehled ročníku') ?></h2>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 table-responsive">
                        <h3><?= __('Zadané projekty') ?></h3>
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <td><?= __('Celkem projektů v databázi') ?></td>
                                <td><?= count($appeal->projects) ?></td>
                            </tr>
                            <tr>
                                <td><?= __('Počet navrhovateli neodeslaných konceptu') ?></td>
                                <td><?= (new Collection($appeal->projects))->filter(function (\App\Model\Entity\Project $project) {
                                        return $project->canProposerSubmit();
                                    })->count() ?></td>
                            </tr>
                            <tr>
                                <td><?= __('Počet odeslaných projektů čekajících na kontrolu formálních náležitostí') ?></td>
                                <td><?= (new Collection($appeal->projects))->filter(function (\App\Model\Entity\Project $project) {
                                        return $project->canFormalControlCheck();
                                    })->count() ?></td>
                            </tr>
                            <tr>
                                <td><?= __('Počet projektů zobrazených v galerii') ?></td>
                                <td><?= (new Collection($appeal->projects))->filter(function (\App\Model\Entity\Project $project) {
                                        return $project->isPublic();
                                    })->count() ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-6 table-responsive">
                        <h3><?= __('Stav zveřejňování projektů') ?></h3>
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <td><?= __('Počet aktuálně hlasovatelných projektů') ?></td>
                                <td><?= (new Collection($appeal->projects))->filter(function (\App\Model\Entity\Project $project) {
                                        return $project->project_status->is_voting_enabled;
                                    })->count() ?></td>
                            </tr>
                            <tr>
                                <td><?= __('Počet projektů u kterých je skryt aktuální výsledek hlasování') ?></td>
                                <td><?= (new Collection($appeal->projects))->filter(function (\App\Model\Entity\Project $project) {
                                        return $project->project_status->are_votes_public;
                                    })->count() ?></td>
                            </tr>
                            <tr>
                                <td><?= __('Počet projektů vrácených navrhovatelům k dopracování') ?></td>
                                <td><?= (new Collection($appeal->projects))->filter(function (\App\Model\Entity\Project $project) {
                                        return $project->project_submission_status_id === \App\Model\Entity\ProjectSubmissionStatus::WAITING_FOR_RE_SUBMISSION;
                                    })->count() ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row bt-1 pt-2">
                    <div class="col-sm-6 table-responsive">
                        <h3><?= __('Počet projektů dle kategorií') ?></h3>
                        <table class="table table-striped">
                            <tbody>
                            <?php foreach ($appeal->getCountByCategory() as $category_name => $count): ?>
                                <?php if ($count < 1) {
                                    continue;
                                } ?>
                                <tr>
                                    <td><?= $category_name ?></td>
                                    <td><?= $count ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-6 table-responsive">
                        <h3><?= __('Počet projektů dle stavů') ?></h3>
                        <table class="table table-striped">
                            <tbody>
                            <?php foreach ($appeal->getCountByStatus() as $status_name => $count): ?>
                                <?php if ($count < 1) {
                                    continue;
                                } ?>
                                <tr>
                                    <td><?= $status_name ?></td>
                                    <td><?= $count ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row bt-1 pt-2">
                    <div class="col-sm-6 table-responsive">
                        <h3><?= __('Podle městské části') ?></h3>
                        <table class="table table-striped">
                            <tbody>
                            <?php foreach ($appeal->getCountByDistrictId() as $district_name => $count): ?>
                                <tr>
                                    <td><?= $district_name ?></td>
                                    <td><?= $count ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-6 table-responsive">
                        <h3><?= __('Podle finálního stavu projektu') ?></h3>
                        <table class="table table-striped">
                            <tbody>
                            <?php foreach ($appeal->getCountByFinalState() as $final_state_name => $count): ?>
                                <tr>
                                    <td><?= $final_state_name ?></td>
                                    <td><?= $count ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="formalcontrol-tab-content" role="tabpanel" aria-labelledby="formalcontrol-tab">

        <div class="card m-2">
            <h2 class="card-header">
                <?= __('Formální kontrola') ?>
            </h2>
            <div class="card-body table-responsive">
                <table class="table table-bordered datatables">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('ID') ?></th>
                        <th><?= __('Název') ?></th>
                        <th><?= __('Kategorie') ?></th>
                        <th><?= __('Odesláno') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($appeal->projects as $project): ?>
                        <?php
                        if ($project->isFormallyAccepted()) {
                            continue;
                        }
                        ?>
                        <tr>
                            <td><?= $project->id ?></td>
                            <td><?= $this->Html->link($project->name, ['action' => 'detail', 'controller' => 'Projects', 'project_id' => $project->id, 'appeal_id' => $project->appeal_id, 'organization_id' => $organization->id,]) ?></td>
                            <td><?= $project->getCategories(true) ?></td>
                            <td><?= $project->modified ?></td>
                            <td>
                                <?php
                                if ($project->canFormalControlCheck()) {
                                    echo $this->Html->link(
                                        __('Provést kontrolu návrhu'),
                                        ['controller' => 'Projects', 'action' => 'formalControl', 'organization_id' => $organization->id, 'project_id' => $project->id, 'appeal_id' => $project->appeal_id],
                                        ['class' => 'btn btn-warning']
                                    );
                                } else {
                                    echo $project->project_submission_status->name;
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="aproved-tab-content" role="tabpanel" aria-labelledby="aproved-tab">

        <div class="card m-2">
            <div class="card-header">
                <h2>
                    <?= __('Schválené Projekty') ?>
                </h2>
                <?= $this->Html->link(__('Pomocník: hromadné nastavení pořadí vítězných projektů'), ['_name' => 'appeal_order_projects', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered datatables">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('ID') ?></th>
                        <th><?= __('Název') ?></th>
                        <th><?= __('Kategorie') ?></th>
                        <th><?= __('Stav') ?></th>
                        <th><?= __('Hlasování') ?></th>
                        <th><?= __('Hlasovatelný') ?></th>
                        <th><?= __('Veřejný') ?></th>
                        <th><?= __('Vítězné pořadí') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($appeal->projects as $project): ?>
                        <?php
                        if (!$project->isFormallyAccepted()) {
                            continue;
                        }
                        ?>
                        <tr>
                            <td><?= $project->id ?></td>
                            <td><?= h(trim($project->name)) ?></td>
                            <td>
                                <?php
                                foreach ($project->project_categories as $category) {
                                    echo $category->name . '<br/>';
                                }
                                ?>
                            </td>
                            <td>
                                <?= $project->project_status->name ?>
                                (<?= $project->project_submission_status_id ?>)
                            </td>
                            <td>
                                <?= sprintf('%s: %d<br/>%s: %d<br/><strong>%s: %d</strong>', __('Pozitivní'), $project->cache_votes_positive, __('Negativní'), $project->cache_votes_negative, __('Výsledek'), $project->cache_votes_overall) ?>
                            </td>
                            <td>
                                <?= $project->project_status->is_voting_enabled ? 'Hlasovatelný' : 'Nelze hlasovat' ?>
                            </td>
                            <td>
                                <?= $project->project_status->is_public ? 'Veřejný' : 'Neveřejný' ?>
                            </td>
                            <td>
                                <?php
                                if ($project->project_status->project_final_status_id !== \App\Model\Entity\ProjectFinalState::STATE_FINAL_SUCCESS) {
                                    echo __('Projekt není vítězný');
                                } else {
                                    echo $project->final_appeal_order;
                                }
                                ?>
                            </td>
                            <td>
                                <?= $this->Html->link(__('Detail'), ['action' => 'detail', 'controller' => 'Projects', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id], ['class' => 'text-success']) ?>
                                ,
                                <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'controller' => 'Projects', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id], ['class' => 'text-primary']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <?= $this->Html->link(__('Přidat projekt'), ['action' => 'addModify', 'controller' => 'Projects', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="phases-tab-content" role="tabpanel" aria-labelledby="phases-tab">

        <div class="card m-2">
            <h2 class="card-header"><?= __('Fáze / Harmonogram') ?></h2>
            <div class="card-body table-responsive">
                <div class="alert alert-info">
                    <?= __('Jednotlivé fáze, tak jak jsou nezbytné pro celý průběh ročníku participativního rozpočtu') ?>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th><?= __('Od (včetně)') ?></th>
                        <th><?= __('Do (včetně)') ?></th>
                        <th><?= __('Název') ?></th>
                        <th><?= __('Popis') ?></th>
                        <th><?= __('Minimální počet elektronických hlasů pro úspěch projektu') ?></th>
                        <th><?= __('Lze navrhovat projekty?') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($appeal->phases as $phase): ?>
                        <tr>
                            <td><?= $phase->date_start->format('d.m.Y') ?></td>
                            <td><?= $phase->date_end->format('d.m.Y') ?></td>
                            <td><?= $phase->name ?></td>
                            <td><?= $phase->description ?></td>
                            <td><?= empty($phase->identity_providers) ?
                                    __('V této fázi nelze hlasovat') :
                                    ($phase->minimum_votes_count > 0 ? $phase->minimum_votes_count : __('Minimální počet hlasů není vyžadován')) ?></td>
                            <td><?= $phase->allows_proposals_submission ? __('ANO') : __('V této fázi nelze navrhovat projekty') ?></td>
                            <td>
                                <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'controller' => 'Phases', 'appeal_id' => $phase->appeal_id, 'phase_id' => $phase->id, 'organization_id' => $organization->id]) ?>
                                .
                                <?= $this->Html->link(__('Smazat'), ['action' => 'delete', 'controller' => 'Phases', 'appeal_id' => $phase->appeal_id, 'phase_id' => $phase->id, 'organization_id' => $organization->id], ['class' => 'text-danger']) ?>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <?= $this->Html->link(__('Přidat další fázi'), ['action' => 'addModify', 'controller' => 'Phases', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="appeal-tab-content" role="tabpanel" aria-labelledby="appeal-tab">

        <div class="card m-2">
            <h2 class="card-header"><?= $this->fetch('title') ?></h2>
            <div class="card-body">
                <?= __('Odkaz na informace o výzvě') ?>: <?= $appeal->info_link ?>
                <br/>
                <?= __('Rozpočet tohoto ročníku pro podporu projektů') ?>
                : <?= Number::currency($appeal->budget_czk, 'CZK') ?>
            </div>
            <div class="card-footer">
                <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
                <?= $this->Html->link(__('Vytvořit kopii'), ['action' => 'makeCopy', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>




