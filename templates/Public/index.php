<?php
/**
 * @var $this AppView
 * @var $organization null|Organization
 * @var $organizations Organization[]
 */

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

/** @var null|Organization $organization */
$organization = $this->Identity->get('organizations.0');
$hasOrganization = $organization instanceof Organization;

$this->assign('title', __('Participativní rozpočet'));
?>
<script type="text/javascript">
    function loginFirst(ev) {
        alert('Musíte se nejdříve přihlásit');
        if (ev) {
            ev.preventDefault();
        }
    }
</script>

<ul class="nav nav-tabs" id="index-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="user-tab" data-toggle="tab" href="#user-tab-content" role="tab"
           aria-controls="user-tab-content" aria-selected="true">
            <?= __('Účastník participativního rozpočtu') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="manager-tab" data-toggle="tab" href="#manager-tab-content" role="tab"
           aria-controls="manager-tab-content" aria-selected="false">
            <?= __('Koordinátor participativního rozpočtu') ?>
        </a>
    </li>
</ul>

<div class="tab-content" id="index-tabs-content">
    <div class="tab-pane fade show active" id="user-tab-content" role="tabpanel" aria-labelledby="user-tab">
        <div class="card m-2">
            <h2 class="card-header">
                <?= __('Jak se účastnit participativního rozpočtu') ?>
            </h2>
            <div class="card-body">
                <?= __('Na tomto portále se můžete účastnit participativního rozpočtu jednotlivých obcí, které jsou zde registrovány') ?>
                <br/>
                <?= __('Pro hlasování v jednotlivých projektech se můžete, ale nemusíte registrovat. Hlasování můžete dle nastavení dané organizace provést i bez ověření a/nebo uživatelského účtu') ?>
                <br/><br/>
                <?= __('Pokud však chcete navrhovat projekty, je registrace platnou e-mailovou adresou nezbytná, aby s vámi daná obec následně mohla komunikovat o stavu vašeho navrženého projektu') ?>
            </div>
        </div>
        <div class="card m-2">
            <h2 class="card-header">
                <?= __('Obce, které na tomto portále spravují svůj participativní rozpočet') ?>
            </h2>
            <div class="card-body">
                <ul class="list-group">
                    <?php foreach ($organizations as $organization): ?>
                        <li class="list-group-item">
                            <?=
                            $this->Html->link(
                                h($organization->name),
                                $organization->getSettingValue(OrganizationSetting::ORGANIZATION_PRIMARY_PORTAL_URL) ?? ['_name' => 'public_organization_detail', 'organization_id' => $organization->id]
                            )
                            ?>
                            <?php
                            $contactMail = $organization->getSettingValue(OrganizationSetting::ORGANIZATION_CONTACT_EMAIL);
                            if (!empty($contactMail)) {
                                echo sprintf(', %s: %s', __('e-mail'), $contactMail);
                            }
                            ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="manager-tab-content" role="tabpanel" aria-labelledby="manager-tab">
        <div class="card m-2">
            <h2 class="card-header"><?= __('Jak si vytvořit svůj participativní rozpočet?') ?></h2>
            <div class="card-body">
                <ol>
                    <li><?= $this->Html->link(__('Registrujte se na této stránce a přihlaste se'), ['_name' => 'register']) ?></li>
                    <li><?= $this->Html->link(__('Vytvořte si novou organizaci (např. obec)'), ['_name' => 'organization_add']) ?></li>
                    <li><?= $this->Html->link(__('Přidejte si ročník a jednotlivé fáze participativního rozpočtu'), $hasOrganization ? ['_name' => 'appeal_add', 'organization_id' => $organization->id] : 'javascript:loginFirst();') ?></li>
                    <li><?= $this->Html->link(__('Pokud nechcete vlastní webové stránky o participativním rozpočtu, můžete celý proces realizovat zde'), $hasOrganization ? ['_name' => 'public_organization_detail', 'organization_id' => $organization->id] : 'javascript:loginFirst();') ?></li>
                    <li><?= sprintf(
                            __('Pokud chcete participativní rozpočet prezentovat na svých webových stránkách, použijte naše %s nebo si udělejte integraci sami pomocí %s'),
                            $this->Html->link(__('Widgety'), ['_name' => 'public_widgets']),
                            $this->Html->link(__('API'), ['_name' => 'api_info'])
                        ) ?></li>
                </ol>
            </div>
        </div>

        <div class="card m-2">
            <h2 class="card-header"><?= __('FAQ: Často kladené dotazy') ?></h2>
            <div class="card-body table-responsive">
                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('Otázka') ?></th>
                        <th><?= __('Odpověď') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $faqs = [
                        [
                            'Kolik to stojí&nbsp;?',
                            'Software je zdarma ke stažení na ' . $this->Html->link('Gitlab', 'https://gitlab.com/otevrenamesta/participativni-rozpocet') . ', pokud jej ale chcete využívat jako službu, tak pro členy spolku Otevřených Měst je zdarma, pro ostatní provozní poplatek 1.000 Kč ročně',
                        ],
                        [
                            'Kdo může hlasovat o navržených projektech / navrhovat projekty&nbsp;?',
                            'V každé fázi, ve které lze hlasovat nebo navrhovat projekty, si sami nastavíte povolené způsoby ověření uživatelů.<br/>Podporujeme jak hlasování bez ověření, tak ověření e-mailem nebo i <strong>eIdentita (NIA) s ověřením věku a bydliště hlasujícího občana</strong>'
                        ],
                        [
                            'Chtěli bychom rozpočet na vlastní doméně, ale nechceme si webové stránky dělat sami',
                            'Pokud máte vlastní doménu, na které chcete s občany komunikovat, například participativni-rozpocet.medlanky.cz, stačí ji nasměrovat na naše servery a doménu si přidat k vlastní organizaci v její administraci, vše ostatní již funguje automaticky'
                        ]
                    ];
                    foreach ($faqs as $faq):
                        ?>
                        <tr>
                            <td class="font-weight-bold"><?= $faq[0] ?></td>
                            <td><?= $faq[1] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
