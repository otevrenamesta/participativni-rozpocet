<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectStatus;
use App\Model\Entity\ProjectSubmissionStatus;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $project Project
 * @var $organization Organization
 * @var $appeal Appeal
 * @var $project_states ProjectStatus[]
 */
$this->assign('title', __('Formální kontrola'));

echo $this->Form->create($project);
?>

    <div class="card m-2">
        <h2 class="card-header"><?= $this->fetch('title') ?></h2>
        <div class="card-body">
            <?php
            echo $this->Form->control(Project::FIELD_PROJECT_SUBMISSION_STATUS_ID, [
                'type' => 'radio',
                'label' => __('Rozhodnutí o výsledku formální kontroly návrhu'),
                'options' => [
                    ProjectSubmissionStatus::WAITING_FOR_RE_SUBMISSION => __('Žadatel musí opravit formální nedostatky'),
                    ProjectSubmissionStatus::FORMAL_CONTROL_ACCEPTED => __('Vyhovuje formálním požadavkům'),
                    ProjectSubmissionStatus::WAITING_FOR_FORMAL_CHECK => __('Čeká na formální kontrolu'),
                ]
            ]);
            echo $this->Form->control('formal_comment', [
                'label' => __('Komentář formální kontroly (zde můžete navrhovateli upřesnit co má napravit)'),
                'type' => 'textarea',
                'data-use-quill' => 'data-use-quill'
            ]);
            echo $this->Form->control(Project::FIELD_PROJECT_STATUS_ID, [
                'label' => __('Volitelně můžete změnit aktuální stav projektu'),
                'options' => $project_states
            ]);
            ?>
        </div>
        <div class="card-footer">
            <?= $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

<?php
echo $this->Form->end();
echo $this->element('project_card', ['project' => $project, 'appeal' => $appeal, 'organization' => $organization]);
