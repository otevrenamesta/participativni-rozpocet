<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Attachment;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Organization;
use App\Model\Entity\Phase;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectBudgetItem;
use App\Model\Entity\ProjectLog;
use App\Model\Entity\ProjectLogType;
use App\Model\Entity\ProjectSubmissionStatus;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $project Project
 * @var $appeal Appeal
 * @var $organization Organization
 * @var $emptyLog ProjectLog
 * @var $emptyBudgetItem ProjectBudgetItem
 * @var $emptyAttachment Attachment
 * @var $attachmentTypes array
 * @var $parts array
 */

$this->assign('title', h($project->name));
?>


<ul class="nav nav-tabs" id="index-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="basics-tab" data-toggle="tab" href="#basics-tab-content" role="tab"
           aria-controls="basics-tab-content" aria-selected="true">
            <?= __('Základní informace') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="budget-tab" data-toggle="tab" href="#budget-tab-content" role="tab"
           aria-controls="budget-tab-content" aria-selected="false">
            <?= __('Rozpočet projektu') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="attachments-tab" data-toggle="tab" href="#attachments-tab-content" role="tab"
           aria-controls="attachments-tab-content" aria-selected="false">
            <?= __('Přílohy / Fotogalerie') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="votes-tab" data-toggle="tab" href="#votes-tab-content" role="tab"
           aria-controls="votes-tab-content" aria-selected="false">
            <?= __('Detaily hlasování') ?>
        </a>
    </li>

    <?php foreach (ProjectLogType::ALL_TYPES as $logTypeId): ?>
        <li class="nav-item">
            <a class="nav-link" id="logs-<?= $logTypeId ?>-tab" data-toggle="tab"
               href="#logs-<?= $logTypeId ?>-tab-content" role="tab"
               aria-controls="logs-<?= $logTypeId ?>-tab-content" aria-selected="false">
                <?= ProjectLogType::getLabel($logTypeId) ?>
            </a>
        </li>
    <?php endforeach; ?>


    <li class="nav-item">
        <a class="nav-link" id="versions-tab" data-toggle="tab" href="#versions-tab-content" role="tab"
           aria-controls="versions-tab-content" aria-selected="false">
            <?= __('Historie změn projektu') ?>
        </a>
    </li>
</ul>


<div class="tab-content" id="index-tabs-content">
    <div class="tab-pane fade show active" id="basics-tab-content" role="tabpanel" aria-labelledby="basics-tab">

        <div class="card m-2">
            <div class="card-header">
                <h2>
                    <?= $this->fetch('title') ?>
                </h2>
                <?php
                echo $this->Html->link(__('Upravit'), ['action' => 'addModify', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id], ['class' => 'btn btn-success']);
                ?>
            </div>
            <div class="card-header">
                <h3><?= __('Hlasování') ?>: </h3>
                <?= sprintf('%s: %d, %s: %d, %s: %d', __('Pozitivní'), $project->cache_votes_positive, __('Negativní'), $project->cache_votes_negative, __('Výsledek'), $project->cache_votes_overall) ?>
            </div>
            <div class="card-header">
                <h3><?= __('Rychlá změna stavu projektu') ?></h3>
                <?php
                foreach ($organization->project_status as $project_status) {
                    echo $this->Form->postLink(
                        $project_status->name,
                        ['action' => 'addModify', 'organization_id' => $organization->id, 'appeal_id' => $project->appeal_id, 'project_id' => $project->id],
                        [
                            'class' => 'btn m-2',
                            'style' => 'background-color: #' . $project_status->color . '; color: ' . getContrastColor($project_status->color),
                            'data' => [
                                'project_status_id' => $project_status->id,
                            ]
                        ]
                    );
                }
                ?>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('Popis') ?></th>
                        <th><?= __('Obsah') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $listing = [
                        'project_status.name' => __('Stav projektu'),
                        'description' => __('Popis'),
                        'progress' => __('Postup realizace projektu (%)'),
                        'address' => __('Adresa / Místo realizace'),
                        'district.name' => __('Městská část'),
                        'public_interest' => __('Veřejný přínos projektu'),
                        'gps_latitude' => __('GPS Latitude'),
                        'gps_longitude' => __('GPS Longitude')
                    ];
                    foreach ($listing as $path => $description):
                        ?>
                        <tr>
                            <td><?= $description ?></td>
                            <td><?= Hash::get($project, $path) ?></td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    <?php if (!$project->has_no_location): ?>
                        <tr>
                            <td><?= __('Umístění projektu') ?></td>
                            <td>
                                <?= $this->element('map_display_only', ['lat' => $project->gps_latitude, 'lon' => $project->gps_longitude, 'organization' => $organization]) ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td><?= __('Kategorie') ?></td>
                        <td>
                            <?php
                            foreach ($project->project_categories as $category) {
                                echo $category->name . ', ';
                            }
                            ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="budget-tab-content" role="tabpanel" aria-labelledby="budget-tab">

        <div class="card m-2">
            <div class="card-header">
                <h2><?= __('Rozpočet projektu') ?></h2>
                <?= $this->Html->link(__('Přidat položku'), ['action' => 'addModifyBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="card-body table-responsive">
                <?php
                $budgets = [
                    __('Navržený') => false,
                    __('Schválený') => true,
                ];
                foreach ($budgets as $title => $approvedState):
                    ?>
                    <h3><?= $title ?></h3>
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th><?= __('Položka') ?></th>
                            <th><?= __('Položková cena') ?></th>
                            <th><?= __('Počet položek') ?></th>
                            <th><?= __('Cena celkem') ?></th>
                            <th><?= __('Akce') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($project->project_budget_items as $item): ?>
                            <?php if ($item->is_final !== $approvedState) {
                                continue;
                            } ?>
                            <tr>
                                <td><?= $item->description ?></td>
                                <td><?= Number::currency($item->item_price, 'CZK') ?></td>
                                <td><?= $item->item_count ?></td>
                                <td><?= Number::currency($item->total_price, 'CZK') ?></td>
                                <td>
                                    <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'item_id' => $item->id], ['class' => 'text-success']) ?>
                                    ,
                                    <?= $this->Html->link(__('Smazat'), ['action' => 'deleteBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'item_id' => $item->id], ['class' => 'text-danger']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endforeach; ?>
            </div>
            <div class="card-footer" id="rozpocet">
                <strong><?= __('Rychlé přidání položky') ?></strong>
                <?php
                echo $this->Form->create($emptyBudgetItem, [
                    'url' => ['action' => 'addModifyBudgetItem', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, '#' => 'rozpocet']
                ]);

                echo $this->Form->control('description', ['label' => __('Popis položky'), 'type' => 'string']);
                echo $this->Form->control('item_price', ['label' => __('Cena za 1 kus (Kč)'), 'min' => 1, 'step' => 1, 'type' => 'number']);
                echo $this->Form->control('item_count', ['label' => __('Počet kusů')]);
                echo $this->Form->control('is_final', ['type' => 'checkbox', 'label' => __('Zaškrtněte pokud se jedná o položku finální, vysoutěženou při realizaci')]);

                echo $this->Form->submit(__('Přidat'));
                echo $this->Form->end();
                ?>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="attachments-tab-content" role="tabpanel" aria-labelledby="attachments-tab">

        <div class="card m-2">
            <div class="card-header">
                <h2><?= __('Přílohy') ?></h2>
                <?= $this->Html->link(__('Přidat přílohu'), ['action' => 'addModifyAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('Název') ?></th>
                        <th><?= __('Typ přílohy') ?></th>
                        <th><?= __('Velikost') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($project->attachments as $attachment): ?>
                        <tr>
                            <td><?= sprintf("%s%s", $attachment->title, $attachment->id === $project->title_image_id ? __(' (titulní foto)') : '') ?></td>
                            <td><?= AttachmentType::getLabel($attachment->attachment_type_id) ?></td>
                            <td><?= Number::toReadableSize($attachment->filesize) ?></td>
                            <td>
                                <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'attachment_id' => $attachment->id], ['class' => 'text-success']) ?>
                                ,
                                <?= $this->Html->link(__('Smazat'), ['action' => 'deleteAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'attachment_id' => $attachment->id], ['class' => 'text-danger']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer" id="prilohy">
                <strong><?= __('Rychlé přidání přílohy') ?></strong>
                <?php
                echo $this->Form->create($emptyAttachment, [
                    'type' => 'file',
                    'url' => ['action' => 'addModifyAttachment', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, '#' => 'prilohy']
                ]);

                echo $this->Form->control('title', ['label' => __('Název přílohy'), 'type' => 'string']);
                echo $this->Form->control('attachment_type_id', ['label' => __('Typ přílohy'), 'options' => $attachmentTypes]);
                echo $this->Form->control('filedata', ['type' => 'file', 'label' => __('Soubor')]);
                echo $this->Form->control('is_title_image', ['label' => __('Zaškrtněte pokud je příloha titulním obrázkem projektu'), 'type' => 'checkbox']);

                echo $this->Form->submit(__('Přidat'));
                echo $this->Form->end();
                ?>
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="votes-tab-content" role="tabpanel" aria-labelledby="votes-tab-content">
        <div class="card m-2">
            <h2 class="card-header"><?= __('Detaily hlasování') ?></h2>
            <div class="card-body table-responsive">
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('Fáze projektu') ?></th>
                        <th><?= __('Pozitivní hlasy') ?></th>
                        <th><?= __('Negativní hlasy') ?></th>
                        <th><?= __('Výsledek hlasování') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    list($breakdown, $overall) = $project->getVotesBreakdown();
                    foreach ($breakdown as $phase_id => $phase_stats):
                        ?>
                        <tr>
                            <td><?= $appeal->getPhaseById($phase_id, Phase::FIELD_NAME) ?></td>
                            <td><?= $phase_stats['positive'] ?></td>
                            <td><?= $phase_stats['negative'] ?></td>
                            <td><?= $phase_stats['positive'] - $phase_stats['negative'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot class="thead-dark">
                    <tr>
                        <th><?= __('Celkem') ?></th>
                        <th><?= $overall['positive'] ?></th>
                        <th><?= $overall['negative'] ?></th>
                        <th><?= $overall['positive'] - $overall['negative'] ?></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php foreach (ProjectLogType::ALL_TYPES as $logTypeId): ?>
        <div class="tab-pane fade" id="logs-<?= $logTypeId ?>-tab-content" role="tabpanel"
             aria-labelledby="logs-<?= $logTypeId ?>-tab">

            <div class="card m-2">
                <div class="card-header">
                    <h2><?= ProjectLogType::getLabel($logTypeId) ?></h2>
                    <?= $this->Html->link(__('Přidat nový záznam'), ['action' => 'addModifyLog', 'log_type' => $logTypeId, 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th><?= __('Útvar') ?></th>
                            <th><?= __('Nadpis') ?></th>
                            <th><?= __('Text') ?></th>
                            <th><?= __('Platné od') ?></th>
                            <th><?= __('Akce') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($project->getLogsByType($logTypeId) as $log): ?>
                            <tr>
                                <td><?= $log->organization_part->name ?? '' ?></td>
                                <td><?= $log->title ?></td>
                                <td><?= $log->description ?></td>
                                <td><?= $log->date_when->nice() ?></td>
                                <td>
                                    <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyLog', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'log_type' => $log->project_log_type_id, 'log_id' => $log->id], ['class' => 'text-success']) ?>
                                    ,
                                    <?= $this->Html->link(__('Smazat'), ['action' => 'deleteLog', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'log_type' => $log->project_log_type_id, 'log_id' => $log->id], ['class' => 'text-danger']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer" id="logs<?= $logTypeId ?>">
                    <strong><?= __('Rychlé přidání záznamu') ?></strong>
                    <?php
                    echo $this->Form->create($emptyLog, [
                        'url' => ['action' => 'addModifyLog', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, 'log_type' => $logTypeId, '#' => 'logs' . $logTypeId]
                    ]);

                    echo $this->Form->control('organization_part_id', ['label' => __('Útvar'), 'empty' => __('Můžete ponechat prázdné'), 'options' => $parts]);
                    echo $this->Form->control('title', ['label' => __('Nadpis')]);
                    echo $this->Form->control('description', ['label' => __('Obsah'), 'type' => 'string']);
                    echo $this->Form->control('date_when', ['label' => __('Platné od')]);

                    echo $this->Form->submit(__('Přidat'));
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>


    <div class="tab-pane fade" id="versions-tab-content" role="tabpanel" aria-labelledby="versions-tab-content">
        <div class="card m-2">
            <h2 class="card-header"><?= __('Historie změn projektu') ?></h2>
            <div class="card-body table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= __('Datum/čas') ?></th>
                        <th><?= __('Provedeno kým') ?></th>
                        <th><?= __('Seznam změn') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $last_version = null;
                    /** @var Project[] $versions */
                    $versions = $project->versions();
                    foreach ($versions as $version):

                        $this_version = [];
                        $blacklist = [
                            'created',
                            'modified',
                            'version_id',
                            'version_user_id',
                            'version_created'
                        ];
                        $key_names = [
                            'appeal_id' => __('Ročník'),
                            'title_image_id' => __('Titulní obrázek'),
                            'proposed_by_contact_id' => __('Navrhovatel'),
                            'project_status_id' => __('Stav projektu'),
                            'project_submission_status_id' => __('Stav odeslání projektu'),
                            'name' => __('Název projektu'),
                            'description' => __('Popis projektu'),
                            'annotation' => __('Anotace'),
                            'public_interest' => __('Veřejný přínos projektu'),
                            'formal_comment' => __('Komentář koordinátora / formální kontroly'),
                            'gps_latitude' => __('GPS Latitude'),
                            'gps_longitude' => __('GPS Longitude'),
                            'address' => __('Adresa'),
                            'has_no_location' => __('Projekt má umístění'),
                            'district_id' => __('Městská část / obvod'),
                            'progress' => __('Postup realizace'),
                            'promo_video_url' => __('URL adresa promo videa'),
                        ];
                        $handlers = [
                            'appeal_id' => function ($appeal_id) use ($appeal) {
                                return $appeal->year;
                            },
                            'contact_id' => function ($contact_id) use ($project) {
                                return $project->contact->name;
                            },
                            'project_status_id' => function ($status_id) use ($organization) {
                                foreach ($organization->project_status as $status) {
                                    if ($status->id === $status_id) {
                                        return $status->name;
                                    }
                                }
                                return null;
                            },
                            'project_submission_status_id' => function ($submission_status_id) {
                                return ProjectSubmissionStatus::getLabel($submission_status_id);
                            },
                            'district_id' => function ($district_id) use ($organization) {
                                foreach ($organization->districts as $district) {
                                    if ($district->id === $district_id) {
                                        return $district->name;
                                    }
                                }
                                return null;
                            }
                        ];
                        foreach ($version->getVisible() as $field_name) {
                            if (in_array($field_name, $blacklist)) {
                                continue;
                            }
                            $this_version[$field_name] = $version->get($field_name);
                        }
                        if ($last_version === null) {
                            $last_version = $this_version;
                            $differences = $this_version;
                        } else {
                            $differences = array_diff_assoc($last_version, $this_version);
                            $last_version = $this_version;
                        }

                        ?>
                        <tr>
                            <td><?= $version->version_created->toDateTimeString() ?></td>
                            <td><?= $version->version_user_id ?></td>
                            <td>
                                <ul>
                                    <?php
                                    foreach ($differences as $key => $value) {
                                        printf('<li>%s: %s</li>', $key_names[$key] ?? $key, isset($handlers[$key]) ? $handlers[$key]($value) : $value);
                                    }
                                    ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
