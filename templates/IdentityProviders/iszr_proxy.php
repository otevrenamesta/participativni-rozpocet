<?php

use App\Controller\Component\VotingComponent;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $component VotingComponent
 */
?>
<div class="row">
    <div class="col-md-8 offset-md-2 p-4 mt-4 bg-light">
        <div class="h2">
            <?= __('Vyberte prosím typ dokladu a opište jeho číslo') ?>
        </div>
        <hr/>
        <?php
        echo $this->Form->create(null, [
            'url' => [
                '_name' => 'voting_interactive',
                'provider_id' => $component->getIdentityProvider()->getProviderId(),
                'otp' => $component->getOTP(),
            ]
        ]);

        echo $this->Form->control('document_type', ['label' => __('Typ dokladu'), 'required' => true, 'options' => [
            'ID' => __('Občanský průkaz'),
            'P' => __('Cestovní pas'),
            //'IR' => __('Povolení k pobytu')
        ]]);
        echo $this->Form->control('document_id', ['type' => 'text', 'label' => __('Číslo dokladu'), 'required' => true]);

        echo $this->Form->submit(__('Vložit hlas'));
        echo $this->Form->end();
        ?>
        <div class="alert alert-warning mt-2 table-responsive">
            Pro testování lze použít následující
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Typ dokladu</th>
                    <th>Číslo dokladu</th>
                    <th>Očekávaný výsledek</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Občanský průkaz</td>
                    <td>111111984</td>
                    <td>Není občanem se správným trvalým pobytem</td>
                </tr>
                <tr>
                    <td>Občanský průkaz</td>
                    <td>111111985</td>
                    <td>Je občanem se správným trvalým pobytem</td>
                </tr>
                <tr>
                    <td>Cestovní pas</td>
                    <td>11112057</td>
                    <td>Není občanem se správným trvalým pobytem</td>
                </tr>
                <tr>
                    <td>Cestovní pas</td>
                    <td>11112062</td>
                    <td>Je občanem se správným trvalým pobytem</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
