<?php

use App\Controller\Component\VotingComponent;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $component VotingComponent
 */
?>
<div class="row">
    <div class="col-md-8 offset-md-2 p-4 mt-4 bg-light">
        <div class="h2">
            <?= __('Přihlaste se prosím jménem a heslem, které vám bylo poskytnuto') ?>
        </div>
        <hr/>
        <?php
        echo $this->Form->create(null, [
            'url' => [
                '_name' => 'voting_interactive',
                'provider_id' => $component->getIdentityProvider()->getProviderId(),
                'otp' => $component->getOTP(),
            ]
        ]);

        echo $this->Form->control('username', ['type' => 'email', 'label' => __('Uživatelské jméno'), 'required' => true]);
        echo $this->Form->control('password', ['type' => 'password', 'label' => __('Heslo'), 'required' => true]);

        echo $this->Form->submit(__('Přihlásit'));
        echo $this->Form->end();
        ?>
    </div>
</div>
