<?php

use App\Controller\Component\VotingComponent;
use App\IdentityProvider\WithouAuthenticationProvider;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $component VotingComponent
 */
?>
<div class="row">
    <div class="col-md-8 offset-md-2 p-4 mt-4 bg-light">
        <div class="h2">
            <?= __('Opište prosím kód z obrázku, abychom měli jistotu, že nejste robot') ?>
        </div>
        <hr/>
        <?php
        echo $this->Form->create(null, [
            'url' => [
                '_name' => 'voting_interactive',
                'provider_id' => $component->getIdentityProvider()->getProviderId(),
                'otp' => $component->getOTP(),
            ]
        ]);
        ?>
        <div style="color: white; background-color: black; text-align: center; font-size: 3rem;">
            <?= $component->retrieve(WithouAuthenticationProvider::PERSIST_CAPTCHA) ?>
        </div>
        <?php
        echo $this->Form->control('captcha', ['type' => 'text', 'label' => __('Ověřovací kód'), 'required' => true]);
        echo $this->Form->submit(__('Hlasovat!'));
        echo $this->Form->end();
        ?>
    </div>
</div>
