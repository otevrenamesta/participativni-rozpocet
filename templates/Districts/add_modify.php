<?php

use App\Model\Entity\District;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $district District
 */

$this->assign('title', $district->isNew() ? __('Přidání nové městské části/obvodu') : __('Úprava městské části/obvodu'));
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($district);
        echo $this->Form->control('name', ['label' => __('Název městské části/obvoud')]);
        echo $this->Form->control('ui_momc', ['label' => __('Kód městského obvodu/městské části z číselníku RUIAN UI_MOMC')]);
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
