<?php

use App\Model\Entity\CustomUser;
use App\Model\Entity\CustomUserCategory;
use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $users CustomUser[]
 * @var $categories CustomUserCategory[]
 */

$this->assign('title', __('Vlastní uživatelské kmeny'));
?>
<div class="card m-2">
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Vlastní uživatelské kmeny, jsou uživatelé, kteří se nemusí registrovat a jejich identifikace k hlasování je jim vydávána jiným způsobem (např. emailem nebo osobně na úřadě)') ?>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Vytvořit novou skupinu uživatel'), ['action' => 'addModifyCategory', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php foreach ($categories as $category): ?>
    <div class="card m-2">
        <div class="card-header">
            <div class="row">
                <div class="col-8">

                    <h2><?= h($category->name) ?></h2>
                </div>
                <div class="col text-right">
                    <?= $this->Html->link(__('Smazat skupinu vč. uživatel'), ['action' => 'deleteCategory', 'organization_id' => $organization->id, 'category_id' => $category->id], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= __('Uživatelské jméno') ?></th>
                    <th><?= __('Heslo') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($category->custom_users as $user): ?>
                    <tr>
                        <td><?= $user->username ?></td>
                        <td><?= $user->password ?></td>
                        <td>
                            <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'organization_id' => $organization->id, 'user_id' => $user->id], ['class' => 'text-primary']) ?>
                            ,
                            <?= $this->Html->link(__('Smazat'), ['action' => 'delete', 'organization_id' => $organization->id, 'user_id' => $user->id], ['class' => 'text-danger']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <?= $this->Html->link(__('Přidat nového uživatele do této skupiny'), ['action' => 'addModify', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php endforeach; ?>
