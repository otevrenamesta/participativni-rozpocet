<?php

use App\Model\Entity\CustomUser;
use App\Model\Entity\CustomUserCategory;
use App\Model\Entity\IdentityProvider;
use App\View\AppView;
use Cake\Utility\Text;

/**
 * @var $this AppView
 * @var $custom_user CustomUser
 * @var $categories CustomUserCategory[]
 */

$this->assign('title', $custom_user->isNew() ? __('Přidání nového uživatele') : __('Úprava uživatele'));
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($custom_user);
        echo $this->Form->control(CustomUser::FIELD_USERNAME, ['label' => __('Uživatelské jméno (např. e-mail ale můžete zde napsat cokoli)')]);
        echo $this->Form->control(CustomUser::FIELD_PASSWORD, ['label' => __('Heslo (které uživatel musí znát spolu se jménem pro účast v hlasování nebo při navrhování projektů)'), 'type' => 'text']);

        echo $this->Form->control('custom_user_categories._ids', ['label' => __('Uživatelská skupina'), 'type' => 'select', 'options' => $categories, 'empty' => false, 'required' => true,]);
        ?>
        <hr/>
        <strong><?= __('Kontaktní informace') ?></strong>
        <?php
        echo $this->Form->control('contact.name', ['label' => __('Jméno a Příjmení')]);
        echo $this->Form->control('contact.phone', ['label' => __('Telefonní číslo (nepovinné)')]);
        echo $this->Form->control('contact.identity_provider_id', ['type' => 'hidden', 'value' => IdentityProvider::PROVIDER_CUSTOM_USERS]);
        echo $this->Form->control('contact.identity_key', ['type' => 'hidden', 'value' => Text::uuid()]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
