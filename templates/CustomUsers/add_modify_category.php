<?php

use App\Model\Entity\CustomUserCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $custom_category CustomUserCategory
 */

$this->assign('title', $custom_category->isNew() ? __('Přidání nové uživatelské skupiny') : __('Úprava uživatelské skupiny'));
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($custom_category);
        echo $this->Form->control(CustomUserCategory::FIELD_NAME, ['label' => __('Název skupiny (např. ročník participace nebo důvod proč tito nemohou použít běžné způsoby ověření)')]);
        echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']);
        echo $this->Form->end();
        ?>
    </div>
</div>
