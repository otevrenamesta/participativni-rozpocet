<?php

use App\Model\Entity\Organization;
use App\Model\Entity\ProjectStatus;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $status ProjectStatus[]
 */
$this->assign('title', __('Stavy projektů'));
echo $this->element('card_project_status', compact('organization', 'status'));
