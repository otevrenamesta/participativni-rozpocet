<?php

use App\Model\Entity\Organization;
use App\Model\Entity\ProjectStatus;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $status ProjectStatus
 * @var $finalStates array
 */

$this->assign('title', $status->isNew() ? __('Vytvořit nový stav projektu') : __('Upravit stav projektu'));
echo $this->element('spectrum_color_picker');
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <div class="alert alert-info">
            <?= __('Projekt může být v jednu chvíli pouze v jednom stavu') ?>
        </div>
        <?php
        echo $this->Form->create($status);
        echo $this->Form->control('name', ['label' => __('Název stavu')]);
        echo $this->Form->control('color', ['label' => __('Barva pozadí při zobrazení na webu'), 'class' => 'spectrum-color-picker']);
        echo $this->Form->control('is_public', ['label' => __('Je projekt veřejný v galerii projektů'), 'type' => 'checkbox']);
        echo $this->Form->control('is_voting_enabled', ['label' => __('Lze projekt v tomto stavu podpořit v hlasování?'), 'type' => 'checkbox']);
        echo $this->Form->control('are_votes_public', ['label' => __('Zda je v tomto stavu u projektu zobrazen aktuální počet hlasů'), 'type' => 'checkbox']);
        echo $this->Form->control('is_default_for_proposals', ['label' => __('Je tento stav výchozí pro nově navržené projekty?'), 'type' => 'checkbox']);
        echo $this->Form->control('is_editable_by_proposer', ['label' => __('Může navrhovatel projekt v tomto stavu upravovat?'), 'type' => 'checkbox']);
        echo $this->Form->control('project_final_status_id', ['label' => __('Finální stav projektu'), 'options' => $finalStates]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit();
        echo $this->Form->end();
        ?>
    </div>
</div>
