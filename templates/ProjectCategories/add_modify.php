<?php

use App\Model\Entity\Organization;
use App\Model\Entity\ProjectCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $category ProjectCategory
 */

$this->assign('title', $category->isNew() ? __('Vytvořit novou kategorii') : __('Upravit kategorii'));
echo $this->element('spectrum_color_picker');
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($category);
        echo $this->Form->control('name', ['label' => __('Název kategorie')]);
        echo $this->Form->control('color', ['label' => __('Barva pozadí při zobrazení na webu'), 'class' => 'spectrum-color-picker'])
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit();
        echo $this->Form->end();
        ?>
    </div>
</div>
