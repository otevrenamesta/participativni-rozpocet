<?php

use App\Model\Entity\Organization;
use App\Model\Entity\ProjectCategory;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $categories ProjectCategory[]
 */
$this->assign('title', __('Kategorie projektů'));
echo $this->element('card_project_categories', compact('organization', 'categories'));
