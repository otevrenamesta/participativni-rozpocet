<?php

use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organizations Organization[]
 */

$this->assign('title', __('Moje organizace'));
?>
<div class="card m-2">
    <div class="card-header">
        <div class="row">
            <div class="col-md">
                <h2>
                    <?= __('Vaše organizace') ?>
                </h2>
            </div>
            <div class="col-md text-right">
                <?= $this->Html->link(__('Přidat novou organizaci'), ['action' => 'addModify'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Název') ?></th>
                <th><?= __('Je povolena?') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($organizations as $organization): ?>
                <tr>
                    <td><?= $organization->id ?></td>
                    <td><?= $this->Html->link($organization->name, ['action' => 'detail', 'organization_id' => $organization->id]) ?></td>
                    <td><?= $organization->is_enabled ? __('ANO') : __('NE') ?></td>
                    <td>
                        <?= $this->Html->link(__('Upravit'), ['action' => 'addModify', 'organization_id' => $organization->id], ['class' => 'text-success']) ?>
                        ,
                        <?= $this->Html->link(__('Otevřít'), ['action' => 'detail', 'organization_id' => $organization->id], ['class' => 'text-primary']) ?>
                        ,
                        <?= $this->Form->postLink(
                            __('Smazat'),
                            ['action' => 'delete', 'organization_id' => $organization->id, '_method' => 'POST'],
                            ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat celou organizaci?')]
                        ) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
