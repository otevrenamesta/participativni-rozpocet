<?php

use App\IdentityProvider\NIAIdentityProvider;
use App\Model\Entity\IdentityProvider;
use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;
use Cake\Routing\Router;

/**
 * @var $this AppView
 * @var $organization Organization
 */

$this->assign('title', __('NIA eIdentita'));
?>

<div class="card m-2">
    <div class="card-header">
        <h2><?= __('Nastavení SeP - Poskytovatele služby') ?></h2>
    </div>
    <div class="card-body">

        <div class="alert alert-info">
            <?= __('Následující jsou informace, které jsou nezbytné k registraci této aplikace v prostředí SZRČR eIdentita jako SeP - Kvalifikovaného poskytovatele služby.') ?>
            <?= __('Pokud nevíte co s těmito informacemi, můžete začít studium zde: ') . $this->Html->link('https://nia.otevrenamesta.cz/sep-info') ?>
        </div>
        <div class="alert alert-warning">
            <?= __('Pokud využíváte náš federovaný portál, tak místo domény "paro2.otevrenamesta.cz" v následujících odkazech používejte přímo doménu vaši, tj. např. "participace.ostrava.cz"') ?>
        </div>
        <hr/>

        <?php
        echo $this->Form->control('nia_use_production', [
            'label' => sprintf(__('Používat produkční (ostré) prostředí, tj. %s ?'), NIAIdentityProvider::TARGET_URL_PRODUCTION),
            'disabled' => true,
            'type' => 'checkbox'
        ]);
        echo $this->Form->control('nia_sep_metadata_url', [
            'label' => __('URL s informacemi o poskytovateli služby, také Adresa pro naštení veřejné části šifrovacího certifikátu v nastavení SeP'),
            'default' => Router::url(['_name' => 'voting_metadata', 'organization_id' => $organization->id, 'provider_id' => IdentityProvider::PROVIDER_NIA], true),
            'disabled' => true,
        ]);
        echo $this->Form->control('nia_sep_issuer', [
            'label' => __('Unikátní URL adresa SeP - SAML 2.0 element Issuer'),
            'default' => Router::url(['_name' => 'public_organization_detail', 'organization_id' => $organization->id], true),
            'disabled' => true,
        ]);
        echo $this->Form->control('nia_sep_receive_url', [
            'label' => __('URL adresa pro příjem vydaného tokenu'),
            'default' => Router::url(['_name' => 'voting_non_interactive', 'provider_id' => IdentityProvider::PROVIDER_NIA], true),
            'disabled' => true,
        ]);
        echo $this->Form->control('nia_sep_logout_url', [
            'label' => __('URL adresa, na kterou bude uživatel přesměrován při odhlášení z Vašeho webu, TOTO JE NÁVRH, TUTO URL ADRESU SI MŮŽETE NASTAVIT DLE VLASTNÍHO UVÁŽENÍ'),
            'default' => Router::url(['_name' => 'public_organization_detail', 'organization_id' => $organization->id], true),
            'disabled' => true,
        ]);
        echo $this->Form->control('nia_certificate', [
            'label' => __('Certifikát pro NIA, který můžete zaregistrovat v detailech poskytovatele služby, pokud použijete k nastavení URL výše, toto pole můžete ignorovat'),
            'type' => 'textarea',
            'default' => $organization->getSettingValue(OrganizationSetting::NIA_CERTIFICATE_PEM),
            'disabled' => true,
        ]);
        ?>
    </div>
</div>
