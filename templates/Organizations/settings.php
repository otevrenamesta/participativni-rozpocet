<?php

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 */

$this->assign('title', __('Nastavení'));

$sections = [
    [
        'name' => __('Umístění a kontakt na organizaci'),
        'id' => 'basics',
        'settings' => OrganizationSetting::ALL_BASIC,
        'collapsed' => false,
    ],
];

echo $this->Form->create($organization);
$counter = 1;
foreach ($sections as $_section): ?>
    <div class="card mt-2 <?= $_section['border'] ?? '' ?>">
        <div class="card-header font-weight-bold">
            <a href="#<?= $_section['id'] ?>" class="text-dark " role="button" data-toggle="collapse"
               aria-controls="<?= $_section['id'] ?>" aria-expanded="false"><?= $_section['name'] ?></a>
        </div>
        <div class="card-body <?= ($_section['collapsed'] ?? false) ? 'collapse' : '' ?>" id="<?= $_section['id'] ?>">
            <?php
            foreach ($_section['settings'] as $field) {
                echo $this->Form->control('settings.' . $field, array_merge([
                    'label' => OrganizationSetting::getSettingLabel($field),
                    OrganizationSetting::getSettingFormOptionName($field) => $organization->getSettingValue($field, true),
                    'type' => OrganizationSetting::getSettingDataType($field)
                ]));
            }
            ?>
        </div>
        <?php if ($counter === count($sections)): ?>
            <div class="card-footer">
                <?php
                echo $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success m-2']);
                echo $this->Form->end();
                ?>
            </div>
        <?php endif; ?>
    </div>

<?php
endforeach;

