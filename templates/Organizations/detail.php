<?php

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $organization Organization
 */

$this->assign('title', $organization->name);
?>

<ul class="nav nav-tabs" id="index-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="basic-tab" data-toggle="tab" href="#basic-tab-content" role="tab"
           aria-controls="basic-tab-content" aria-selected="true">
            <?= __('Základní správa organizace') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="states-tab" data-toggle="tab" href="#states-tab-content" role="tab"
           aria-controls="states-tab-content" aria-selected="false">
            <?= __('Stavy projektů') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="categories-tab" data-toggle="tab" href="#categories-tab-content" role="tab"
           aria-controls="categories-tab-content" aria-selected="false">
            <?= __('Kategorie projektů') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="orgparts-tab" data-toggle="tab" href="#orgparts-tab-content" role="tab"
           aria-controls="orgparts-tab-content" aria-selected="false">
            <?= __('Útvary (části úřadu)') ?>
        </a>
    </li>
    <?php if ($organization->getSettingValue(OrganizationSetting::HAS_CITY_DISCTRICTS)): ?>
        <li class="nav-item">
            <a class="nav-link" id="districts-tab" data-toggle="tab" href="#districts-tab-content" role="tab"
               aria-controls="districts-tab-content" aria-selected="false">
                <?= __('Městské části') ?>
            </a>
        </li>
    <?php endif; ?>
    <li class="nav-item">
        <a class="nav-link" id="domains-tab" data-toggle="tab" href="#domains-tab-content" role="tab"
           aria-controls="domains-tab-content" aria-selected="false">
            <?= __('Domény') ?>
        </a>
    </li>
</ul>

<div class="tab-content" id="index-tabs-content">
    <div class="tab-pane fade show active" id="basic-tab-content" role="tabpanel" aria-labelledby="basic-tab">
        <div class="card m-2">
            <h2 class="card-header"><?= $organization->name ?></h2>
            <div class="card-body">
                <?= sprintf("%s %s", __('Organizace povolena?'), $organization->is_enabled ? __('ANO') : __('NE')) ?>
                <div class="alert alert-info m-2">
                    <?= __('Pokud organizace není povolena, není vidět ve veřejné galerii projektů, není dostupná v API a nefungují ani její dotační portály (domény), pokud jsou nastaveny') ?>
                </div>
            </div>
            <div class="card-footer">
                <?= $this->Html->link(__('Upravit organizaci'), ['action' => 'addModify', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
                <?= $this->Html->link(__('Nastavení správců organizace'), ['action' => 'managers', 'organization_id' => $organization->id], ['class' => 'btn btn-primary']) ?>
                <?= $this->Html->link(__('Nastavení organizace'), ['action' => 'settings', 'organization_id' => $organization->id], ['class' => 'btn btn-warning']) ?>
                <?= $this->Html->link(__('Nastavení NIA / eIdentita.cz'), ['action' => 'niaSettings', 'organization_id' => $organization->id], ['class' => 'btn btn-warning']) ?>
                <?= $this->Html->link(__('Nastavení Vlastních uživatelských kmenů'), ['controller' => 'CustomUsers', 'action' => 'index', 'organization_id' => $organization->id], ['class' => 'btn btn-warning']) ?>
            </div>
        </div>

        <div class="card m-2">
            <h3 class="card-header"><?= __('Ročníky') ?></h3>
            <div class="table-responsive card-body">
                <div class="alert alert-info">
                    <?= __('Ročník (nebo Výzva) je řada po sobě jdoucích etap průběhu participativního rozpočtu. Jednotlivé etapy, projekty, atd. můžete spravovat v detailu příslušné výzvy') ?>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th><?= __('Rok') ?></th>
                        <th><?= __('Odkaz na informace') ?></th>
                        <th><?= __('Rozpočet') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($organization->sortAppeals()->appeals as $appeal): ?>
                        <tr>
                            <td><?= $this->Html->link($appeal->year, ['controller' => 'Appeals', 'action' => 'detail', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id], ['class' => 'text-success']) ?></td>
                            <td><?= (empty($appeal->info_link) || !filter_var($appeal->info_link, FILTER_VALIDATE_URL, FILTER_NULL_ON_FAILURE)) ? $appeal->info_link : $this->Html->link($appeal->info_link, $appeal->info_link, ['class' => 'text-dark', 'target' => '_blank']) ?></td>
                            <td><?= Number::currency($appeal->budget_czk, 'CZK') ?></td>
                            <td>
                                <?= $this->Html->link(
                                    __('Otevřít'),
                                    ['controller' => 'Appeals', 'action' => 'detail', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id],
                                    ['class' => 'text-success'],
                                ) ?>
                                ,
                                <?= $this->Html->link(
                                    __('Upravit'),
                                    ['controller' => 'Appeals', 'action' => 'addModify', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id],
                                ) ?>
                                ,
                                <?= $this->Html->link(
                                    __('Vytvořit kopii'),
                                    ['controller' => 'Appeals', 'action' => 'makeCopy', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id],
                                    ['class' => 'text-primary'],
                                ) ?>
                                ,
                                <?php $this->Form->postLink(
                                    __('Smazat'),
                                    ['controller' => 'Appeals', 'action' => 'delete', 'appeal_id' => $appeal->id, 'organization_id' => $organization->id, '_method' => 'POST'],
                                    ['class' => 'text-danger'],
                                ) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <?= $this->Html->link(__('Vytvořit nový ročník'), ['action' => 'addModify', 'controller' => 'Appeals', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="states-tab-content" role="tabpanel" aria-labelledby="states-tab">
        <?= $this->element('card_project_status', ['organization' => $organization, 'status' => $organization->project_status, 'limit' => 10]); ?>
    </div>
    <div class="tab-pane fade" id="categories-tab-content" role="tabpanel" aria-labelledby="categories-tab">
        <?= $this->element('card_project_categories', ['organization' => $organization, 'categories' => $organization->project_categories, 'limit' => 5]); ?>
    </div>
    <div class="tab-pane fade" id="orgparts-tab-content" role="tabpanel" aria-labelledby="orgparts-tab">
        <?= $this->element('card_organization_parts', ['organization' => $organization, 'parts' => $organization->organization_parts, 'limit' => 10]); ?>
    </div>

    <?php if ($organization->getSettingValue(OrganizationSetting::HAS_CITY_DISCTRICTS, true) === true): ?>
        <div class="tab-pane fade" id="districts-tab-content" role="tabpanel" aria-labelledby="districts-tab">
            <div class="card m-2">
                <h3 class="card-header"><?= __('Městské části') ?></h3>
                <div class="table-responsive card-body">
                    <div class="alert alert-info">
                        <?= __('V nastavení organizace můžete ovlivnit, zda bude při vytváření/navrhování projektů povinné vybrat i příslušnou městskou část') ?>
                    </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><?= __('Městská část') ?></th>
                            <th><?= __('Akce') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($organization->districts as $district): ?>
                            <tr>
                                <td><?= $this->Html->link($district->name, ['controller' => 'Districts', 'action' => 'addModify', 'appeal_id' => $district->id, 'organization_id' => $organization->id, 'district_id' => $district->id], ['class' => 'text-success']) ?></td>
                                <td>
                                    <?= $this->Html->link(__('Upravit'), ['controller' => 'Districts', 'action' => 'addModify', 'organization_id' => $organization->id, 'district_id' => $district->id]) ?>
                                    ,
                                    <?= $this->Form->postLink(
                                        __('Smazat'),
                                        ['controller' => 'Districts', 'action' => 'delete', 'organization_id' => $organization->id, 'district_id' => $district->id, '_method' => 'POST'],
                                        ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat tuto doménu?')]
                                    ) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <?= $this->Html->link(__('Přidat městskou část'), ['action' => 'addModify', 'controller' => 'Districts', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="tab-pane fade" id="domains-tab-content" role="tabpanel" aria-labelledby="domains-tab">
        <div class="card m-2">
            <h3 class="card-header"><?= __('Domény') ?></h3>
            <div class="table-responsive card-body">
                <div class="alert alert-info">
                    <?= __('Domény které jsou povoleny pro danou organizaci, aby přistupovaly k API, zobrazovaly projekty uživatelům a přesměrovávaly na hlasování o projektech') ?>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th><?= __('Doména') ?></th>
                        <th><?= __('Povolena?') ?></th>
                        <th><?= __('Akce') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($organization->domains as $domain): ?>
                        <tr>
                            <td><?= $this->Html->link($domain->domain, ['controller' => 'Domains', 'action' => 'addModify', 'organization_id' => $organization->id, 'domain_id' => $domain->id], ['class' => 'text-success']) ?></td>
                            <td><?= $domain->is_enabled ? __('Povolena') : __('Zakázána') ?></td>
                            <td>
                                <?= $this->Html->link(__('Upravit'), ['controller' => 'Domains', 'action' => 'addModify', 'organization_id' => $organization->id, 'domain_id' => $domain->id]) ?>
                                ,
                                <?= $this->Form->postLink(
                                    __('Smazat'),
                                    ['controller' => 'Domains', 'action' => 'delete', 'organization_id' => $organization->id, 'domain_id' => $domain->id, '_method' => 'POST'],
                                    ['class' => 'text-danger', 'confirm' => __('Opravdu chcete smazat tuto doménu?')]
                                ) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <?= $this->Html->link(__('Přidat doménu'), ['action' => 'addModify', 'controller' => 'Domains', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
</div>
