<?php

use App\Model\Entity\Organization;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $users array
 */

$this->assign('title', __('Manažeři organizace'));
echo $this->Form->create($organization);
?>
    <div class="card m-2">
        <div class="card-header">
            <h2><?= $this->fetch('title') ?></h2>
        </div>
        <div class="card-body">
            <?php
            echo $this->Form->control('managers._ids', ['options' => $users]);
            ?>
        </div>
        <div class="card-footer">
            <?= $this->Form->submit(__('Uložit'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
<?php
echo $this->Form->end();
