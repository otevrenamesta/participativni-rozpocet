<?php

use App\View\AppView;

/**
 * @var $this AppView
 * @var string $resultMessage
 * @var string $allowedRedirectUrl
 * @var bool $resultIsError
 */
?>
<div class="row">
    <div class="col-md-8 offset-md-2 text-center p-4 mt-4 bg-light">
        <div class="alert alert-<?= $resultIsError ? 'danger' : 'success' ?> h2">
            <?= $resultMessage ?? __('Chybí výsledná zpráva') ?>
        </div>
        <?php
        echo $this->Html->link(__('Klikněte zde pro návrat do galerie projektů'), $allowedRedirectUrl ?? '/', ['class' => 'text-dark underline']);
        ?>
        <br/><br/>
        <span id="jsmessage" class="text-muted"></span>
        <script type="text/javascript">
            window.addEventListener('load', (event) => {
                document.getElementById('jsmessage').innerText = 'budete automaticky přesměrování za 5 vteřin';
                setTimeout(() => {
                    window.location = '<?= $allowedRedirectUrl ?? '/' ?>';
                }, 5000);
            });
        </script>
    </div>
</div>
