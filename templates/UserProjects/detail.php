<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Attachment;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Organization;
use App\Model\Entity\Phase;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectBudgetItem;
use App\Model\Entity\ProjectLog;
use App\Model\Entity\ProjectLogType;
use App\View\AppView;
use Cake\I18n\Number;

/**
 * @var $this AppView
 * @var $project Project
 * @var $appeal Appeal
 * @var $organization Organization
 * @var $emptyLog ProjectLog
 * @var $emptyBudgetItem ProjectBudgetItem
 * @var $emptyAttachment Attachment
 * @var $attachmentTypes array
 * @var $parts array
 */

$this->assign('title', h($project->name));
?>


<ul class="nav nav-tabs" id="index-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="basics-tab" data-toggle="tab" href="#basics-tab-content" role="tab"
           aria-controls="basics-tab-content" aria-selected="true">
            <?= __('Základní informace') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="budget-tab" data-toggle="tab" href="#budget-tab-content" role="tab"
           aria-controls="budget-tab-content" aria-selected="false">
            <?= __('Rozpočet projektu') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="attachments-tab" data-toggle="tab" href="#attachments-tab-content" role="tab"
           aria-controls="attachments-tab-content" aria-selected="false">
            <?= __('Přílohy / Fotogalerie') ?>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="votes-tab" data-toggle="tab" href="#votes-tab-content" role="tab"
           aria-controls="votes-tab-content" aria-selected="false">
            <?= __('Detaily hlasování') ?>
        </a>
    </li>

    <?php foreach (ProjectLogType::ALL_TYPES as $logTypeId): ?>
        <li class="nav-item">
            <a class="nav-link" id="logs-<?= $logTypeId ?>-tab" data-toggle="tab"
               href="#logs-<?= $logTypeId ?>-tab-content" role="tab"
               aria-controls="logs-<?= $logTypeId ?>-tab-content" aria-selected="false">
                <?= ProjectLogType::getLabel($logTypeId) ?>
            </a>
        </li>
    <?php endforeach; ?>
</ul>


<div class="tab-content" id="index-tabs-content">
    <div class="tab-pane fade show active" id="basics-tab-content" role="tabpanel" aria-labelledby="basics-tab">
        <?= $this->element('project_card', ['project' => $project, 'organization' => $appeal->organization, 'appeal' => $appeal, 'show_budget' => false]) ?>
    </div>
    <div class="tab-pane fade" id="budget-tab-content" role="tabpanel" aria-labelledby="budget-tab">

        <div class="card m-2">
            <div class="card-header">
                <h2><?= __('Rozpočet projektu') ?></h2>
            </div>
            <div class="card-body table-responsive">
                <?php
                $budgets = [
                    __('Navržený') => false,
                    __('Schválený') => true,
                ];
                foreach ($budgets as $title => $approvedState):
                    ?>
                    <h3><?= $title ?></h3>
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th><?= __('Položka') ?></th>
                            <th><?= __('Položková cena') ?></th>
                            <th><?= __('Počet položek') ?></th>
                            <th><?= __('Cena celkem') ?></th>
                            <?php if ($project->canProposerEdit()): ?>
                                <th><?= __('Akce') ?></th>
                            <?php endif; ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($project->project_budget_items as $item): ?>
                            <?php if ($item->is_final !== $approvedState) {
                                continue;
                            } ?>
                            <tr>
                                <td><?= $item->description ?></td>
                                <td><?= Number::currency($item->item_price, 'CZK') ?></td>
                                <td><?= $item->item_count ?></td>
                                <td><?= Number::currency($item->total_price, 'CZK') ?></td>
                                <?php if ($project->canProposerEdit()): ?>
                                    <td>
                                        <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'item_id' => $item->id], ['class' => 'text-success']) ?>
                                        ,
                                        <?= $this->Html->link(__('Smazat'), ['action' => 'deleteBudgetItem', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'item_id' => $item->id], ['class' => 'text-danger']) ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endforeach; ?>
            </div>
            <?php if ($project->canProposerEdit()): ?>
                <div class="card-footer" id="rozpocet">
                    <strong><?= __('Rychlé přidání položky') ?></strong>
                    <?php
                    echo $this->Form->create($emptyBudgetItem, [
                        'url' => ['action' => 'addModifyBudgetItem', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, '#' => 'rozpocet']
                    ]);

                    echo $this->Form->control('description', ['label' => __('Popis položky'), 'type' => 'string']);
                    echo $this->Form->control('item_price', ['label' => __('Cena za 1 kus (Kč)'), 'min' => 1, 'step' => 1, 'type' => 'number']);
                    echo $this->Form->control('item_count', ['label' => __('Počet kusů')]);
                    echo $this->Form->control('is_final', ['type' => 'hidden', 'value' => false]);

                    echo $this->Form->submit(__('Přidat'));
                    echo $this->Form->end();
                    ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="tab-pane fade" id="attachments-tab-content" role="tabpanel" aria-labelledby="attachments-tab">

        <div class="card m-2">
            <div class="card-header">
                <h2><?= __('Přílohy') ?></h2>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('Název') ?></th>
                        <th><?= __('Typ přílohy') ?></th>
                        <th><?= __('Velikost') ?></th>
                        <?php if ($project->canProposerEdit()): ?>
                            <th><?= __('Akce') ?></th>
                        <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($project->attachments as $attachment): ?>
                        <tr>
                            <td><?= sprintf("%s%s", $attachment->title, $attachment->id === $project->title_image_id ? __(' (titulní foto)') : '') ?></td>
                            <td><?= AttachmentType::getLabel($attachment->attachment_type_id) ?></td>
                            <td><?= Number::toReadableSize($attachment->filesize) ?></td>

                            <?php if ($project->canProposerEdit()): ?>
                                <td>
                                    <?= $this->Html->link(__('Upravit'), ['action' => 'addModifyAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'attachment_id' => $attachment->id], ['class' => 'text-success']) ?>
                                    ,
                                    <?= $this->Html->link(__('Smazat'), ['action' => 'deleteAttachment', 'project_id' => $project->id, 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'attachment_id' => $attachment->id], ['class' => 'text-danger']) ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <?php if ($project->canProposerEdit()): ?>
                <div class="card-footer" id="prilohy">
                    <strong><?= __('Rychlé přidání přílohy') ?></strong>
                    <?php
                    echo $this->Form->create($emptyAttachment, [
                        'type' => 'file',
                        'url' => ['action' => 'addModifyAttachment', 'organization_id' => $organization->id, 'appeal_id' => $appeal->id, 'project_id' => $project->id, '#' => 'prilohy']
                    ]);

                    echo $this->Form->control('title', ['label' => __('Název přílohy'), 'type' => 'string']);
                    echo $this->Form->control('attachment_type_id', ['label' => __('Typ přílohy'), 'options' => $attachmentTypes]);
                    echo $this->Form->control('filedata', ['type' => 'file', 'label' => __('Soubor')]);
                    echo $this->Form->control('is_title_image', ['label' => __('Zaškrtněte pokud je příloha titulním obrázkem projektu'), 'type' => 'checkbox']);

                    echo $this->Form->submit(__('Přidat'));
                    echo $this->Form->end();
                    ?>
                </div>
            <?php endif; ?>

        </div>
    </div>

    <div class="tab-pane fade" id="votes-tab-content" role="tabpanel" aria-labelledby="votes-tab-content">
        <div class="card m-2">
            <h2 class="card-header"><?= __('Detaily hlasování') ?></h2>
            <div class="card-body table-responsive">
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th><?= __('Fáze projektu') ?></th>
                        <th><?= __('Pozitivní hlasy') ?></th>
                        <th><?= __('Negativní hlasy') ?></th>
                        <th><?= __('Výsledek hlasování') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    list($breakdown, $overall) = $project->getVotesBreakdown();
                    foreach ($breakdown as $phase_id => $phase_stats):
                        ?>
                        <tr>
                            <td><?= $appeal->getPhaseById($phase_id, Phase::FIELD_NAME) ?></td>
                            <td><?= $phase_stats['positive'] ?></td>
                            <td><?= $phase_stats['negative'] ?></td>
                            <td><?= $phase_stats['positive'] - $phase_stats['negative'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot class="thead-dark">
                    <tr>
                        <th><?= __('Celkem') ?></th>
                        <th><?= $overall['positive'] ?></th>
                        <th><?= $overall['negative'] ?></th>
                        <th><?= $overall['positive'] - $overall['negative'] ?></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <?php foreach (ProjectLogType::ALL_TYPES as $logTypeId): ?>
        <div class="tab-pane fade" id="logs-<?= $logTypeId ?>-tab-content" role="tabpanel"
             aria-labelledby="logs-<?= $logTypeId ?>-tab">

            <div class="card m-2">
                <div class="card-header">
                    <h2><?= ProjectLogType::getLabel($logTypeId) ?></h2>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th><?= __('Útvar') ?></th>
                            <th><?= __('Nadpis') ?></th>
                            <th><?= __('Text') ?></th>
                            <th><?= __('Platné od') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($project->getLogsByType($logTypeId) as $log): ?>
                            <tr>
                                <td><?= $log->organization_part->name ?? '' ?></td>
                                <td><?= $log->title ?></td>
                                <td><?= $log->description ?></td>
                                <td><?= $log->date_when->nice() ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>
