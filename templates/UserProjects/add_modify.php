<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\Model\Entity\Project;
use App\View\AppView;
use Cake\Collection\Collection;

/**
 * @var $this AppView
 * @var $project Project
 * @var $appeal Appeal
 * @var $organization Organization
 */

$this->assign('title', $project->isNew() ? __('Vytvoření nového projektu') : __('Úprava projektu'));
echo $this->Form->create($project);
?>
<div class="card m-2">
    <h2 class="card-header">
        <?= $this->fetch('title') ?>
    </h2>
    <div class="card-body">
        <strong><?= __('Základní informace o projektu') ?></strong>
        <?php
        echo $this->Form->control('name', ['label' => __('Název projektu')]);
        echo $this->Form->control('project_categories._ids', ['options' => (new Collection($organization->project_categories))->combine('id', 'name'), 'label' => __('Kategorie do kterých projekt zapadá'), 'empty' => false, 'required' => 'required', 'class' => 'select2']);
        echo $this->Form->control('annotation', ['label' => __('Anotace projektu')]);
        echo $this->Form->control('description', ['label' => __('Popis projektu'), 'data-use-quill' => 'data-use-quill', 'required' => true,]);
        echo $this->Form->control('public_interest', ['label' => __('Veřejný přínos projektu'), 'required' => true]);
        ?>
        <hr/>
        <strong><?= __('Kontakty na navrhovatele') ?></strong>
        <?php
        echo $this->Form->control('contact.name', ['label' => __('Jméno a Příjmení'), 'disabled' => false]);
        echo $this->Form->control('contact.email', ['label' => __('E-mailová adresa'), 'disabled' => true, 'value' => $this->getUser('email')]);
        echo $this->Form->control('contact.phone', ['label' => __('Telefonní číslo'), 'disabled' => false]);
        ?>
        <hr/>
        <strong><?= __('Umístění') ?></strong>
        <?php
        echo $this->Form->control('has_no_location', ['label' => __('Projekt není vázán na konkrétní lokalitu'), 'type' => 'checkbox']);

        if ($organization->getSettingValue(OrganizationSetting::HAS_CITY_DISCTRICTS, true)) {
            echo $this->Form->control('district_id', ['label' => __('Městská část do které projekt spadá'), 'options' => (new Collection($organization->districts))->combine('id', 'name'), 'empty' => __('Vyberte z nabídky prosím')]);
        }
        echo $this->Form->control('address', ['label' => __('Adresa místa realizace projektu'), 'class' => 'mapy-suggestor', 'data-suggestor-callback' => 'suggestionSelected']);

        echo $this->Form->control('gps_latitude', ['label' => __('Zeměpisná šířka (GPS, např. 50.0588364)'), 'step' => "0.0000001"]);
        echo $this->Form->control('gps_longitude', ['label' => __('Zeměpisná délka (GPS, např. 14.4125592)'), 'step' => "0.0000001"]);

        echo $this->element('map_position_select', ['organization' => $organization, 'lat' => $project->gps_latitude, 'lon' => $project->gps_longitude]);
        ?>
    </div>
    <div class="card-footer">
        <?= $this->Form->submit(__('Uložit koncept'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
echo $this->Form->end();
?>

<script type="text/javascript">
    let districtsCodeToId = <?= json_encode((new Collection($organization->districts))->combine('ui_momc', 'id')) ?>;

    function positionSelected(position) {
        let floats = position.toWGS84();
        $("input#gps-latitude").val(parseFloat(floats[1]).toFixed(5));
        $("input#gps-longitude").val(parseFloat(floats[0]).toFixed(5));
        $.fn.paro2.cuzk.getMOMCCode(floats[0], floats[1], function (code) {
            console.log('MOMC: ', code);
            $("select#district-id").val(districtsCodeToId[code]).trigger('change');
        });
        $.fn.paro2.mapy_select_point.reverseGeocode(position, $("input#address"));
    }

    function suggestionSelected(suggestionResult) {
        let lat = suggestionResult.data.latitude.toFixed(5);
        let lon = suggestionResult.data.longitude.toFixed(5);
        let coords = SMap.Coords.fromWGS84(lon, lat);
        $("input#gps-latitude").val(lat);
        $("input#gps-longitude").val(lon);
        $.fn.paro2.mapy_select_point.setNewCoords(undefined, coords, true);
        $.fn.paro2.mapy_select_point.reverseGeocode(coords, $("input#address"));
    }

</script>
