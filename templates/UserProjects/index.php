<?php

use App\Model\Entity\Appeal;
use App\Model\Entity\Contact;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectSubmissionStatus;
use App\View\AppView;
use Cake\Utility\Hash;

/**
 * @var $this AppView
 * @var $projects Project[]
 * @var $contacts Contact[]
 * @var $open_appeals Appeal[]
 */

$this->assign('title', __('Mé projekty'));
?>

<div class="card m-2">
    <h2 class="card-header"><?= __('Nyní můžete navrhovat projekty v těchto obcích / ročnících') ?></h2>
    <div class="card-body">
        <?php if (empty($open_appeals)): ?>
            <div class="alert alert-danger">
                <?= __('V tuto chvíli bohužel není otevřena žádná výza k podání projektů') ?>
            </div>
        <?php else: ?>
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th><?= __('Ročník') ?></th>
                    <th><?= __('Obec') ?></th>
                    <th><?= __('Odkaz na informace o výzvě / ročníku') ?></th>
                    <th><?= __('Akce') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($open_appeals as $appeal): ?>
                    <tr>
                        <td><?= $appeal->year ?></td>
                        <td><?= $appeal->organization->name ?></td>
                        <td><?= $this->Html->link($appeal->info_link, $appeal->info_link, ['target' => '_blank']) ?></td>
                        <td>
                            <?= $this->Html->link(__('Navrhnout nový projekt'), ['action' => 'addModify', 'appeal_id' => $appeal->id], ['class' => 'btn btn-success']) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>

    </div>
</div>

<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <?php if ($projects->isEmpty()): ?>
        <div class="card-body">
            <div class="alert alert-info">
                <?= __('Ještě nemáte žádné projekty') ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php foreach ($projects as $project): ?>
    <?= $this->element('project_card', ['project' => $project, 'appeal' => $project->appeal, 'organization' => $project->appeal->organization, 'brief' => true,]) ?>
<?php endforeach; ?>
