<?php

/**
 * @var $this AppView
 */

use App\View\AppView;
use Cake\Routing\Router;

$this->assign('title', __('Participativní Rozpočet - Widgety'));
?>
<div class="card m-2">
    <h2 class="card-header">
        <?= $this->fetch('title') ?>
    </h2>
    <div class="card-body">
        <?= sprintf(
            __('Pokud chcete prezentovat participativní rozpočet svojí obce na vlastních stránkách a zároveň využít tento projekt, pro správu projektů a hlasování, můžete využít %s a informace zobrazovat vlastním způsobem'),
            $this->Html->link(__('API'), ['_name' => 'api_info'])
        ) ?>
        <br/><br/>
        <?= __('Pro zjednodušení jsme pro vás připravili widget, který si můžete vložit do svých stránek a proces si tak velmi zjednodušit') ?>
        <br/><br/>
        <?= __('Widget je používán i na našich stránkách, například v sekci Galerie projektů') ?>
    </div>
</div>

<div class="card m-2">
    <h2 class="card-header"><?= __('Instrukce') ?></h2>
    <div class="card-body">
        <?= __('Pro správnou funkčnost, potřebujete minimálně tyto komponenty:') ?>
        <br/><br/>
        <strong>HTML</strong>
        <pre>
            // ve vaší prezentaci si vyhraďte DIV, kterému určíte šířku, ostatní obsah widgetu se přizpůsobí
            // id=gallery je jen příklad, můžete si zvolit vlastní
            &lt;div id="gallery" style="width: 100%;">&lt;/div>
        </pre>
        <strong>CSS</strong>
        <pre>
            // tato verze bootstrap je upravena čistě pro Participativní Rozpočet, nelze ji nahradit běžnou verzí Bootstrap knihovny
            &lt;link rel="stylesheet" href="<?= Router::url('/widgets/common/bootstrap-prefix/bootstrap-4.5.2.paro2.min.css', true) ?>"/&gt;
            &lt;link rel="stylesheet" href="<?= Router::url('/widgets/jquery.paro2-widgets.css', true) ?>"/&gt;
        </pre>
        <strong>Javascript</strong>
        <pre>
            // tato verze bootstrap je upravena čistě pro Participativní Rozpočet, nelze ji nahradit běžnou verzí Bootstrap knihovny
            &lt;script src="<?= Router::url('/widgets/common/jquery-3.5.1.min.js', true) ?>"&gt;&lt;/script&gt;
            &lt;script src="<?= Router::url('/widgets/common/bootstrap-prefix/bootstrap-4.5.2.bundle.paro2.min.js', true) ?>"&gt;&lt;/script&gt;
            &lt;script src="<?= Router::url('/widgets/jquery.paro2-widgets.js', true) ?>"&gt;&lt;/script&gt;
            // následující skript po kompletním načtení DOM dokumentu nahraje z API data pro zvolenou organizaci (nebo všechny organizace)
            // a zobrazí projekty a/nebo harmonogram aktuálně probíhajícího ročníku
            &lt;script type="text/javascript"&gt;
                // onload
                $(function() {
                    $("#gallery").participativniRozpocet({
                        // target API host, full without relative path, eg. https://example.com
                        host: 'https://paro2.otevrenamesta.cz',
                        // ID of organization or false, if false, allows selecting between public organizations
                        organization: 1,
                    });
                });
            &lt;/script&gt;
        </pre>
        <br/><br/>
        <?=
        __('Vzhled a chování je konfigurovatelné, pro kompletní přehled možností konfigurace, se podívejte přímo do JS knihovny, blok začínající "let defaults = " např. zde: ')
        . $this->Html->link(Router::url('/widgets/jquery.paro2-widgets.js', true))
        ?>
        <br/>
        <?= __('Pomocí zmíněných parametrů, lze např. widget omezit na jednotlivou organizaci, ročník nebo upravit všechny texty, které widget zobrazuje') ?>
    </div>
</div>
