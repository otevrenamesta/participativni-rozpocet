<?php
/**
 * @var $this AppView
 */

use App\View\AppView;

$this->assign('title', __('Galerie projektů'));

$this->Html->css([
    '/widgets/common/bootstrap-prefix/bootstrap-4.5.2.paro2.min.css',
    '/widgets/jquery.paro2-widgets.css'
], ['block' => true]);
$this->Html->script([
    '/widgets/common/bootstrap-prefix/bootstrap-4.5.2.bundle.paro2.min.js',
    '/widgets/jquery.paro2-widgets.js'
], ['block' => true]);
$this->Html->scriptBlock('$(function () { $("#gallery").participativniRozpocet({texts:{label_organizations:"Obec"}}); });', ['block' => true]);
?>
<div id="gallery" class="w-100 h-100 mh-100 mb-4">

</div>

