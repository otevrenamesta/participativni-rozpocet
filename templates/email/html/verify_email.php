<?php

use App\View\AppView;
use Cake\Routing\Router;

/**
 * @var $this AppView
 * @var $user_id int
 * @var $code string
 */


?>
<?= __('Svůj uživatelský účet můžete ověřit a aktivovat pomocí následujícího odkazu') ?>
<br/>
<?= $this->Html->link(Router::url(['_name' => 'verify_email', 'user_id' => $user_id, 'code' => $code], true)) ?>
<br/><br/>
<?= __('Pokud vám tento e-mail přišel omylem nebo jste se neregistrovali, ozvěte se nám prosím na damenavas@brno.cz, a společně to vyřešíme') ?>
