<?php
/**
 * @var $this AppView
 * @var $project Project
 * @var $organization Organization
 * @var $appeal Appeal
 * @var $brief bool
 */

if (!isset($brief)) {
    $brief = false;
}

if (!isset($show_budget)) {
    $show_budget = true;
}

use App\Model\Entity\Appeal;
use App\Model\Entity\AttachmentType;
use App\Model\Entity\Organization;
use App\Model\Entity\Project;
use App\Model\Entity\ProjectSubmissionStatus;
use App\View\AppView;
use Cake\I18n\Number;
use Cake\Utility\Hash;

?>
<div class="card m-2">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h2><?= h($project->name) ?></h2>
                <div>
                    <?= $organization->name . ', ' . $appeal->year ?>
                </div>
            </div>
            <div class="col h3 text-right">
                    <span class="badge"
                          style="color: <?= getContrastColor($project->project_status->color) ?>; background-color: <?= '#' . $project->project_status->color ?>;"><?= $project->project_status->name ?></span>
                <span class="badge"
                      style="color: <?= getContrastColor(ProjectSubmissionStatus::getColor($project->project_submission_status_id)) ?>; background-color: <?= ProjectSubmissionStatus::getColor($project->project_submission_status_id) ?>">
                        <?= $project->project_submission_status->name ?>
                    </span>
            </div>
        </div>
    </div>
    <?php if ($project->isProposer($this->getUser())): ?>
        <div class="card-header">
            <?php

            if ($this->getRequest()->getParam('action') === 'detail' && $project->canProposerEdit()) {
                echo $this->Html->link(__('Upravit projekt'), ['_name' => 'user_projects_modify', 'appeal_id' => $project->appeal_id, 'project_id' => $project->id], ['class' => 'btn btn-success m-2']);
            }

            if ($project->canProposerSubmit() && $this->getRequest()->getParam('controller') === 'UserProjects') {
                echo $this->Html->link(__('Odeslat projekt ke kontrole'), ['action' => 'submit', 'appeal_id' => $project->appeal_id, 'project_id' => $project->id], ['class' => 'btn btn-warning m-2']);
            }

            if ($this->getRequest()->getParam('action') !== 'detail' && $this->getRequest()->getParam('controller') === 'UserProjects') {
                echo $this->Html->link(__('Otevřít detail projektu'), ['action' => 'detail', 'appeal_id' => $project->appeal_id, 'project_id' => $project->id], ['class' => 'btn btn-primary m-2']);
            }
            ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($project->formal_comment)): ?>
        <div class="card-header">
            <div
                class="alert alert-<?= $project->project_submission_status_id === ProjectSubmissionStatus::FORMAL_CONTROL_ACCEPTED ? 'success' : 'danger' ?>">
                <strong><?= $project->project_submission_status_id === ProjectSubmissionStatus::FORMAL_CONTROL_ACCEPTED ? __('Zpráva od koordinátora participativního rozpočtu') : __('Návrh vám byl vrácen k opravě s následujícím komentářem: ') ?></strong>

                <?= $project->formal_comment ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($brief === false): ?>
        <div class="card-body">
            <table class="table table-bordered">
                <thead class="thead-dark">
                <tr>
                    <th><?= __('Popis') ?></th>
                    <th><?= __('Obsah') ?></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?= __('Kategorie') ?></td>
                    <td>
                        <?php
                        foreach ($project->project_categories as $category) {
                            echo $category->name . ', ';
                        }
                        ?>
                    </td>
                </tr>
                <?php
                $listing = [
                    'project_status.name' => __('Stav projektu'),
                    'annotation' => __('Anotace'),
                    'description' => __('Popis'),
                    'public_interest' => __('Veřejný přínos projektu'),
                ];
                if (!$project->has_no_location) {
                    $listing = array_merge($listing, [
                        'district.name' => __('Městská část'),
                        'address' => __('Adresa / Místo realizace'),
                    ]);
                }
                foreach ($listing as $path => $description):
                    ?>
                    <tr>
                        <td><?= $description ?></td>
                        <td><?= Hash::get($project, $path) ?></td>
                    </tr>
                <?php
                endforeach;
                ?>
                <?php if (!$project->has_no_location): ?>
                    <tr>
                        <td><?= __('Umístění projektu') ?></td>
                        <td>
                            <?= $this->element('map_display_only', ['lat' => $project->gps_latitude, 'lon' => $project->gps_longitude, 'organization' => $organization]) ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ($show_budget === true): ?>
                    <tr>
                        <td>
                            <?= __('Rozpočet projektu') ?>
                        </td>
                        <td class="table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                <tr>
                                    <th><?= __('Položka') ?></th>
                                    <th><?= __('Položková cena') ?></th>
                                    <th><?= __('Počet položek') ?></th>
                                    <th><?= __('Cena celkem') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $total_proposed = 0; ?>
                                <?php foreach ($project->project_budget_items as $item): ?>
                                    <?php
                                    if ($item->is_final) {
                                        continue;
                                    }
                                    $total_proposed += $item->total_price;
                                    ?>
                                    <tr>
                                        <td><?= $item->description ?></td>
                                        <td><?= Number::currency($item->item_price, 'CZK') ?></td>
                                        <td><?= $item->item_count ?></td>
                                        <td><?= Number::currency($item->total_price, 'CZK') ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                <tfoot class="thead-dark">
                                <tr>
                                    <th>
                                        <?= __('Celkem') ?>
                                    </th>
                                    <th></th>
                                    <th></th>
                                    <th>
                                        <?= Number::currency($total_proposed, 'CZK') ?>
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td><?= __('Fotogalerie') ?></td>
                    <td>
                        <div class="row">
                            <?php
                            foreach ($project->attachments as $attachment) {
                                if ($attachment->attachment_type_id !== AttachmentType::TYPE_PHOTO) {
                                    continue;
                                }
                                ?>
                                <div class="col-4">
                                    <?= $this->Html->image(['action' => 'downloadAttachment', 'organization_id' => $organization->id, 'appeal_id' => $project->appeal_id, 'project_id' => $project->id, 'attachment_id' => $attachment->id, 'force' => false], ['class' => 'mw-100']) ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= __('Ostatní přílohy') ?></td>
                    <td>
                        <?php
                        foreach ($project->attachments as $attachment) {
                            if ($attachment->attachment_type_id === AttachmentType::TYPE_PHOTO) {
                                continue;
                            }
                            echo $this->Html->link(h($attachment->original_filename), ['action' => 'downloadAttachment', 'organization_id' => $organization->id, 'appeal_id' => $project->appeal_id, 'project_id' => $project->id, 'attachment_id' => $attachment->id, 'force' => true], ['class' => 'd-block']);
                        }
                        ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
</div>
