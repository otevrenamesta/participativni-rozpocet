<?php

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationSetting;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $lat null|string marker position latitude
 * @var $lon null|string marker position longitude
 **/

$this->Html->script('https://api.mapy.cz/loader.js', ['block' => true]);
$this->Html->scriptBlock("Loader.load(null, {suggest: true})", ['block' => true]);
$this->Html->script('proj4-2.7.2.min.js', ['block' => true]);
$this->Html->script('paro2-cuzk.js', ['block' => true]);
$this->Html->script('mapy-select-point.js', ['block' => true]);
?>

<div
    id="mapy-select-point"
    class="w-100"
    style="height: 400px"
    data-center-lat="<?= $lat ?? $organization->getSettingValue(OrganizationSetting::MAP_CENTER_LAT, true) ?>"
    data-center-lon="<?= $lon ?? $organization->getSettingValue(OrganizationSetting::MAP_CENTER_LON, true) ?>"
    data-default-zoom="<?= $organization->getSettingValue(OrganizationSetting::MAP_DEFAULT_ZOOM, true) ?>"
>
</div>
<a href="#" id="mapy-select-point-kn-link">Zobrazit informace o poloze v Katastru Nemovitostí</a>
