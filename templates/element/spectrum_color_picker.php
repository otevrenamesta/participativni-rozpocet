<?php
/**
 * @var $this \App\View\AppView
 **/

$this->Html->css('spectrum-1.8.1.css', ['block' => true]);
$this->Html->script('spectrum-1.8.1.js', ['block' => true]);
$this->Html->script('spectrum-apply.js', ['block' => true]);
