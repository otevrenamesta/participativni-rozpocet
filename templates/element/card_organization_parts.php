<?php

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationPart;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $parts OrganizationPart[]
 * @var $limit int
 */
if (!isset($limit) || !is_numeric($limit)) {
    $limit = 0;
}

usort($parts, function (OrganizationPart $a, OrganizationPart $b) {
    return strcmp($a->text_code, $b->text_code);
});
$counter = 0;
?>

<div class="card m-2">
    <h2 class="card-header"><?= __('Útvary') ?></h2>
    <div class="card-body table-responsive">
        <div class="alert alert-info">
            <?= __('Následující jsou útvary, části úřadu, které se mohou vyjadřovat při posouzení proveditelnosti projektů') ?>
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><?= __('ID') ?></th>
                <th><?= __('Název') ?></th>
                <th><?= __('Kód útvaru') ?></th>
                <th><?= __('Zkratka útvaru?') ?></th>
                <th><?= __('Akce') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($parts as $part): ?>
                <?php $counter++; ?>
                <?php if ($limit === 0 || $counter < $limit): ?>
                    <tr>
                        <td><?= $part->id ?></td>
                        <td><?= $part->name ?></td>
                        <td><?= $part->number_code ?></td>
                        <td><?= $part->text_code ?></td>
                        <td>
                            <?= $this->Html->link(__('Upravit'), ['controller' => 'OrganizationParts', 'action' => 'addModify', 'organization_id' => $organization->id, 'part_id' => $part->id]) ?>
                            ,
                            <?= $this->Form->postLink(
                                __('Smazat'),
                                ['controller' => 'OrganizationParts', 'action' => 'delete', 'organization_id' => $organization->id, 'part_id' => $part->id, '_method' => 'POST'],
                                ['confirm' => __('Opravdu chcete smazat tento útvar?'), 'class' => 'text-danger']
                            ) ?>
                        </td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td colspan="7"><?= $this->Html->link(sprintf(__('A dalších %d ...'), count($parts) - $limit), ['controller' => 'OrganizationParts', 'action' => 'index', 'organization_id' => $organization->id]) ?></td>
                    </tr>
                    <?php break; endif; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <?= $this->Html->link(__('Přidat nový útvar'), ['action' => 'addModify', 'controller' => 'OrganizationParts', 'organization_id' => $organization->id], ['class' => 'btn btn-success']) ?>
    </div>
</div>
