<?php
/**
 * @var $this AppView
 */

use App\Model\Entity\UserRole;
use App\View\AppView;

?>
<ul class="navbar-nav ml-auto">
    <!--    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="sectionsDropdown" role="button" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <?= __('Administrace') ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="sectionsDropdown">
        </div>
    </li>-->
    <li class="nav-item">
        <?= $this->Html->link(__('Mé projekty'), ['action' => 'index', 'controller' => 'UserProjects'], ['class' => 'btn btn-success ml-2 mr-2']) ?>
    </li>
    <?php if ($this->userHasRole(UserRole::ROLE_MANAGER) || $this->userHasManagedOrganizations()): ?>
        <li class="nav-item">
            <?= $this->Html->link(__('Moje Organizace'), ['action' => 'index', 'controller' => 'Organizations'], ['class' => 'btn btn-primary ml-2 mr-2']) ?>
        </li>
    <?php endif; ?>
    <li class="nav-item dropdown">
        <a href="#" id="dropdownUserSettings" class="dropdown-toggle nav-link" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user"></i> <?= h($this->getUser('email')) ?>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUserSettings">
            <?= $this->Html->link(__('Odhlásit se'), ['_name' => 'logout'], ['class' => 'dropdown-item']) ?>
        </div>
    </li>
</ul>
