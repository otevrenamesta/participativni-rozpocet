<?php

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationPart;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $parts OrganizationPart[]
 */
$this->assign('title', __('Útvary (části organizace)'));
echo $this->element('card_organization_parts', compact('organization', 'parts'));
