<?php

use App\Model\Entity\Organization;
use App\Model\Entity\OrganizationPart;
use App\View\AppView;

/**
 * @var $this AppView
 * @var $organization Organization
 * @var $part OrganizationPart
 */

$this->assign('title', $part->isNew() ? __('Vytvořit nový útvar (část organizace)') : __('Upravit útvar (část organizace)'));
echo $this->element('spectrum_color_picker');
?>
<div class="card m-2">
    <h2 class="card-header"><?= $this->fetch('title') ?></h2>
    <div class="card-body">
        <?php
        echo $this->Form->create($part);
        echo $this->Form->control('name', ['label' => __('Název útvaru')]);
        echo $this->Form->control('number_code', ['label' => __('Číselný kód')]);
        echo $this->Form->control('text_code', ['label' => __('Textový kód / zkratka')]);
        ?>
    </div>
    <div class="card-footer">
        <?php
        echo $this->Form->submit();
        echo $this->Form->end();
        ?>
    </div>
</div>
